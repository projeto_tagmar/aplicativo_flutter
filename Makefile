############################################################################### Principal

help:		# Essa ajuda
	@clear
	@echo ""
	@echo '████████╗ █████╗  ██████╗ ███╗   ███╗ █████╗ ██████╗ '
	@echo '╚══██╔══╝██╔══██╗██╔════╝ ████╗ ████║██╔══██╗██╔══██╗'
	@echo '   ██║   ███████║██║  ███╗██╔████╔██║███████║██████╔╝'
	@echo '   ██║   ██╔══██║██║   ██║██║╚██╔╝██║██╔══██║██╔══██╗'
	@echo '   ██║   ██║  ██║╚██████╔╝██║ ╚═╝ ██║██║  ██║██║  ██║'
	@echo '   ╚═╝   ╚═╝  ╚═╝ ╚═════╝ ╚═╝     ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝'
	@echo ""
	@cat $(MAKEFILE_LIST) | grep -e "^[a-zA-Z_\-]*: *.*"


############################################################################### Apoio ao flutter

update:		# Baixa dependências
	flutter pub get

test:		# Roda os testes
	@echo 'todo'

publish:	# Publica
	flutter build appbundle

run:		# Roda
	@flutter run

soLinux:	# configura para linux
	@flutter config --enable-linux-desktop

soNoLinux:	# retira configuracao para linux
	@flutter config --no-enable-linux-desktop