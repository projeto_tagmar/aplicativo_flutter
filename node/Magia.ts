import Prop from "./Prop";
import Nivel from "./Nivel";

class Magia {
  id: number;
  nome: string;
  evocacao: Prop;
  alcance: Prop;
  duracao: Prop;
  descricao: string;
  niveis: Nivel[];

  constructor(id: number) {
    this.id = id;
    this.nome = "";
    this.niveis = [];
    this.descricao = "";
    this.alcance = new Prop("alcance", "Alcance");
    this.duracao = new Prop("duracao", "Duração");
    this.evocacao = new Prop("evocacao", "Evocação");
  }

  reset(): void {
    this.id++;
    this.nome = "";
    this.evocacao.reset();
    this.alcance.reset();
    this.duracao.reset();
    this.descricao = "";
    this.niveis.length = 0;
  }

  parseLine(line: string) {
    if (this.nome.trim().length === 0) {
      this.nome = line.replace(/[\.	]/gi, "").trim();
    } else if (this.evocacao.fromLine(line)) return;
    else if (this.alcance.fromLine(line)) return;
    else if (this.duracao.fromLine(line)) return;
    else if (this.isAddNivel(line)) return;
    else {
      this.descricao += line.trim() + "\n\n";
    }
  }

  private getNiveis() {
    let output = "   niveis: [\n";
    for (let i = 0; i < this.niveis.length; ++i) {
      output += this.niveis[i].toDart(i);
    }
    output += "],\n";
    return output;
  }

  private getEnums() {
    let output = this.alcance.toDart();
    output += this.duracao.toDart();
    output += this.evocacao.toDart();
    return output;
  }

  toDart(): string {
    let output = `Magia(id: ${this.id}, nome: "${this.nome.trim()}",\n`;
    output += this.getNiveis();
    output += `   descricao: '''${this.descricao.trim()}''',\n`;
    output += this.getEnums();

    output += "),";
    output += "\n";

    return output;
  }

  private isAddNivel(line: string): boolean {
    if (
      line.toLowerCase().match(new RegExp(this.nome.toLowerCase() + " \\d+:"))
    ) {
      const index = line.indexOf(":") + 1;
      const desc: string = line.slice(index).trim();
      const valorNivel: number = Number.parseInt(line.match(/(\D*)(\d+)/)[2]);

      this.niveis.push(new Nivel(valorNivel, desc));

      return true;
    }
    return false;
  }
}

export default Magia;
