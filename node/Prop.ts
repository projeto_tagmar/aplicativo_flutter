import { capitalizeFirstLetter, clear } from "./Help";

class Prop {
  nome: string;
  valor: number;
  medida: string;
  inText: string;
  constructor(nome: string, inText: string) {
    this.nome = nome;
    this.inText = inText;

    this.valor = NaN;
    this.medida = undefined;
  }

  reset(): void {
    this.valor = NaN;
    this.medida = undefined;
  }

  fromLine(line: string): boolean {
    if (line.includes(`${this.inText}: `)) {
      const ev = line.replace(`${this.inText}:`, "").trim();
      const split = ev.split(" ");
      this.valor = Number(split[0]);
      if (split.length > 1) {
        this.medida = split[1];
      } else {
        this.medida = split[0];
      }
      return true;
    }
    return false;
  }

  getValor(valor): string {
    const v = Number(valor);
    if (isNaN(v)) {
      return "";
    }
    return `valor: ${v},`;
  }

  toDart(): string {
    const capital = capitalizeFirstLetter(this.nome);

    return `${this.nome}: ${capital}(tipo: Enum${capital}.${clear(
      this.medida
    )}, ${this.getValor(this.valor)}),
    `;
  }
}

export default Prop;
