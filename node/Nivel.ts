class Nivel {
  private valor: number;
  private descricao: string;

  constructor(valor: number, descricao: string) {
    this.valor = valor;
    this.descricao = descricao;
  }

  toDart(id: number): string {
    return `       Nivel(id: ${id},
           descricao: "${this.descricao.trim()}",
           nivel: ${this.valor},
           ),\n`;
  }
}

export default Nivel;
