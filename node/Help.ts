export function clear(str: string): string {
  return str
    .trim()
    .toLowerCase()
    .normalize("NFKD")
    .replace(/[^\w\s.-_\/]/g, "");
}

export function capitalizeFirstLetter(string: string): string {
  return string.charAt(0).toUpperCase() + string.slice(1);
}
