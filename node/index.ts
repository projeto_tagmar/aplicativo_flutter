import * as fs from "fs";

import Magia from "./Magia";
import { arquivo_saida, arquivo_entrada, contem } from "./Constants";

const input = fs.readFileSync(arquivo_entrada, {
  encoding: "utf-8",
  flag: "r",
});
const lines = input.split("\n");

const magia: Magia = new Magia(227);

for (let i = 0; i < lines.length; ++i) {
  const line = lines[i].trim();
  if (line.length === 0) {
    continue;
  }
  parseLine(line);
}

function parseLine(line) {
  if (line.trim().includes("###")) {
    writeFile();
    magia.reset();
    return;
  }

  if (line.includes(contem)) {
    return;
  }
  magia.parseLine(line);
}

function writeFile() {
  fs.appendFileSync(arquivo_saida, magia.toDart());
}
