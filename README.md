# Tagmar - Flutter

Aplicativo Android para o RPG de Tagmar.

Por ser em Flutter você pode tentar transpilar para web e disponibilizar em seu servidor ou pode compilar para Windows/Linux e rodar localmente.

## Desenvolvimento

Todo o projeto é feito em Flutter/Dart. Existe a possibilidade de adicionar C++, mas ainda não há necessidade para tal.

### Permissões

Arquivos do tipo .gitignore, Makefile, License.md e todos dentro dos diretórios `android` e `ios` serão modificados exclusivamente por mim.

- Em todos os casos de tentativa de alterar, o PR será cancelado na primeira vez.
- No caso de reincidência, o usuário será banido do projeto no git (se você quiser, faça um fork e siga o projeto com suas regras, afinal o código é aberto e livre).

Motivo: São arquivos  Não quero arriscar a segurança dos jogadores de Tagmar.

Caso encontre algum problema nos arquivos acima citados, abra uma issue no projeto. Aponte o problema (se souber a solução, explique adicionando referências) e eu resolvo.

## Leituras recomendadas

[Site Oficial](https://www.tagmar.com.br/).

[Atomic web design](https://bradfrost.com/blog/post/atomic-web-design/): talvez eu use no projeto.

## Como faço para ajudar no aplicativo?

Existem diversas formas de ajudar:

### Organização

- priorizando a lista de bugs;
- corrigindo erros gramaticais;
- atualizando o conteúdo para ficar igual aos textos oficiais;

### UX/UI

- melhorando interface do aplicativo;
- criando novas telas;

### Programação

Atualmente não estou aceitando ajuda. Estou usando o projeto pra aprender e quero montar uma base onde outros poderão seguir ajudando.

- corrigindo bugs;
  - crie um fork;
  - corrija o erro;
  - faça os testes;
    - se souber fazer teste unitário, melhor ainda;
  - abra um pedido de merge citando o bug que foi resolvido;
- implementando novas funcionalidades;
  - pode puxar um papo comigo pra a gente ver se é uma boa;
  - se for uma boa:
    - crie um fork;
    - implemente;
    - adicione comentários;
    - teste;
    - pedido de merge (de preferência citando a nova funcionalidade)
  - se eu não achar uma boa ideia (pode até ser boa, mas eu não queira por algum motivo):
    - crie um fork;
    - faça sua versão do app, o código é aberto e livre;

Semantic e atomic commits, testes unitários com 100% de cobertura, bem escrito (sem gambiarras), comentado e organizado (ver leituras recomendadas) ajudam a ter seu código aceito mais rapidamente.

Muita gente diz que código bom não precisa de comentário, mas comentar TODAS as funções descrevendo o que fazem ajuda todos a entenderem melhor cada parte do projeto.

### Outros

- Sugestões;
- Divulgação;
