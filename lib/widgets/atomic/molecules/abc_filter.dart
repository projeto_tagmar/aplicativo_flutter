import 'package:flutter/material.dart';
import 'package:diacritic/diacritic.dart' as diacritic;

class AbcFilter extends StatelessWidget {
  final List<String> names;
  final Function onSelected;
  final String? selectedString;

  /// construtor
  const AbcFilter({
    Key? key,
    required this.names,
    this.selectedString,
    required this.onSelected,
  }) : super(key: key);

  /// verifica se é o mesmo item
  bool _isSame(String c) {
    return diacritic.removeDiacritics(c) ==
        diacritic.removeDiacritics(selectedString?.toUpperCase() ?? "");
  }

  /// chamado quando clica em uma das letras
  void _onSelected(String c) {
    final output = _isSame(c) ? '' : c;
    onSelected(output);
  }

  Color _getColor(String c, BuildContext context) {
    return _isSame(c)
        ? Theme.of(context).primaryColor
        : Theme.of(context).indicatorColor;
  }

  /// cada um dos botões da lista
  Widget buildCharButton(String text, BuildContext context) {
    final color = _getColor(text, context);

    return Container(
      width: 40,
      margin: const EdgeInsets.symmetric(
        horizontal: 8,
        vertical: 22,
      ),
      child: ButtonTheme(
        height: 35,
        child: OutlinedButton(
          child: Text(
            text,
            style: TextStyle(color: color),
          ),
          style: OutlinedButton.styleFrom(
            side: BorderSide(
              width: 1,
              color: color,
            ),
          ),
          onPressed: () => _onSelected(text),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final Set<String> letras = {};
    letras.addAll(
        names.map((e) => diacritic.removeDiacritics(e[0].toUpperCase())));

    return SizedBox(
      height: 79,
      child: Row(
        children: [
          Expanded(
            child: Scrollbar(
              child: ListView.builder(
                itemCount: letras.length,
                scrollDirection: Axis.horizontal,
                itemBuilder: (ctx, i) =>
                    buildCharButton(letras.elementAt(i), context),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
