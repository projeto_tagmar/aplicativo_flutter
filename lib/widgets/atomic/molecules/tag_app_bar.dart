import "package:flutter/material.dart";

class TagAppBar extends AppBar {
  TagAppBar({
    Key? key,
    IconData? icon,
    String title = '',
    Function? onPressed,
    TextStyle? customStyle,
  }) : super(
          key: key,
          elevation: 0,
          actions: onPressed != null && icon != null
              ? <Widget>[
                  IconButton(onPressed: () => onPressed, icon: Icon(icon))
                ]
              : [],
          title: Text(
            title,
            style: customStyle,
          ),
          centerTitle: true,
        );
}
