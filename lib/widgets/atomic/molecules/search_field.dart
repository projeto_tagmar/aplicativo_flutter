import 'package:flutter/material.dart';

import 'package:tagmar/themes/tag_theme.dart';
import 'package:tagmar/widgets/atomic/atoms/debouncer.dart';

class SearchField extends StatefulWidget {
  final String hintText;
  final Function onChange;
  final int debounceMilli;
  final bool isKeySensitive;

  const SearchField({
    Key? key,
    required this.onChange,
    this.debounceMilli = 500,
    this.hintText = "Pesquisar",
    this.isKeySensitive = false,
  }) : super(key: key);

  @override
  _SearchFieldState createState() => _SearchFieldState();
}

class _SearchFieldState extends State<SearchField> {
  final _controller = TextEditingController();
  Debouncer? _debouncer;
  @override
  void initState() {
    super.initState();
    _debouncer = Debouncer(milliseconds: widget.debounceMilli);
    _controller.addListener(() {
      final String text = widget.isKeySensitive
          ? _controller.text
          : _controller.text.toLowerCase();

      _debouncer?.run(() {
        widget.onChange(text);
      });
    });
  }

  @override
  void dispose() {
    _controller.dispose();
    _debouncer?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: _controller,
      decoration: InputDecoration(
          hintText: widget.hintText,
          border: const OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10)),
            borderSide: BorderSide(width: 2),
          ),
          focusedBorder: const OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10)),
            borderSide: BorderSide(width: 2, color: TagTheme.strongBlue),
          ),
          suffixIcon: IconButton(
            onPressed: _controller.clear,
            icon: const Icon(Icons.clear),
          )),
    );
  }
}
