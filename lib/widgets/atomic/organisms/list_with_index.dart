import "package:flutter/material.dart";
import 'package:diacritic/diacritic.dart' as diacritic;

import 'package:tagmar/themes/tag_theme.dart';
import 'package:tagmar/calculations/string_ext.dart';
import 'package:tagmar/models/xtra/abstract_named_item.dart';
import 'package:tagmar/widgets/pages/magias/magia_screen.dart';
import 'package:tagmar/widgets/atomic/molecules/abc_filter.dart';

class ListWithIndex extends StatefulWidget {
  final List<AbstractNamedItem> items;

  const ListWithIndex({
    Key? key,
    required this.items,
  }) : super(key: key);

  @override
  State<ListWithIndex> createState() => _ListWithIndexState();
}

class _ListWithIndexState extends State<ListWithIndex> {
  String selected = '';

  /// ao selecionar uma opção, seta o estado
  void _onSelected(String c) {
    setState(() {
      selected = c;
    });
  }

  List<AbstractNamedItem> _getList() {
    if (selected != '') {
      return widget.items
          .where((AbstractNamedItem element) =>
              diacritic
                  .removeDiacritics(element.nome.toUpperCase()[0])
                  .compareTo(selected) ==
              0)
          .toList();
    } else {
      return widget.items;
    }
  }

  /// navega para a página passando um id
  void _goToPage(int id) {
    Navigator.of(context).pushNamed(
      MagiaScreen.routeName,
      arguments: id,
    );
  }

  /// cada item da lista
  Widget buildItem(AbstractNamedItem item) {
    return Card(
      key: Key("${item.id}_${item.nome}"),
      margin: const EdgeInsets.symmetric(vertical: 16),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      child: Container(
        padding: const EdgeInsets.symmetric(
          vertical: 8,
          horizontal: 16,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(item.nome.capitalize()),
            IconButton(
              color: TagTheme.orange,
              icon: const Icon(Icons.add),
              onPressed: () => _goToPage(item.id),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final list = _getList();
    return Expanded(
      child: Column(
        children: <Widget>[
          AbcFilter(
            onSelected: _onSelected,
            selectedString: selected,
            names: widget.items.map((e) => e.nome).toList(),
          ),
          Expanded(
            child: Scrollbar(
              child: list.isNotEmpty
                  ? ListView.builder(
                      shrinkWrap: true,
                      itemCount: list.length,
                      itemBuilder: (ctx, index) => buildItem(list[index]),
                    )
                  : const SingleChildScrollView(
                      child: Text("Vazio"),
                    ),
            ),
          ),
        ],
      ),
    );
  }
}
