import 'dart:async';

import 'package:flutter/foundation.dart';

// https://stackoverflow.com/questions/49238908/flutter-textfield-value-always-uppercase-debounce/64691040#64691040

class Debouncer {
  final int milliseconds;
  VoidCallback? action;
  Timer? timer;

  Debouncer({required this.milliseconds});

  run(VoidCallback action) {
    timer?.cancel();
    timer = Timer(Duration(milliseconds: milliseconds), action);
  }

  void dispose() {
    timer?.cancel();
    timer = null;
  }
}
