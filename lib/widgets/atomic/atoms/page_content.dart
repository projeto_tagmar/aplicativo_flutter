import "package:flutter/material.dart";

class PageContent extends StatelessWidget {
  final Widget child;
  const PageContent({Key? key, required this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16),
      child: child,
    );
  }
}
