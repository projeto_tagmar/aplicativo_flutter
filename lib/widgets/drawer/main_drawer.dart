import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart' as Foundation;
import 'package:package_info_plus/package_info_plus.dart';

import 'package:tagmar/models/drawer_item_model.dart';
import 'package:tagmar/widgets/drawer/drawer_item.dart';
import 'package:tagmar/widgets/pages/ficha_screen.dart';
import 'package:tagmar/widgets/pages/mapas_screen.dart';
import 'package:tagmar/widgets/pages/tecnicas_screen.dart';
import 'package:tagmar/widgets/pages/debug/debug_screen.dart';
import 'package:tagmar/widgets/pages/sobre/sobre_screen.dart';
import 'package:tagmar/widgets/pages/magias/magias_screen.dart';
import 'package:tagmar/widgets/pages/fichas/fichas_screen.dart';
import 'package:tagmar/widgets/pages/configuracoes/configuracoes_screen.dart';

class MainDrawer extends StatelessWidget {
  final String selectedRouteName;

  MainDrawer({Key? key, required this.selectedRouteName}) : super(key: key);

  final List<DrawerItemModel> _items = [
    DrawerItemModel(
      text: 'Fichas',
      route: FichasScreen.routeName,
    ),
    DrawerItemModel(
      text: 'Personagem atual',
      route: FichaScreen.routeName,
    ),
    DrawerItemModel(
      text: 'Magias',
      route: MagiasScreen.routeName,
    ),
    DrawerItemModel(
      text: 'Técnicas de combate',
      route: TecnicasScreen.routeName,
    ),
    DrawerItemModel(
      text: 'Mapas',
      route: MapasScreen.routeName,
    ),
    DrawerItemModel(
      text: 'Configurações',
      route: ConfiguracoesScreen.routeName,
    ),
    DrawerItemModel(
      text: 'Sobre',
      route: SobreScreen.routeName,
    ),
  ];

  /// O título apresentado na gaveta (futuramente exibirá os dados do usuário)
  Widget _buildListTile(
    String title,
    String route,
    BuildContext context,
  ) {
    final isSelected = route == selectedRouteName;

    return DrawerItem(
      route: route,
      title: title,
      isSelected: isSelected,
    );
  }

  Widget _buildTitle() {
    return const Text(
      'Tagmar',
      style: TextStyle(
        fontSize: 30,
        fontWeight: FontWeight.w900,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    if (Foundation.kDebugMode) {
      _items.add(DrawerItemModel(
        text: "Debug",
        route: DebugScreen.routeName,
      ));
    }

    return Drawer(
      child: Column(
        children: [
          Container(
            height: 120,
            width: double.infinity,
            padding: const EdgeInsets.all(16),
            alignment: Alignment.center,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                _buildTitle(),
                FutureBuilder(
                  future: PackageInfo.fromPlatform(),
                  builder: (context, snap) {
                    if (snap.hasError) {
                      return const Text("Erro ao buscar informação");
                    } else if (snap.hasData) {
                      final info = snap.data as PackageInfo;
                      final version = info.version;
                      return Text(version);
                    } else {
                      return const CircularProgressIndicator();
                    }
                  },
                ),
              ],
            ),
          ),
          ..._items
              .map((e) => _buildListTile(e.text, e.route, context))
              .toList(),
        ],
      ),
    );
  }
}
