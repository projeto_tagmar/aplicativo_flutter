import "package:flutter/material.dart";
import 'package:tagmar/themes/dark.dart';
import 'package:tagmar/themes/tag_theme.dart';

class DrawerItem extends StatelessWidget {
  final bool isSelected;
  final String title;
  final String route;
  const DrawerItem(
      {Key? key,
      this.isSelected = false,
      required this.title,
      required this.route})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Container(
        padding: const EdgeInsets.symmetric(
          vertical: 10,
          horizontal: 12,
        ),
        decoration: BoxDecoration(
          color: isSelected ? TagTheme.orange30 : null,
          borderRadius: const BorderRadius.all(Radius.circular(4)),
        ),
        child: Text(
          title,
          style: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.w600,
              color: isSelected ? TagTheme.orange : TagTheme.white),
        ),
      ),
      onTap: () {
        Navigator.of(context).pushReplacementNamed(route);
      },
    );
  }
}
