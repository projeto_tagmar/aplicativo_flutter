import 'package:flutter/material.dart';

import 'package:tagmar/models/magia/nivel.dart';
import 'package:tagmar/widgets/pages/magias/nivel_header.dart';
import 'package:tagmar/widgets/pages/magias/magia_nivel_secundario.dart';

class MagiaNivelListItem extends StatelessWidget {
  final Nivel nivel;
  final String nome;

  const MagiaNivelListItem({
    Key? key,
    required this.nome,
    required this.nivel,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final color = Theme.of(context).primaryColor;
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text("• "),
              Expanded(
                child: Text.rich(
                  TextSpan(
                    text: "",
                    children: <TextSpan>[
                      TextSpan(
                        text: "$nome ${nivel.nivel}: ",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: color,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
          NivelHeader(nivel: nivel),
          Text(nivel.descricao.trim()),
          if (nivel.secundario != null)
            MagiaNivelSecundario(nivel: nivel.secundario!)
        ],
      ),
    );
  }
}
