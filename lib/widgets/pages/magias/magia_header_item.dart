import 'package:flutter/material.dart';

class MagiaHeaderItem extends StatelessWidget {
  final String label;
  final String text;
  const MagiaHeaderItem({
    Key? key,
    required this.text,
    required this.label,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(
        vertical: 8,
      ),
      child: Text.rich(
        TextSpan(
          text: "",
          children: [
            TextSpan(
              text: label,
              style: const TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
            TextSpan(text: text),
          ],
        ),
      ),
    );
  }
}
