import 'package:flutter/material.dart';

import 'package:provider/provider.dart';
import 'package:diacritic/diacritic.dart' as diacritic;

import 'package:tagmar/database/magias.dart';
import 'package:tagmar/util/string_help.dart';
import 'package:tagmar/widgets/drawer/main_drawer.dart';
import 'package:tagmar/models/xtra/abstract_named_item.dart';
import 'package:tagmar/widgets/atomic/atoms/page_content.dart';
import 'package:tagmar/widgets/atomic/molecules/tag_app_bar.dart';
import 'package:tagmar/widgets/atomic/molecules/search_field.dart';
import 'package:tagmar/widgets/atomic/organisms/list_with_index.dart';

class MagiasScreen extends StatefulWidget {
  static const routeName = '/magias';

  const MagiasScreen({Key? key}) : super(key: key);

  @override
  State<MagiasScreen> createState() => _MagiasScreenState();
}

class _MagiasScreenState extends State<MagiasScreen> {
  String _needle = "";

  @override
  Widget build(BuildContext context) {
    final magias = Provider.of<Magias>(context, listen: false).all;

    magias.sort((AbstractNamedItem a, AbstractNamedItem b) => diacritic
        .removeDiacritics(a.nome)
        .compareTo(diacritic.removeDiacritics(b.nome)));

    return Scaffold(
      appBar: TagAppBar(
        title: 'Magias',
      ),
      drawer: MainDrawer(
        selectedRouteName: MagiasScreen.routeName,
      ),
      body: PageContent(
        child: Column(
          children: <Widget>[
            SearchField(onChange: (text) {
              setState(() {
                _needle = text;
              });
            }),
            ListWithIndex(
              items: _needle.isEmpty
                  ? magias
                  : magias
                      .where((element) => compareWordWithWordsInSentence(
                          _needle, element.nome, 0.8))
                      .toList(),
            ),
          ],
        ),
      ),
    );
  }
}
