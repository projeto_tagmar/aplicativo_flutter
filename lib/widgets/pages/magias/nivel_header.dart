import 'package:flutter/material.dart';

import 'package:tagmar/models/magia/nivel.dart';
import 'package:tagmar/widgets/pages/magias/magia_header_item.dart';

class NivelHeader extends StatelessWidget {
  final Nivel nivel;
  const NivelHeader({Key? key, required this.nivel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<Widget> headers = [
      if (nivel.alcance != null)
        MagiaHeaderItem(
            text: nivel.alcance!.toString(), label: nivel.alcance!.label),
      if (nivel.duracao != null)
        MagiaHeaderItem(
            text: nivel.duracao!.toString(), label: nivel.duracao!.label),
      if (nivel.evocacao != null)
        MagiaHeaderItem(
          text: nivel.evocacao!.toString(),
          label: nivel.evocacao!.label,
        ),
      if (nivel.custo != null)
        MagiaHeaderItem(text: nivel.custo.toString(), label: "Custo: "),
    ];
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: headers,
      ),
    );
  }
}
