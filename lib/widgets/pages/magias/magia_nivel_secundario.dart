import 'package:flutter/material.dart';

import 'package:tagmar/models/magia/nivel.dart';
import 'package:tagmar/widgets/pages/magias/nivel_header.dart';

class MagiaNivelSecundario extends StatelessWidget {
  final Nivel nivel;
  const MagiaNivelSecundario({Key? key, required this.nivel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text.rich(
            TextSpan(
              children: [
                const TextSpan(
                  text: "Efeito secundário: ",
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                  ),
                ),
                TextSpan(text: nivel.descricao),
              ],
            ),
          ),
          NivelHeader(nivel: nivel)
        ],
      ),
    );
  }
}
