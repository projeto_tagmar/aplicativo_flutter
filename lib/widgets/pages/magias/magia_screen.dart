import 'package:flutter/material.dart';

import 'package:provider/provider.dart';

import 'package:tagmar/database/magias.dart';
import 'package:tagmar/themes/tag_theme.dart';
import 'package:tagmar/models/magia/magia.dart';
import 'package:tagmar/models/magia/nivel.dart';
import 'package:tagmar/widgets/atomic/atoms/page_content.dart';
import 'package:tagmar/widgets/atomic/molecules/tag_app_bar.dart';
import 'package:tagmar/widgets/pages/magias/magia_header_item.dart';
import 'package:tagmar/widgets/pages/magias/magia_nivel_list_item.dart';

class MagiaScreen extends StatelessWidget {
  static const routeName = '/magia';
  final bold = const TextStyle(fontWeight: FontWeight.bold);

  const MagiaScreen({Key? key}) : super(key: key);

  /// o cabeçalho
  List<Widget> _buildHeader(Magia magia) {
    final output = [
      MagiaHeaderItem(
          text: magia.alcance.toString(), label: magia.alcance.label),
      MagiaHeaderItem(
          text: magia.duracao.toString(), label: magia.duracao.label),
      MagiaHeaderItem(
          text: magia.evocacao.toString(), label: magia.evocacao.label),
    ];

    if (magia.custo != null) {
      output
          .add(MagiaHeaderItem(text: magia.custo.toString(), label: "Custo: "));
    }

    return output;
  }

  /// cada um dos elementos expansíveis
  ExpansionTile _buildExpansion(String text, List<Widget> children) {
    return ExpansionTile(
      title: Text(
        text,
        style: bold,
      ),
      children: children,
    );
  }

  /// os níveis da magia
  List<MagiaNivelListItem> _buildNivels(Magia magia) {
    return magia.niveis
        .map((Nivel e) => MagiaNivelListItem(
              nivel: e,
              nome: magia.nome,
            ))
        .toList();
  }

  /// Observações extras
  List<Widget> _buildExtra(Magia magia) {
    return magia.extra!.map((e) => Text(e)).toList();
  }

  @override
  Widget build(BuildContext context) {
    final int magiaId = ModalRoute.of(context)?.settings.arguments as int;

    final Magia magia =
        Provider.of<Magias>(context, listen: false).findById(magiaId);

    return Scaffold(
      appBar: TagAppBar(
        title: magia.nome,
        customStyle: TextStyle(
          color: Theme.of(context).primaryColor,
        ),
      ),
      body: PageContent(
        child: Scrollbar(
          child: SingleChildScrollView(
            child: Container(
              decoration: const BoxDecoration(
                color: TagTheme.darkGrey,
                borderRadius: BorderRadius.all(
                  Radius.circular(10),
                ),
              ),
              child: Padding(
                padding: const EdgeInsets.all(12),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    ..._buildHeader(magia),
                    _buildExpansion("Descrição", [Text(magia.descricao)]),
                    if (magia.extra != null)
                      _buildExpansion("Observações", _buildExtra(magia)),
                    _buildExpansion("Níveis", _buildNivels(magia)),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
