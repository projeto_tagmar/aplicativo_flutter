import 'package:flutter/material.dart';

import 'package:tagmar/widgets/drawer/main_drawer.dart';
import 'package:tagmar/widgets/atomic/atoms/page_content.dart';
import 'package:tagmar/widgets/atomic/molecules/tag_app_bar.dart';

class MapasScreen extends StatelessWidget {
  static const routeName = '/mapas';

  const MapasScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: TagAppBar(
        title: 'Mapas',
      ),
      drawer: MainDrawer(
        selectedRouteName: MapasScreen.routeName,
      ),
      body: const PageContent(child: Text('Mapas')),
    );
  }
}
