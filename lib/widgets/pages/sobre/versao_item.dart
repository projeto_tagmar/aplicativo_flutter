import 'package:flutter/material.dart';

import 'package:tagmar/models/versao/versao.dart';

class VersaoItem extends StatelessWidget {
  final Versao versao;
  const VersaoItem({Key? key, required this.versao}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text("Versão ${versao.version}"),
        ...versao.changes.map((e) => Text(e)).toList(),
        const Text("")
      ],
    );
  }
}
