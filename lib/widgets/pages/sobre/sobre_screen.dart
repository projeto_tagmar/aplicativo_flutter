import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:tagmar/database/version_log.dart';
import 'package:tagmar/widgets/pages/sobre/versao_item.dart';
import 'package:url_launcher/url_launcher.dart' as url_launcher;

import 'package:tagmar/widgets/drawer/main_drawer.dart';
import 'package:tagmar/widgets/atomic/atoms/page_content.dart';
import 'package:tagmar/widgets/atomic/molecules/tag_app_bar.dart';

class SobreScreen extends StatelessWidget {
  static const routeName = '/sobre';

  const SobreScreen({Key? key}) : super(key: key);

  /// Habilita abrir com toque
  Future _abrirLink(String url) async {
    if (await url_launcher.canLaunch(url)) {
      await url_launcher.launch(url);
    } else {
      throw 'Erro ao abrir site: $url';
    }
  }

  /// Cria um texto tocável
  RichText _buildLink(String alias, String url) {
    return RichText(
      text: TextSpan(
        children: [
          TextSpan(
            text: alias,
            recognizer: TapGestureRecognizer()..onTap = () => _abrirLink(url),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: TagAppBar(
        title: 'Sobre',
      ),
      drawer: MainDrawer(
        selectedRouteName: SobreScreen.routeName,
      ),
      body: PageContent(
        child: Center(
          child: Scrollbar(
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  const Text("Tagmar"),
                  _buildLink("Site oficial", "https://tagmar.com.br"),
                  const Text(""),
                  const Text("Código"),
                  _buildLink("Conversão de/para JSON",
                      "https://ashamp.github.io/jsonToDartModel/"),
                  _buildLink("Cálculo de cores",
                      "https://medium.com/@filipvk/creating-a-custom-color-swatch-in-flutter-554bcdcb27f3"),
                  _buildLink("Comparação de palavras",
                      "https://www.geeksforgeeks.org/jaro-and-jaro-winkler-similarity/"),
                  const Text(""),
                  const Text("Fontes"),
                  _buildLink(
                      "Poppins", "https://www.fontsquirrel.com/fonts/poppins"),
                  const Text(""),
                  const Text("Acessoria técnica em Tagmar"),
                  const Text("Marcelo e Edilton"),
                  const Text(""),
                  const Text("Testadores"),
                  const Text(
                      "Álvaro, Breno, Edilton, Marcelo, Petrus, Rodolfo, Sérgio"),
                  const Text(""),
                  ...VersionLog.versoes
                      .map((ver) => VersaoItem(versao: ver))
                      .toList()
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
