import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:tagmar/database/fichas.dart';
import 'package:tagmar/widgets/pages/fichas/lista/list_item_ficha.dart';

class ListaFichas extends StatelessWidget {
  const ListaFichas({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final fichas = Provider.of<Fichas>(context).all;
    return Expanded(
      child: Scrollbar(
        child: ListView.builder(
          itemCount: fichas.length,
          itemBuilder: (ctx, i) {
            return ListItemFicha(ficha: fichas[i]);
          },
        ),
      ),
    );
  }
}
