import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:tagmar/database/fichas.dart';
import 'package:tagmar/themes/tag_theme.dart';
import 'package:tagmar/models/ficha/ficha.dart';
import 'package:tagmar/models/configurations.dart';
import 'package:tagmar/widgets/pages/fichas/confirmar_apagar.dart';

class ListItemFicha extends StatelessWidget {
  final Ficha ficha;
  const ListItemFicha({Key? key, required this.ficha}) : super(key: key);

  /// cria o ícone temporário (depois será substituído por ícone de classe e raça)
  Widget buildIcon() {
    return Container(
      width: 31,
      height: 31,
      decoration: const BoxDecoration(
        color: TagTheme.orange30,
        borderRadius: BorderRadius.all(
          Radius.circular(6),
        ),
      ),
    );
  }

  /// exibe as informações resumidas da ficha
  Widget buildInfo() {
    final criacao = DateFormat('dd/MM/yyyy').format(ficha.criacao);
    return Row(
      children: [
        buildIcon(),
        Column(
          children: [
            Text("Nome: ${ficha.nome}"),
            Text("Nível: ${ficha.nome}"),
            Text("Experiência: ${ficha.nome}"),
            Text("Criacao: $criacao"),
            Text("Narrador: ${ficha.narrador}"),
          ],
        ),
        buildIcon(),
      ],
      mainAxisAlignment: MainAxisAlignment.spaceAround,
    );
  }

  /// botões da parte inferior
  Widget buildButtons(BuildContext context) {
    return Row(
      children: [
        const ElevatedButton(
          onPressed: null,
          child: Icon(Icons.remove_red_eye_outlined),
        ),
        const ElevatedButton(
          onPressed: null,
          child: Icon(Icons.edit),
        ),
        const ElevatedButton(
          onPressed: null,
          child: Icon(Icons.star_border),
        ),
        const ElevatedButton(
          onPressed: null,
          child: Icon(Icons.archive_outlined),
        ),
        OutlinedButton(
          onPressed: () {
            final fichas = Provider.of<Fichas>(context, listen: false);
            showDialog(
              context: context,
              builder: (_) => ConfirmarApagar(onAction: (bool val) {
                if (val) fichas.removeById(ficha.id);
              }),
            );
          },
          child: const Icon(Icons.delete),
        ),
      ],
      mainAxisAlignment: MainAxisAlignment.spaceAround,
    );
  }

  /// componente
  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<Configuracoes>(context, listen: false);
    return Container(
      margin: const EdgeInsets.only(bottom: 16),
      decoration: BoxDecoration(
        color: provider.isDark ? TagTheme.dark70 : TagTheme.white138,
        borderRadius: const BorderRadius.all(
          Radius.circular(5),
        ),
      ),
      padding: const EdgeInsets.symmetric(
        vertical: 20,
        horizontal: 17,
      ),
      child: Column(
        children: [
          buildInfo(),
          buildButtons(context),
        ],
        mainAxisAlignment: MainAxisAlignment.spaceAround,
      ),
    );
  }
}
