import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:tagmar/database/fichas.dart';
import 'package:tagmar/widgets/drawer/main_drawer.dart';
import 'package:tagmar/widgets/atomic/atoms/page_content.dart';
import 'package:tagmar/widgets/atomic/molecules/tag_app_bar.dart';
import 'package:tagmar/widgets/pages/fichas/lista/list_fichas.dart';

@immutable
class FichasScreen extends StatelessWidget {
  static const routeName = '/';

  static const emptyImage = 'assets/images/empty.png';
  static const notEmptyImage = 'assets/images/some.png';

  const FichasScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final fichas = Provider.of<Fichas>(context);

    /// Gera o botão da parte inferior da página
    Widget buildButton() {
      return ElevatedButton(
        onPressed: fichas.mockOne,
        child: const Text("Adicionar ficha"),
      );
    }

    /// Define o visual da tela
    BoxDecoration buildDecoration() {
      return BoxDecoration(
        image: DecorationImage(
          image: fichas.isEmpty
              ? const AssetImage(emptyImage)
              : const AssetImage(notEmptyImage),
          fit: BoxFit.cover,
          alignment: Alignment.center,
        ),
      );
    }

    MainAxisAlignment buildAlignment() {
      return fichas.isEmpty
          ? MainAxisAlignment.end
          : MainAxisAlignment.spaceBetween;
    }

    return Scaffold(
      appBar: TagAppBar(
        title: "Fichas",
      ),
      drawer: MainDrawer(
        selectedRouteName: FichasScreen.routeName,
      ),
      body: Container(
        width: double.infinity,
        height: double.infinity,
        decoration: buildDecoration(),
        child: PageContent(
          child: Column(
            children: <Widget>[
              const ListaFichas(),
              buildButton(),
            ],
            mainAxisAlignment: buildAlignment(),
          ),
        ),
      ),
    );
  }
}
