import 'package:flutter/material.dart';

import 'package:tagmar/themes/tag_theme.dart';

class ConfirmarApagar extends StatelessWidget {
  final Function onAction;
  const ConfirmarApagar({Key? key, required this.onAction}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: const Text('Confirmação'),
      content: const Text('Tem certeza que deseja apagar a ficha?'),
      actions: [
        OutlinedButton(
          onPressed: () {
            onAction(true);
            Navigator.pop(context);
          },
          child: const Text("Confirmar"),
        ),
        ElevatedButton(
          onPressed: () {
            onAction(false);
            Navigator.pop(context);
          },
          child: const Text("Voltar"),
        ),
      ],
      elevation: 0,
      backgroundColor: TagTheme.darkGrey,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(5),
      ),
    );
  }
}
