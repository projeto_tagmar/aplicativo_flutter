import 'package:flutter/material.dart';

import 'package:tagmar/widgets/drawer/main_drawer.dart';
import 'package:tagmar/widgets/atomic/atoms/page_content.dart';
import 'package:tagmar/widgets/atomic/molecules/tag_app_bar.dart';

class TecnicasScreen extends StatelessWidget {
  static const routeName = '/tecnicas';

  const TecnicasScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: TagAppBar(
        title: 'Técnicas',
      ),
      drawer: MainDrawer(
        selectedRouteName: TecnicasScreen.routeName,
      ),
      body: const PageContent(child: Text('técnicas')),
    );
  }
}
