import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:tagmar/models/configurations.dart';
import 'package:tagmar/themes/tag_theme.dart';
import 'package:tagmar/widgets/drawer/main_drawer.dart';
import 'package:tagmar/widgets/atomic/atoms/page_content.dart';
import 'package:tagmar/widgets/atomic/molecules/tag_app_bar.dart';

class ConfiguracoesScreen extends StatefulWidget {
  static const routeName = '/configuracoes';

  const ConfiguracoesScreen({Key? key}) : super(key: key);

  @override
  State<ConfiguracoesScreen> createState() => _ConfiguracoesScreenState();
}

class _ConfiguracoesScreenState extends State<ConfiguracoesScreen> {
  Widget _buildOptions(BuildContext context) {
    final provider = Provider.of<Configuracoes>(context);

    return Column(
      children: [
        const Text("Dados"),
        SwitchListTile(
          value: provider.isCheckUpdate,
          // onChanged: (bool val) => provider.isCheckUpdate = val,
          onChanged: null,
          title: const Text("Verificar atualizações ao iniciar"),
          subtitle: const Text("Baixar dados oficiais mais atualizados"),
        ),
        SwitchListTile(
          value: provider.isWifiOnly,
          // onChanged: provider.isCheckUpdate
          //     ? (bool val) => provider.isWifiOnly = val
          //     : null,
          onChanged: null,
          title: const Text("Baixar somente com Wi-Fi"),
          subtitle: const Text("Atualiza somente se conectado ao Wi-Fi"),
        ),
        const Text("Visual"),
        SwitchListTile(
          onChanged: null,
          value: provider.isAnimate,
          title: const Text("Animar"),
          // onChanged: (bool val) => provider.isAnimate = val,
        ),
        SwitchListTile(
          value: provider.isDark,
          title: const Text("Tema escuro"),
          subtitle: const Text("App com ênfase no tema escuro"),
          onChanged: (bool val) => provider.isDark = val,
          activeColor: TagTheme.orange,
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: TagAppBar(
        title: "Configurações",
      ),
      drawer: MainDrawer(
        selectedRouteName: ConfiguracoesScreen.routeName,
      ),
      body: PageContent(child: _buildOptions(context)),
    );
  }
}
