import 'package:flutter/material.dart';

import 'package:tagmar/database/racas.dart';
import 'package:tagmar/database/profissoes.dart';
import 'package:tagmar/database/habilidades.dart';
import 'package:tagmar/widgets/drawer/main_drawer.dart';
import 'package:tagmar/widgets/atomic/atoms/page_content.dart';
import 'package:tagmar/widgets/atomic/molecules/tag_app_bar.dart';

class DebugScreen extends StatelessWidget {
  static const routeName = '/debug';
  const DebugScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: TagAppBar(
        title: 'Debug',
      ),
      drawer: MainDrawer(
        selectedRouteName: DebugScreen.routeName,
      ),
      body: PageContent(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const Text("-----raças-----"),
              ...Racas.racas
                  .map(
                    (e) => Text("${e.nome} ${e.altura}m ${e.peso}kg"),
                  )
                  .toList(),
              const Text("-----profissões-----"),
              ...Profissoes.profissoes
                  .map(
                    (e) => Text(e.nome),
                  )
                  .toList(),
              const Text("-----habilidades-----"),
              ...Habilidades.habilidades.map((e) => Text(e.nome)).toList(),
            ],
          ),
        ),
      ),
    );
  }
}
