import 'package:flutter/material.dart';

import 'package:tagmar/widgets/drawer/main_drawer.dart';
import 'package:tagmar/widgets/atomic/atoms/page_content.dart';
import 'package:tagmar/widgets/atomic/molecules/tag_app_bar.dart';

class FichaScreen extends StatelessWidget {
  static const routeName = '/ficha';

  const FichaScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: TagAppBar(
        title: 'Ficha',
      ),
      drawer: MainDrawer(
        selectedRouteName: FichaScreen.routeName,
      ),
      body: const PageContent(child: Text('ficha')),
    );
  }
}
