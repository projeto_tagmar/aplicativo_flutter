import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:tagmar/app.dart';
import 'package:tagmar/database/fichas.dart';
import 'package:tagmar/database/magias.dart';
import 'package:tagmar/models/configurations.dart';

void main() {
  runApp(const Tagmar());
}

class Tagmar extends StatelessWidget {
  const Tagmar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => Magias()),
        ChangeNotifierProvider(create: (_) => Fichas()),
        ChangeNotifierProvider(create: (_) => Configuracoes()),
      ],
      child: const App(),
    );
  }
}
