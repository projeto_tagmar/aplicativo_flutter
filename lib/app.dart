import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter/foundation.dart' as Foundation;

import 'package:tagmar/themes/dark.dart';
import 'package:tagmar/themes/light.dart';
import 'package:tagmar/models/configurations.dart';
import 'package:tagmar/widgets/pages/debug/debug_screen.dart';
import 'package:tagmar/widgets/pages/ficha_screen.dart';
import 'package:tagmar/widgets/pages/mapas_screen.dart';
import 'package:tagmar/widgets/pages/tecnicas_screen.dart';
import 'package:tagmar/widgets/pages/sobre/sobre_screen.dart';
import 'package:tagmar/widgets/pages/magias/magia_screen.dart';
import 'package:tagmar/widgets/pages/magias/magias_screen.dart';
import 'package:tagmar/widgets/pages/fichas/fichas_screen.dart';
import 'package:tagmar/widgets/pages/configuracoes/configuracoes_screen.dart';

class App extends StatefulWidget {
  const App({Key? key}) : super(key: key);

  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  @override
  MaterialApp build(BuildContext context) {
    final provider = Provider.of<Configuracoes>(context);
    provider.loadAll();

    var routes = {
      MagiaScreen.routeName: (ctx) => const MagiaScreen(),
      MapasScreen.routeName: (ctx) => const MapasScreen(),
      SobreScreen.routeName: (ctx) => const SobreScreen(),
      FichaScreen.routeName: (ctx) => const FichaScreen(),
      MagiasScreen.routeName: (ctx) => const MagiasScreen(),
      FichasScreen.routeName: (ctx) => const FichasScreen(),
      TecnicasScreen.routeName: (ctx) => const TecnicasScreen(),
      ConfiguracoesScreen.routeName: (ctx) => const ConfiguracoesScreen(),
    };

    if (Foundation.kDebugMode) {
      routes.addAll({
        DebugScreen.routeName: (ctx) => const DebugScreen(),
      });
    }

    return MaterialApp(
      title: 'Tagmar',
      theme: provider.isDark ? ThemeDark.theme : ThemeLight.theme,
      initialRoute: FichasScreen.routeName,
      routes: routes,
    );
  }
}
