/// Primeira letra, de cada palavra, em maiúsculo
extension StringExtension on String {
  String capitalize() {
    if (length <= 1) {
      return toUpperCase();
    }

    // Quebra em palavras
    final List<String> words = split(' ');

    // Deixa a primeira de cada em maiúsculo
    final capitalizedWords = words.map((word) {
      if (word.trim().isNotEmpty) {
        final String firstLetter = word.trim().substring(0, 1).toUpperCase();
        final String remainingLetters = word.trim().substring(1);

        return '$firstLetter$remainingLetters';
      }
      return '';
    });

    // Junta de volta
    return capitalizedWords.join(' ');
  }
}
