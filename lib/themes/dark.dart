import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:tagmar/themes/tag_theme.dart';
import 'package:tagmar/themes/color_calc.dart';

class ThemeDark extends TagTheme {
  static ThemeData get theme {
    final MaterialColor calculated = createMaterialColor(TagTheme.orange);
    return ThemeData.from(
      colorScheme: ColorScheme.fromSwatch(
        primarySwatch: calculated,
        cardColor: TagTheme.darkBg,
        brightness: Brightness.dark,
        accentColor: TagTheme.orange,
        primaryColorDark: calculated,
        errorColor: TagTheme.errorRed,
        backgroundColor: TagTheme.darkBg,
      ),
      textTheme: TagTheme.textTheme,
    ).copyWith(
      appBarTheme: const AppBarTheme(
        elevation: 0,
        systemOverlayStyle: SystemUiOverlayStyle(
          statusBarBrightness: Brightness.dark,
        ),
        toolbarHeight: 58,
      ),
      primaryColor: TagTheme.orange,
    );
  }
}
