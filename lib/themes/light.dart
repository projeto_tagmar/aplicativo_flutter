import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:tagmar/themes/tag_theme.dart';
import 'package:tagmar/themes/color_calc.dart';

class ThemeLight extends TagTheme {
  static ThemeData get theme {
    final MaterialColor calculated = createMaterialColor(TagTheme.orange);
    return ThemeData.from(
      colorScheme: ColorScheme.fromSwatch(
        primarySwatch: calculated,
        cardColor: TagTheme.white,
        brightness: Brightness.light,
        accentColor: TagTheme.orange,
        primaryColorDark: calculated,
        errorColor: TagTheme.errorRed,
        backgroundColor: TagTheme.white,
      ),
      textTheme: TagTheme.textTheme,
    ).copyWith(
      appBarTheme: const AppBarTheme(
        elevation: 0,
        systemOverlayStyle: SystemUiOverlayStyle(
          statusBarBrightness: Brightness.light,
        ),
        toolbarHeight: 58,
      ),
    );
  }
}
