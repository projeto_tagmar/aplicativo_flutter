import 'package:flutter/material.dart';

class TagTheme {
  static const white = Colors.white; // #ffffff
  static const black = Colors.black; // #000000
  static const darkBg = Color.fromARGB(255, 18, 18, 18); // #121212
  static const darkGrey = Color.fromARGB(255, 42, 42, 42); // #2a2a2a
  static const white138 = Color.fromARGB(138, 255, 255, 255); // #ffffff
  static const dark70 = Color.fromARGB(178, 18, 18, 18); // #121212 70%

  static const orange = Color.fromARGB(255, 255, 172, 21); // #ffac3d
  static const lightOrange = Color.fromARGB(255, 255, 197, 119); // #ffc577
  static const orange30 = Color.fromARGB(76, 243, 217, 181); // #F3D9B5 30%

  static const lightBlue = Color.fromARGB(255, 186, 223, 251); // #BADFFB
  static const strongBlue = Color.fromARGB(255, 112, 176, 224); // #70B0E0

  static const errorRed = Color.fromARGB(255, 211, 47, 47);

  static const _poppins = TextStyle(fontFamily: "Poppins");

  static const textTheme = TextTheme(
    bodyText1: _poppins,
    bodyText2: _poppins,
    button: _poppins,
    caption: _poppins,
    headline1: _poppins,
    headline2: _poppins,
    headline3: _poppins,
    headline4: _poppins,
    headline5: _poppins,
    headline6: _poppins,
    overline: _poppins,
    subtitle1: _poppins,
    subtitle2: _poppins,
  );
}
