import 'dart:math' as math;
import 'package:diacritic/diacritic.dart' as diacritic;

// https://www.geeksforgeeks.org/jaro-and-jaro-winkler-similarity/

/// Compara strings dando margem de diferença
double jaroDistance(String s1, String s2) {
  if (s1.compareTo(s2) == 0) return 1;

  // Length of two Strings
  final int len1 = s1.length, len2 = s2.length;

  // Maximum distance upto which matching
  // is allowed
  final int maxDistance = (math.max(len1, len2) / 2).floor() - 1;

  // Count of matches
  int match = 0;

  // Hash for matches
  final List<int> hashS1 = List.filled(len1, 0);
  final List<int> hashS2 = List.filled(len2, 0);

  // Traverse through the first String
  for (int i = 0; i < len1; i++) {
    // Check if there is any matches
    for (int j = math.max(0, i - maxDistance);
        j < math.min(len2, i + maxDistance + 1);
        j++) {
      // If there is a match
      if ((s1[i] == s2[j]) && (hashS2[j] == 0)) {
        hashS1[i] = 1;
        hashS2[j] = 1;
        match++;
        break;
      }
    }
  }

  // If there is no match
  if (match == 0) {
    return 0.0;
  }

  // Number of transpositions
  double t = 0;

  int point = 0;

  // Count number of occurrences
  // where two characters match but
  // there is a third matched character
  // in between the indices
  for (int i = 0; i < len1; i++) {
    if (hashS1[i] == 1) {
      // Find the next matched character
      // in second String
      while (hashS2[point] == 0) {
        point++;
      }

      if (s1[i] != s2[point++]) {
        t++;
      }
    }
  }

  t /= 2;

  // Return the Jaro Similarity
  return ((match.toDouble()) / (len1.toDouble()) +
          (match.toDouble()) / (len2.toDouble()) +
          (match.toDouble() - t) / (match.toDouble())) /
      3.0;
}

/// Calcula a marge de diferença entre duas palavras.
double jaroWinkler(String a, String b) {
  double calculatedJaroDistance = jaroDistance(a, b);

  // If the jaro Similarity is above a threshold
  if (calculatedJaroDistance > 0.7) {
    // Find the length of common prefix
    int prefix = 0;

    for (int i = 0; i < math.min(a.length, b.length); i++) {
      // If the characters match
      if (a[i] == b[i]) {
        prefix++;
      }

      // Else break
      else {
        break;
      }
    }

    // Maximum of 4 characters are allowed in prefix
    prefix = math.min(4, prefix);

    // Calculate jaro winkler Similarity
    calculatedJaroDistance += 0.1 * prefix * (1 - calculatedJaroDistance);
  }
  return calculatedJaroDistance;
}

/// Compara duas palavras [a] e [b] dando um valor mínimo de [similarity] (0 a 1)
bool compareWords(String a, String b, double similarity) {
  final _a = diacritic.removeDiacritics(a).toLowerCase();
  final _b = diacritic.removeDiacritics(b).toLowerCase();

  return _b.contains(_a) || jaroWinkler(_a, _b) >= similarity;
}

/// Compara [a] com as palavras contidas na [sentence] dada uma [similarity] (0 a 1)
bool compareWordWithWordsInSentence(
    String a, String sentence, double similarity) {
  if (a.length < 3) {
    return false;
  }

  return sentence.split(" ").any((element) {
    if (element.length < 3) {
      return false;
    }
    return compareWords(a, element, similarity);
  });
}
