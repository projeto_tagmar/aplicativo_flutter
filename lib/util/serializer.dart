import 'package:tagmar/models/xtra/abstract_indexable_item.dart';

abstract class Serializer<T extends AbstractIndexableItem> {
  Map<String, dynamic> toJson(T object);
  T fromJson(Map<String, dynamic> json);
}
