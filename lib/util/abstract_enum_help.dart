import 'package:tagmar/calculations/string_ext.dart';

abstract class AbstractEnumHelp<T extends Enum> {
  final T tipo;
  final int? valor;

  const AbstractEnumHelp({this.valor = 0, required this.tipo});

  bool get _isPlural {
    return valor != null && valor! > 1;
  }

  String _plural(String unit) {
    if (!_isPlural) {
      return unit;
    } else if (unit == "mês") {
      return "meses";
    } else {
      return "${unit}s";
    }
  }

  String getValueString(String unit) {
    int tempValue = valor ?? 0;
    String val = tempValue > 0 ? tempValue.toString() : "";

    return "$val ${_plural(unit)}";
  }

  String get label {
    return "${name.capitalize()}: ";
  }

  String get name;
  String get selectedName;

  @override
  String toString() {
    return getValueString(selectedName);
  }
}
