import 'package:tagmar/enums/enum_atributo.dart';
import 'package:tagmar/models/tecnica/tecnica.dart';
import 'package:tagmar/enums/enum_tecnicas_combate.dart';
import 'package:tagmar/enums/enum_tecnica_categoria.dart';

class TecnicasDeCombate {
  static List<TecnicaDeCombate> lista = [
    TecnicaDeCombate(
      id: EnumTecnicasDeCombate.ajustarDisparo.index,
      nome: "Ajustar Disparo",
      mecanica:
          '''Depois de errar um alvo, o arqueiro soma seu total em Ajustar disparo nas colunas do próximo Disparo.

Obs: O tempo de intervalo entre o disparo perdido e o disparo ajustado não pode ultrapassar a rodada após o uso da técnica para que o combatente receba os benefícios desta técnica''',
      atributo: EnumAtributo.percepcao,
      categoria: EnumTecnicaCategoria.precisao,
      aprimoramento:
          'Aprimorados nesta técnica podem usá-la após uma técnica defensiva do inimigo que tenha tornado inefetivo (anulado) completamente um ataque seu.',
      requisitos: [
        EnumTecnicasDeCombate.mira,
        EnumTecnicasDeCombate.imprevisibilidade,
      ],
    )
  ];
}
