import 'package:flutter/cupertino.dart';

import 'package:tagmar/models/profissao/profissao.dart';
import 'package:tagmar/enums/enum_codigo_habilidade.dart';

class Profissoes with ChangeNotifier {
  static const List<Profissao> profissoes = [
    Profissao(
      id: 0,
      nome: "Bardo",
      ehBasica: 9,
      racasNaoPermitidas: [0],
      habilidadeIdAperfeicoada: 15,
      pontosAquisicaoHabilidade: 16,
      descricao:
          '''Com espírito livre e fala amistosa, os Bardos se tornaram os principais veículos de informação no Mundo Conhecido. Tendo na Arte o seu maior poder, eles conseguem cativar pessoas facilmente, sendo muito bem recebidos na maior parte dos reinos. Sua habilidade de lidar com pessoas é tal que os Bardos se fazem muito úteis na grande maioria dos acontecimentos, desde os mais corriqueiros aos mais estrondosos.

Algo que unifica todos os Bardos é a Arte: todos eles precisam ter conhecimento de pelo menos uma expressão artística. A mais comum delas é a música, já que ela incita a dança (outra expressão artística) e é portátil, diferentemente da pintura e escultura. Esse também é o principal motivo pelos quais Bardos são raros: o ensino da arte geralmente só é concedido às classes mais abastadas, como os grandes comerciantes e a nobreza.

Seu contato constante com as pessoas faz com que o Carisma seja o principal atributo dos Bardos: é com que ele que se conquista a confiança, convence e entretém as pessoas e que possibilita a ampliação do seu conhecimento estético (da Arte). A Aura é seu segundo atributo mais importante, porque é ela quem permite uma intimidade maior com a Arte, que lhe concede seus poderes. Percepção vem logo em seguida, importante para o Bardo na medida em que o permite observar detalhes nas pessoas e nas circunstâncias à sua volta. Após esses os mais importantes são Intelecto, Agilidade, Físico e Força, respectivamente.''',
    ),
    Profissao(
      id: 1,
      nome: "Guerreiro",
      ehBasica: 18,
      racasNaoPermitidas: [],
      habilidadeIdAperfeicoada: 23,
      pontosAquisicaoHabilidade: 14,
      cdTipoHabilidadePenalizada: EnumCodigoHabilidade.influencia,
      descricao:
          '''Tanto na realidade quanto na fantasia, muitos homens e mulheres viveram de sua capacidade de combate. Exemplos destas pessoas são: Hércules, o rei Arthur, Conan, Roland, Siegfried, um legionário romano, um Guerreiro grego, um cavaleiro teutônico, e muitos outros que lutavam como membros de um exército.

Para o Guerreiro, a característica principal é a Força. É através dela que ele intimida e luta contra seus adversários. Também são importantes o Físico e a Agilidade; o primeiro, para garantir a sobrevivência às feridas dos combates e a segunda, para tentar evitar golpes e dá-los com maior precisão.

Alguns ainda ambicionam ser líderes, como Carlos Magno ou César, e para isto precisam de Carisma e Intelecto. A característica menos privilegiada pelos Guerreiros é a Aura.

Os Guerreiros são a profissão mais combativa de Tagmar sendo capazes de usar mais manobras de combate que qualquer outra profissão. Além disso, os Guerreiros podem participar de Academias de Combate, tendo acesso a técnicas restritas de luta.''',
    ),
    Profissao(
      id: 2,
      nome: "Ladino",
      ehBasica: 12,
      racasNaoPermitidas: [],
      habilidadeIdAperfeicoada: 36,
      pontosAquisicaoHabilidade: 22,
      descricao:
          '''Ladinos são aventureiros que se desenvolveram como os melhores no uso de Habilidades. Isso, combinado com seu considerável poder de combate, os fazem úteis em praticamente qualquer situação. Embora os Ladinos possam ser muito diferentes entre si eles compartilham de duas características em comum: versatilidade e grande capacidade de sobreviver a todo tipo de perigo.

De uma maneira geral os atributos principais dos Ladinos são Agilidade e Percepção para o uso de suas Habilidades, seguidos por Carisma, necessário para sua interação social. Após estes os atributos mais importantes são Inteligência, Físico, Força e Aura respectivamente.

Estes heróis (ou anti-heróis) podem pertencer a três Guildas, representando três posicionamentos diferentes perante a sociedade: os Assassinos, os Ladrões e os Piratas. Mais adiante serão apresentadas cada uma das Guildas.

Para ingressarem em uma guilda, os Ladinos devem ser conhecidos no respectivo meio ou terem realizado feitos recentes que chamem a atenção delas. Nessas condições, o personagem será contatado por um representante de uma das organizações e será convidado a ingressar nela, normalmente após uma missão para provar seu valor. Pertencer a uma guilda confere grandes vantagens ao personagem como será mais bem apresentado na descrição das mesmas.''',
    ),
    Profissao(
      id: 3,
      nome: "Mago",
      ehBasica: 6,
      racasNaoPermitidas: [0, 5],
      habilidadeIdAperfeicoada: 3,
      pontosAquisicaoHabilidade: 12,
      cdTipoHabilidadePenalizada: EnumCodigoHabilidade.manobra,
      descricao:
          '''Em muitas das lendas contadas pelo mundo, certas figuras se ressaltam: os magos, feiticeiros, bruxos, necromantes etc. Pessoas místicas que usam poderes além da compreensão dos não iniciados.

Os Magos são indivíduos voltados para o estudo de passagens e escrituras místicas. Eles se consagram integralmente ao aprendizado de formas arcanas de conhecimento. Isso os afasta de outras atividades, prejudicando de certa forma suas habilidades em outras áreas (por exemplo, mesmo podendo usar armas, eles têm um conhecimento militar mínimo). Outra dificuldade em combate é o problema trazido pelas armaduras: elas impedem a manipulação do "Mana" (energia mística presente no universo) diminuindo o contato do feiticeiro com sua fonte de poder, impossibilitando a execução de suas magias. Desta forma, é raríssimo encontrar um Mago usuário de armaduras, sendo os roupões folgados suas vestimentas prediletas.

Contudo, toda esta dedicação não é em vão: eles possuem um vasto arsenal de magias que pode torná-los poderosíssimos. A maior dificuldade de um Mago é resistir à aos primeiros estágios, na qual ele é consideravelmente fraco. Entretanto, à medida que o personagem se torna mais experiente, avançando de estágio, ele também se torna capaz de feitos incríveis e devastadores com a magia.

Isso faz o Mago muito poderoso por um lado, mas com uma única arma, a magia, o que, por outro lado, o faz muito fraco. Desta forma, estes profissionais da magia são cautelosos e precavidos, e procuram se afastar da força bruta.

A Aura é a característica mais importante para o Mago, pois, além de ser sua fonte de poder mágico (Karma), é através dela que ele manipula o mana ao seu redor. Um bom Intelecto lhe facilita a compreensão dos textos mágicos e ajuda a memorizar os rituais e encantamentos necessários. Uma Agilidade alta pode ser desejável, assim como um bom Físico; os demais atributoss são menos importantes, sendo a Força normalmente desprezada por lembrar a barbárie.''',
    ),
    Profissao(
      id: 4,
      nome: "Rastreador",
      ehBasica: 15,
      racasNaoPermitidas: [0, 5],
      habilidadeIdAperfeicoada: 4,
      pontosAquisicaoHabilidade: 12,
      cdTipoHabilidadePenalizada: EnumCodigoHabilidade.subterfugio,
      descricao:
          '''Estes são guerreiros que se adaptaram em viver e lutar nas florestas. Eles são de certa forma ligados às matas e desta comunhão ganham um presente único: magias ligadas ao meio em que vivem.

Os Rastreadores costumam usar todo tipo de armas (embora favoreçam arcos e espadas), o que os torna bons em combate e, na floresta, capazes de surpreender os inimigos com certa facilidade (Subterfúgio). No domínio das armas, ocupam o segundo lugar, perdendo apenas para os Guerreiros.

Infelizmente a ligação com as matas tem um preço: o porte de grandes quantidades de metal tira o poder de usar magia. Assim, os Rastreadores só podem fazer magias se estiverem no máximo com armadura de cota de malha parcial e não estiverem usando elmos e escudos. Além disso, como seu poder provém das florestas, a devastação destas pode causar-lhes sérios problemas, o que os torna muito preocupados em preservar a natureza.

Suas principais características são a Força, para se firmar como Guerreiro, e a Aura, para servi-la de meio aos poderes da natureza. Esta última é, portanto, sua fonte de poder mágico (Karma). Percepção é muito importante, pois é através dela que o Rastreador entra em comunhão com a natureza.

Além disto, ele deve ser bom em Agilidade, para sua furtividade e ter um bom Físico, para resistir ao combate e à vida selvagem. O Carisma é uma característica secundária para o Rastreador.

Rastreadores, em geral, são empregados como guias, perseguidores, exploradores, ou mesmo como guardiões autoproclamados de florestas ou bosques. Os Elfos Florestais tendem a encarar com simpatia membros desta profissão, independente da raça.

Num mundo permeado de magia como Tagmar, a interação das forças da natureza com os restantes dos seres vivos e sua harmonia é mais compreendida que em nossa realidade. Os Rastreadores tornam-se então verdadeiros ecologistas deste mundo.''',
    ),
    Profissao(
      id: 5,
      nome: "Sacerdote",
      ehBasica: 12,
      racasNaoPermitidas: [],
      habilidadeIdAperfeicoada: 12,
      pontosAquisicaoHabilidade: 14,
      cdTipoHabilidadePenalizada: EnumCodigoHabilidade.profissional,
      descricao:
          '''Estes Sacerdotes são parecidos com os clérigos medievais. Treinados na fé e nas armas, eles levam a palavra dos deuses a todos e lutam pela causa desses quando necessário.

Devido a sua grande devoção e fé, os Sacerdotes são capazes de evocar magias através de seus deuses, os chamados "milagres". Os milagres do Sacerdote são o único tipo de magia que todas as culturas aceitam, pois mesmo os mais supersticiosos são obrigados a reconhecer o poder dos deuses.

Os milagres devem ser usados com reverência, pois são uma dádiva especial dos deuses e, sempre que possível, para ajudar os seguidores da mesma religião. O uso destes poderes é importante para a propagação da fé.

Exemplos de destaque são: frei Tuck (ver Robin Hood), o arcebispo Turbin, das lendas de Carlos Magno, e as ordens como os Templários.

A característica principal para o Sacerdote é a Aura. Somente assim ele é capaz de solicitar poderes ao deus e ser o meio de ação deste. É através dessa característica que ele ganha força mágica (Karma). O Carisma também é muito importante, pois ele determina a força da vontade e a convicção do Sacerdote em servir aos desígnios do deus, ou seja, caracteriza sua fé. É o Carisma que permite ao Sacerdote conseguir o auxílio de seu deus nas mais diversas situações, ao ser capaz de evocar diversos milagres diferentes.

Outras características importantes para o Sacerdote são: Força, para torná-lo um bom combatente, e Inteligência, para torná-lo um estudioso eficiente de sua fé. O Físico e a Agilidade podem ajudar em combate, mas são secundários.

Os Sacerdotes são os canais de atuação direta dos deuses no plano de existência dos mortais. Isto é, os deuses não apenas comunicam-se através de Sacerdotes, mas também agem e interferem por seu intermédio. Por isto os Sacerdotes recebem poder e capacidade para realizar milagres.''',
    ),
  ];
}
