import 'package:flutter/cupertino.dart';
import 'package:tagmar/enums/enum_atributo.dart';
import 'package:tagmar/models/atributo/atributo.dart';

class Atributos with ChangeNotifier {
  static List<Atributo> atributos = [
    Atributo(
      id: EnumAtributo.agilidade.index,
      nome: "Agilidade",
      descricao:
          "São os reflexos, a rapidez e a coordenação. Esta característica é especialmente importante para o Subterfúgio (Ações Furtivas, Manusear Armadilhas etc.) e para as manobras (Escalar Superfícies, Montar Animais etc.), além de tornar o personagem mais difícil de ser acertado em combate.",
    ),
    Atributo(
      id: EnumAtributo.aura.index,
      nome: "Aura",
      descricao:
          "Todos os seres são envolvidos por um tipo de energia que os conecta com outros planos de existência. Esta energia é chamada Aura, e é através dela que proporciona o Karma (poder mágico) ou a resistência a efeitos de certas magias.",
    ),
    Atributo(
      id: EnumAtributo.carisma.index,
      nome: "Carisma",
      descricao:
          "Corresponde à voz de comando, a força de vontade, e a autoestima. Define a força da personalidade do personagem. Desta característica dependem Habilidades como Persuasão e Liderança.",
    ),
    Atributo(
      id: EnumAtributo.fisico.index,
      nome: "Físico",
      descricao:
          "Resistência ao esforço e à dor; saúde e vigor. Ajuda a resistir à fadiga do combate e, em caso de ferimentos, a sobreviver aos golpes recebidos.",
    ),
    Atributo(
      id: EnumAtributo.forca.index,
      nome: "Força",
      descricao:
          "Capacidade de levantar pesos, de utilizar os músculos em combate, causar mais dano, etc. A Força é muito importante em combate, pois, aumenta o dano, permite o uso de armas mais pesadas.",
    ),
    Atributo(
      id: EnumAtributo.intelecto.index,
      nome: "Intelecto",
      descricao:
          "Capacidade de raciocínio, memória. O Intelecto é importante para o estudo de magia arcana e Habilidades como Navegação, Medicina etc.",
    ),
    Atributo(
      id: EnumAtributo.percepcao.index,
      nome: "Percepção",
      descricao:
          "Capacidade do indivíduo de perceber o ambiente em que está. Inclui a atenção e a capacidade de concentração. É de extrema importância para Rastreadores.",
    ),
  ];
}
