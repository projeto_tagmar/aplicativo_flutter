import 'package:flutter/cupertino.dart';
import 'package:tagmar/models/versao/versao.dart';

class VersionLog with ChangeNotifier {
  static const List<Versao> versoes = [
    Versao(
      version: "1.0.18",
      changes: [
        "Separando 'observações'",
        "Revisão de magias D e E",
      ],
    ),
    Versao(
      version: "1.0.17",
      changes: [
        "Separando 'observações'",
        "Revisão de magias A a C",
      ],
    ),
    Versao(
      version: "1.0.16",
      changes: [
        "Tolerância à grafia errada",
        "Adição das magias restantes",
        "Precisando revisar magias C a Z",
      ],
    ),
    Versao(
      version: "1.0.15",
      changes: [
        "Revisão de magias de A a D",
        "Adição de magias até a letra L (a revisar)"
      ],
    ),
    Versao(
      version: "1.0.14",
      changes: ["Revisão da tela 'sobre'", "Magias de A a C"],
    ),
    Versao(
      version: "1.0.13",
      changes: ["Revisão de magias", "Revisão de acentuação"],
    ),
  ];
}
