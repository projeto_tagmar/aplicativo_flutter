import 'package:flutter/cupertino.dart';

import 'package:tagmar/models/magia/magia.dart';
import 'package:tagmar/database/magias_lista/a.dart' as a;
import 'package:tagmar/database/magias_lista/b.dart' as b;
import 'package:tagmar/database/magias_lista/c.dart' as c;
import 'package:tagmar/database/magias_lista/d.dart' as d;
import 'package:tagmar/database/magias_lista/e.dart' as e;
import 'package:tagmar/database/magias_lista/f.dart' as f;
import 'package:tagmar/database/magias_lista/g.dart' as g;
import 'package:tagmar/database/magias_lista/h.dart' as h;
import 'package:tagmar/database/magias_lista/i.dart' as i;
import 'package:tagmar/database/magias_lista/l.dart' as l;
import 'package:tagmar/database/magias_lista/m.dart' as m;
import 'package:tagmar/database/magias_lista/n.dart' as n;
import 'package:tagmar/database/magias_lista/o.dart' as o;
import 'package:tagmar/database/magias_lista/p.dart' as p;
import 'package:tagmar/database/magias_lista/q.dart' as q;
import 'package:tagmar/database/magias_lista/r.dart' as r;
import 'package:tagmar/database/magias_lista/s.dart' as s;
import 'package:tagmar/database/magias_lista/t.dart' as t;
import 'package:tagmar/database/magias_lista/u.dart' as u;
import 'package:tagmar/database/magias_lista/v.dart' as v;

class Magias with ChangeNotifier {
  static final List<Magia> magias = [
    ...a.lista,
    ...b.lista,
    ...c.lista,
    ...d.lista,
    ...e.lista,
    ...f.lista,
    ...g.lista,
    ...h.lista,
    ...i.lista,
    ...l.lista,
    ...m.lista,
    ...n.lista,
    ...o.lista,
    ...p.lista,
    ...q.lista,
    ...r.lista,
    ...s.lista,
    ...t.lista,
    ...u.lista,
    ...v.lista,
  ];

  List<Magia> get all {
    return [...magias];
  }

  List<Magia> getByName(String name) {
    if (name.isEmpty) {
      return all;
    }
    return magias
        .where((element) =>
            element.nome.toLowerCase().contains(name.toLowerCase()))
        .toList();
  }

  Magia findById(int id) {
    return magias.firstWhere((magia) => magia.id == id);
  }
}
