import 'package:flutter/cupertino.dart';

import 'package:tagmar/models/raca/raca.dart';

class Racas with ChangeNotifier {
  static const List<Raca> racas = [
    Raca(
      id: 0,
      nome: "Anão",
      agilidade: -1,
      fisico: 2,
      forca: 1,
      velocidade: 16,
      efBasica: 15,
      peso: 53,
      altura: 1.39,
      descricao:
          '''Os Anões são uma raça de seres humanoides de baixa estatura, fisicamente semelhantes a seres Humanos excepcionalmente baixos e atarracados, tendo em média 1,39 metros de altura. A aparência dos Anões engana, pois apesar de sua baixa estatura, eles costumam ser mais fortes e mais resistentes que os Humanos.

Os Anões costumam viver em cidades construídas sob a terra, geralmente no interior de montanhas. Devido a essa vida na escuridão, essa raça desenvolveu a capacidade de ver perfeitamente em ambientes com pouquíssima luz.

A maioria dos Anões viaja pelo mundo em uma época ou outra de suas vidas, geralmente em busca de riqueza, pois são ambiciosos e estão sempre à procura de meios de aumentar suas posses. Para um Anão, porém, mais importante que tesouros é a fama de grande aventureiro. Por isso, a maior parte deles parte em busca de aventuras para criar uma reputação, pois o orgulho, tanto individual quanto racial, é uma característica marcante de sua raça.

Os Anões são orgulhosos, ambiciosos e suscetíveis (nunca brinque a respeito de sua altura!). Contudo, salvo raras exceções, não seriam capazes de trair um amigo ou serem desonestos por essa ambição. Apesar do tempo de vida longo, girando em torno dos 450 anos, os Anões são pouco numerosos, sendo as mulheres de sua raça mais raras ainda. Assim, raramente irá se encontrar uma aventureira anã, pois estas são muito protegidas. Isto não impede que assumam grandes responsabilidades administrativas, educacionais e científicas (engenharia, medicina, etc.) em suas comunidades, já que os homens dedicam-se mais à guerra e à religião.

Os Anões são quase sempre taciturnos e um pouco irritadiços, além de tenderem à usura. Com raras exceções, porém, são muito leais a seus companheiros, destemidos em combate e de confiança em qualquer situação.

O grande valor dado por esta raça à força física resulta em uma cultura anã de desprezo e desconfiança em relação aos Magos em geral.

Devido a sua grande massa corporal e a robustez de seu corpo, Anões são pouco ágeis, recebendo ajuste de –1 em Agilidade.

Em compensação, Anões são muito fortes e resistentes, recebendo um ajuste de + 1 na Força e +2 no Físico. Estes bônus fazem dos Anões Guerreiros excepcionais. Eles também podem ser Sacerdotes ou Ladinos (muito raramente). Porém, não podem ser Magos, Bardos ou Rastreadores, por não possuírem a capacidade de usar a magia não divina.

Anões são mestres artífices por índole e, apesar de sua personalidade, apreciadores de obras de arte (principalmente escultura e joalheria). Isto os torna respeitadores de bons artífices de qualquer raça, fazendo mesmo com que procurem contatos com o mundo exterior para melhorarem sua arte. Anões serão muitas vezes encontrados nas cidades humanas exercendo posições de autoridade nas corporações de ofícios, ou como artífices pessoais de reis ou da alta nobreza.''',
    ),
    Raca(
      id: 1,
      nome: "Elfo Dourado",
      agilidade: 1,
      aura: 2,
      fisico: -1,
      inteligencia: 1,
      velocidade: 18,
      efBasica: 13,
      peso: 53,
      altura: 1.63,
      descricao:
          '''Eles são belos e altivos com sua pele dourada e cabelos brancos, dourados ou ruivos. Sua capacidade física é baixa, mas sua capacidade intelectual e mágica mais do que compensam isso.

Os Elfos Dourados são os mais raros dentro da raça élfica. Os poucos que existem vivem associados a outras raças, geralmente junto a tribos de Elfos Florestais, ocupando lugares de destaque. Os clãs que se unem para formar uma cidade ou nação constituem uma exceção.

Sua sociedade é voltada para o saber, produzindo muitos Magos e Bardos, dando aos seus membros um ajuste de +1 no Intelecto. Magicamente ativos, suas capacidades são lendárias, daí o seu ajuste de +2 na Aura. Também, como os Elfos Florestais, são mais ágeis que os Humanos, com + 1 na Agilidade. Apesar de tudo isso, eles são fisicamente frágeis, recebendo -1 no Físico e na Força.

Poucos são os seus Sacerdotes, pois sua cultura privilegia os Magos. Alguns são Guerreiros (fato raro), um número maior, Rastreadores, devido à sua intimidade com a natureza. Raros também são os Ladinos, por serem considerados indignos do alto nível da raça.

Sua curiosidade sempre insatisfeita e seu amor fanático pelo conhecimento são praticamente os únicos motivos que os levam a se aventurar (pode-se somar também uma parcela de tédio). Existem lendas de Elfos Dourados que viveram muito mais que 800 anos, pois nunca saciaram sua sede de conhecimentos e, por isto, nunca perderam a vontade de viver.''',
    ),
    Raca(
      id: 2,
      nome: "Elfo Florestal",
      agilidade: 1,
      aura: 1,
      fisico: -1,
      forca: -1,
      percepcao: 2,
      velocidade: 18,
      efBasica: 14,
      peso: 53,
      altura: 1.63,
      descricao:
          '''Estes são o tipo mais comum de elfo. Eles são separatistas e não gostam de contato com raças que não sejam das florestas. São xenófobos e têm grande antipatia por Anões, assim como os Anões têm antipatia por eles. Os Elfos Florestais têm certa reverência frente aos Elfos Dourados, considerando-os nobres e respeitados, sendo suas opiniões sempre bem vistas e seus conselhos importantes.

Sua sociedade produz muitos Guerreiros, pois privilegiam o poder marcial. Alguns se tornam Rastreadores, e se autodenominam defensores das florestas. Dentre os Elfos, os florestais são os que usam menos magia. Ainda assim, porém, existem muitos Magos e raros Sacerdotes.

Alguns deles se tomam ótimos Ladinos devido a seus ajustes raciais. Raríssimos são os Bardos, pois não se comunicam bem com outras raças.

Os Elfos Florestais, como todos os Elfos, têm um corpo leve e frágil, com -1 no Físico e na Força, mas ágil, com +1 na Agilidade. Elfos Florestais estão sempre atentos, recebendo + 2 em Percepção. Mesmo usando menos magias, ainda assim, são magicamente ativos, recebendo +1 em Aura.

Os Elfos Florestais são os mais numerosos entre os Elfos, mas sua atitude guerreira e xenófoba (principalmente contra Humanos e Anões) os levou a sangrentas guerras, que só contribuíram para diminuir a raça.

No entanto, recentemente, devido à intervenção dos Elfos Dourados e a existência de outros contatos (que levaram à existência dos Meio-Elfos) os Elfos Florestais passaram a aceitar um contato maior com o mundo exterior.''',
    ),
    Raca(
      id: 3,
      nome: "Humano",
      velocidade: 20,
      efBasica: 17,
      peso: 77,
      altura: 1.77,
      descricao:
          '''A raça mais numerosa de Tagmar. Eles podem viver em qualquer ambiente e se reproduzem com muito mais rapidez que os Anões e os Elfos. Os Humanos comuns são o modelo de comparação para as outras raças, e por isso não recebem qualquer ajuste, mas os Humanos dos personagens dos jogadores são aventureiros e sempre possuem um algo mais que a média, assim Humanos em termos de jogo recebe +1 para ser colocado em um de seus atributos (a escolha do jogador). Enfim, Humanos são o elo com o nosso mundo real, são os humanos que você conhece.

Apesar das diferenças étnicas entre os Humanos, todos têm as mesmas características, não havendo qualquer diferença (do ponto de vista prático) entre eles. As capacidades e as limitações são iguais para todos.

Em Tagmar, devido à melhor compreensão da natureza e da disponibilidade da mágica, o tempo de vida dos seres Humanos é muito maior que o de um ser Humano da Idade Média de nossa realidade, chegando a cerca de 80 anos.''',
    ),
    Raca(
      id: 4,
      nome: "Meio-Elfo",
      agilidade: 1,
      carisma: 1,
      velocidade: 17,
      efBasica: 15,
      peso: 69,
      altura: 1.68,
      descricao:
          '''Meio-Elfos são o produto da união de Elfos e Humanos. É surpreendente que duas raças assim diferentes possam gerar progenitores, e ainda mais que estes sejam férteis, mas esta miscigenação é um fenômeno muito observado em Tagmar.

Os Meio-Elfos parecem somar as qualidades dos dois. Eles possuem a personalidade fascinante dos Elfos e a Força e a Resistência Física dos Humanos. Meio-Elfos se adaptam bem a qualquer profissão, e alguns deles são encontrados entre os maiores heróis de todos os tempos. A Agilidade proeminente lhes confere +1 de Agilidade e seu Carisma e sua vivacidade lhes dão +1 no Carisma.

Esta raça vive aproximadamente 450 anos, e seu envelhecimento se dá como nos Elfos. Meio-Elfos não formam comunidades ou cidades próprias. Em vez disso eles vivem entre os Humanos ou Elfos, sendo muito bem aceitos nesses grupos (embora certa desconfiança possa ser evidenciada vez por outra).

Devido a seu lento amadurecer (por padrões Humanos) os Meio-Elfos passam em geral, a primeira parte de suas vidas entre Elfos. No entanto a mais dura decisão da vida de um Meio-Elfo é escolher em que raça ele viverá e quais costumes e leis respeitará (humanas ou élficas). Esta decisão, juntamente com outros fatores leva o Meio-Elfo a se aventurar, pois este precisa conhecer o mundo para escolher.''',
    ),
    Raca(
      id: 5,
      nome: "Pequenino",
      agilidade: 2,
      carisma: 1,
      fisico: 1,
      forca: -2,
      percepcao: 1,
      velocidade: 14,
      efBasica: 11,
      peso: 30,
      altura: 1.14,
      descricao:
          '''Os Pequeninos são uma raça de humanoides de pequena estatura. Fisicamente, eles se assemelham muito aos seres Humanos, exceto na escala: têm, em média, 1,14 metros de altura, e os pés desproporcionalmente grandes e peludos.

Os Pequeninos são um povo amante da paz, que via de regra preferem viver em seus povoados e são, como raça, bastante tímidos. Suas comunidades são difíceis de se localizar devido à excelente camuflagem; se um personagem, no entanto, é aceito, o povo pequeno se revela hospitaleiro e alegre.

Os Pequeninos são, em geral, um pouco epicuristas, amando os prazeres simples da boa mesa, música, dança e festas, sendo os Bardos sempre bem recebidos em seus povoados. Isto torna o Pequenino, quando em companhia de seu povo ou de amigos, um personagem alegre e extrovertido. Detentores de grande agilidade pequeninos possuem +25% em testes para equilibrar-se e ocultar-se. Por evitarem o combate frontal, alguns os julgam covardes. Ledo engano! Sua coragem é respeitada por todos que os conhecem, pois eles apenas preferem a cautela a atos impensados.

Por esta timidez e felicidade com sua vida, os Pequeninos não se interessam em partir em busca da riqueza, conhecimento ou poder. Ocasionalmente, porém, um Pequenino deixa seu lar para viajar pelo mundo, levado pela curiosidade ou pela ambição (esta última bastante rara).

Devido a sua pequena estatura, os Pequeninos recebem um ajuste de -2 para sua Força. Eles apresentam, porém, uma grande Resistência Física e por isso recebem um ajuste de +1 no seu Físico. O epicurismo, a simpatia e a vida alegre e extrovertida concedem um ajuste de +1 no Carisma. Além disso, os Pequeninos costumam ser extremamente ágeis e atentos aos padrões Humanos, recebendo um ajuste de +2 para sua Agilidade e de +1 em Percepção.

Pequeninos não compreendem bem a magia que não vem dos Deuses e por isso não podem se tomar Magos, Rastreadores, mas podem ser Bardos. Eles podem também ser Guerreiros, pois embora tenham ajuste de –2 em Força, seus bônus em Agilidade e Físico possibilitam bons combatentes. Como Ladinos, porém, os Pequeninos são insuperáveis, pois sua Agilidade pode chegar a níveis inatingíveis aos Humanos (ou mesmo aos Elfos) e alguns, por isso, chegaram a se tomar conhecidos como os melhores gatunos do continente. Embora seja muito raro, eles também poder ser Sacerdotes, como todas as raças.

Os Pequeninos têm um período de vida similar ao dos Humanos, isto é, cerca de 80 anos.''',
    ),
  ];

  List<Raca> get all {
    return [...racas];
  }

  Raca findById(int id) {
    return racas.firstWhere((raca) => raca.id == id);
  }
}
