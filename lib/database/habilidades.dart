import 'package:flutter/cupertino.dart';
import 'package:tagmar/enums/enum_codigo_habilidade.dart';
import 'package:tagmar/enums/enum_habilidades.dart';
import 'package:tagmar/models/habilidade/area_habilidade.dart';
import 'package:tagmar/models/habilidade/habilidade.dart';
import 'package:tagmar/models/habilidade/enum_penalidades.dart';

class Habilidades with ChangeNotifier {
  static List<Habilidade> habilidades = [
    Habilidade(
      id: EnumHabilidades.acoesFurtivas.index,
      nome: "Ações Furtivas",
      custo: 2,
      tipoHabilidade: EnumCodigoHabilidade.subterfugio,
      idAtributoBase: 0,
      penalidades: [Penalidades.armadura],
      descricao:
          '''É a habilidade de usar o silêncio, a camuflagem e as sombras para passar despercebido e esconder-se ou deslocar-se sem deixar vestígios. “Ações Furtivas” também deve ser empregada quando se deseja ocultar pequenos objetos, seja no próprio corpo, seja no corpo de outra pessoa ou ainda em qualquer outro objeto. A dificuldade no teste, neste caso, dependerá da disponibilidade de opções para ocultação, a critério do mestre.

A habilidade pode ser executada em movimento ou não. Devido à particular necessidade de discrição que essa habilidade possui, ela será muito prejudicada se quem desejar usá-la estiver trajando armaduras médias ou pesadas, de acordo com as regras apresentadas no item “Penalidades por carga e armadura”, bem como carregando muito peso ou equipamentos muito barulhentos, a critério do mestre.

Para permanecer oculto quando alguém está ativamente procurando, o personagem deverá obter um resultado superior na habilidade Ações Furtivas em relação ao resultado obtido na habilidade “Usar os Sentidos” feito por quem o está rastreando. Se o personagem estiver imóvel ou o objetivo seja esconder algum objeto, um empate já será suficiente para ter a vantagem sobre quem o procura. O mestre deve aplicar modificadores às colunas de resolução sobre o teste, de acordo com a disponibilidade que o ambiente próximo dispõem para se esconder ou passar despercebido .

As orientações abaixo devem servir como parâmetro para o mestre estabelecer uma dificuldade quando o personagem tenta se esconder de uma ameaça passiva, ou seja, aquela que não está ativamente procurando por ele. Para os testes resistidos é apresentado um modificador às colunas de resolução para a tentativa desta habilidade.''',
      tarefasAperfeicoadas: [
        'seguir',
        'ocultar objetos',
        'despistar examinador.',
        'misturar-se a multidão',
        'esconder-se nas sombras',
        'esconder-se atrás de outros objetos',
        'andar em silêncio; falar discretamente',
      ],
    ),
    Habilidade(
      id: EnumHabilidades.acrobacias.index,
      tipoHabilidade: EnumCodigoHabilidade.manobra,
      nome: "Acrobacias",
      custo: 2,
      idAtributoBase: 0,
      penalidades: [Penalidades.armadura, Penalidades.carga],
      descricao:
          '''É a habilidade de executar movimentos corporais complexos e de grande precisão, como dar cambalhotas, equilibrar-se, movimentar-se durante uma queda, escorregar por um corrimão, manejar uma arma enquanto está agarrado a um candelabro, contorcer-se etc.

Tal habilidade também pode ser utilizada para agregar elegância em movimentos mais simples mantendo o corpo firme e os movimentos precisos, como dar um salto mortal, por exemplo. Por este motivo, essa habilidade também é muito praticada por artistas de circo em todos os lugares como forma de ganha-pão.

“Acrobacias” também é uma habilidade que os aventureiros mais acostumados com exploração de locais com difícil acesso normalmente terão alguma prática, uma vez que será comum encontrar templos e masmorras antigas descaracterizados pelo tempo e com acesso possível apenas com manobras complicadas.

O uso de armaduras ou o peso extra carregado pelo personagem concedem penalidades no uso dessa habilidade. No entanto, o mestre pode permitir tentativas sem penalidade quando o personagem deseja manter-se em pé sobre solo firme se ele tiver condições de manter os dois pés no chão.''',
      tarefasAperfeicoadas: [
        'cair em pé',
        'saltos mortais',
        'andar sobre cordas',
        'escorregar com direção',
        'mudar de posição rapidamente',
        'piruetas apoiando-se com as mãos.',
        'equilibrar-se em pisos escorregadios',
        'equilibrar-se em plataforma estreita',
      ],
    ),
    Habilidade(
      id: EnumHabilidades.agricultura.index,
      tipoHabilidade: EnumCodigoHabilidade.profissional,
      nome: "Agricultura",
      custo: 1,
      idAtributoBase: 5,
      descricao:
          '''Esta habilidade representa o conhecimento, a técnica e a prática de administração do meio rural. Praticamente toda a população de Tagmar é alimentada graças a produção dos camponeses, que formam uma população rural que é muito maior do que a população urbana e que levam a vida orando muito por Sevides, Quiris e Líris. Os sacerdotes da tríade da agricultura são os grandes mestres desta habilidade, embora todo camponês tenha algum conhecimento básico para seu próprio sustento.

A habilidade Agricultura é útil quando o personagem deseja, por exemplo: analisar se uma cultura agrícola está se desenvolvendo bem; identificar se existe alguma praga natural ou sobrenatural ameaçando a lavoura ou o solo; operar mecanismos rurais, como um moinho; prever o clima em uma faixa de algumas dezenas de horas, contanto que esteja em ambientes abertos; constatar a fertilidade de um solo; constatar se uma fonte de água é suficiente e saudável para os vivos, incluindo a lavoura; aplicar adubos de forma segura para as plantas; estimar o momento ideal para o cultivo que garanta uma plantação pelo menos 1/3 melhor do que a média; identificar ou reconhecer as características de plantas.''',
      tarefasAperfeicoadas: [
        'prever o clima',
        'espalhar pragas',
        'melhorar produção',
        'identificar pragas',
        'encontrar onde há água subterrânea',
        'reconhecer frutas, legumes e vegetais',
        'operar moinhos, arados e outros equipamentos rurais',
        'racionar e estimar quantidade necessária de suprimentos',
      ],
    ),
    Habilidade(
      id: EnumHabilidades.aplicarEsforco.index,
      tipoHabilidade: EnumCodigoHabilidade.manobra,
      nome: "Aplicar Esforço",
      custo: 1,
      idAtributoBase: 4,
      penalidades: [Penalidades.carga],
      descricao:
          '''Esta é a técnica para utilizar corretamente a musculatura no intuito de potencializar a aplicação de um esforço na realização de uma tarefa. Ela é útil em situações como levantar peso, empurrar, torcer ou quebrar objetos, arrombar portas, saltar e outras ações atléticas.

Aplicar esforço é aprimorada com a prática e o treinamento, sendo uma habilidade conhecida por muitos guerreiros e atletas. Contudo, ela é uma habilidade ativa, ou seja, ela assume que o personagem está consciente do que está fazendo e tenta de alguma forma empregar sua técnica.

A regra de danificar objetos estabelece que deve ser empregado um teste de arma. Contudo, quando é possível que o personagem utilize sua força bruta para destruí-los - empurrando-os, quebrando-os, rasgando-os ou torcendo-os - o mestre deve exigir um teste de “Aplicar Esforço” em seu lugar.

A habilidade deve ser testada apenas uma vez por tarefa, sem novas tentativas até que a situação mude. Por exemplo, o personagem só tem direito a um salto nas mesmas condições, só pode tentar segurar a respiração uma vez até que recupere o fôlego etc.

As distâncias de salto adicionais da lista assumem que o personagem correu por pelo menos 3 metros antes do salto. Se esta afirmativa não for verdadeira, reduza os valores à metade. Se o mestre julgar que é possível o personagem utilizar um apoio para saltar, como uma vara ou uma corda suspensa, acrescente metade do comprimento do apoio á distância horizontal e vertical.

Ressalta-se que o uso desta habilidade não é nada discreto. Como regra geral, todos os testes para perceber alguém fazendo uso da habilidade são realizados com, no mínimo, um nível de dificuldade a menos, ficando a critério do mestre prejuízos maiores. Todas as faixas de peso a que a descrição dos resultados se refere estão relacionados com os esforços da seção Mecânica de Regras e na tabela de força da seção de atributos.''',
      tarefasAperfeicoadas: [
        'levantar peso',
        'sustentar peso',
        'salto em altura',
        'arrombar portas',
        'arrastar ou puxar',
        'salto em distância',
        'quebrar ou esmagar',
        'segurar a respiração',
      ],
    ),
    Habilidade(
      id: EnumHabilidades.arte.index,
      tipoHabilidade: EnumCodigoHabilidade.influencia,
      nome: "Arte",
      custo: 1,
      idAtributoBase: 2,
      isTesteSemNivel: false,
      isRestrita: true,
      descricao:
          '''Arte é uma habilidade ampla dividida em áreas de atuação que o personagem deve adquirir separadamente. Seu grande grupo inclui todas as formas de expressão artística, e as áreas de atuação escolhidas podem ser: Artes visuais, Desenho, Literatura e Música.

Personagens que dominem um subgrupo de Arte não saberão necessariamente se expressar de todas as formas existentes dele, mas com alguma prática poderão aprender com mais ou menos facilidade dependendo da complexidade do ato. Ex: Saber música não significa ser capaz de dançar todas as danças que encontrar, mas com um pouco de prática ele poderá aprender qualquer novo estilo.

Todos os ramos de arte são restritos a grande parte da população, sendo ensinados somente a uma pequena parcela mais abastada (Grande Comerciante em diante). A única exceção é Música, que pode ser conhecida por qualquer um. Logo, esta habilidade não pode ser testada sem nível. Cada uma de suas áreas de atuação abrangem:''',
      tarefasAperfeicoadas: [
        'criar arte',
        'criar opiniões',
        'mudar opiniões',
        'identificar arte',
        'distrair com arte',
        'impressionar com arte',
        'alterar ou restaurar arte',
        'trabalhar com materiais improvisados',
      ],
      areas: const <AreaHabilidade>[
        AreaHabilidade(
          id: 0,
          nome: 'Artes visuais (Car)',
          descricao:
              'O talento para criar pinturas e trabalhar em barro, metais ou pedras a fim de criar estátuas e outras obras de arte. Um personagem hábil em Arte (Artes Visuais) é capaz de reconhecer substâncias capazes de colorir ou descolorir objetos e identificar o autor de uma obra com base em seu estilo. Ele também é capaz de distinguir os tipo de pedras ou metais utilizados na construção de uma obra de arte. O uso de óleos e tintas está coberto no uso desta habilidade. Portanto, um personagem com Arte (Artes Visuais) é capaz de criar tais substâncias se possuir os materiais adequados (geralmente pigmentos extraídos de rochas ou plantas, assim como óleos, álcool e outros solventes).',
        ),
        AreaHabilidade(
          id: 1,
          nome: 'Desenho (Per)',
          descricao:
              'A habilidade de desenhar em papeis, pergaminhos ou outra superfície plana é utilizada para representar roupas, mapas, pessoas, placas e símbolos com clareza. Um desenhista pode ganhar a vida como alfaiate se também possuir a habilidade de trabalhos manuais, como construtor de armas e armaduras únicas se também possuir a habilidade trabalhos em metais, como projetista de casas, muralhas e máquinas de guerra se possuir engenharia, de embarcações se também possuir carpintaria ou como cartógrafo se também possuir a habilidade Navegação. Desenho por si só também é utilizada para copiar mapas ou projetos de engenharia corretamente e de forma compreensível para um leigo, para criar ou lembrar e desenhar corretamente um símbolo pouco comum ou para fazer retratos falados de pessoas. Para pintar um rosto ou retrato em tinta, utiliza-se Artes (Artes Visuais).',
        ),
        AreaHabilidade(
          id: 2,
          nome: 'Literatura (Car)',
          descricao:
              'O talento para compor textos, romances, poesias e roteiros teatrais convincentes e com qualidade. Um personagem hábil em Arte (Literatura) é capaz de transmitir uma mensagem subliminar através de sua escrita, fazendo com que, a longo prazo, suas ideias sejam mais facilmente aceitas em um determinado círculo. Uma vez que a escrita é um conhecimento pouco difundido em Tagmar, o conhecimento dessa arte confere alto reconhecimento social. Por outro lado, embora possua um poder de influência de longo prazo bastante elevado, seu alcance se limita aos poucos que sabem ler, como nobres, grandes comerciantes e raças antigas. O personagem só é capaz de utilizar esta habilidade em um idioma que saiba escrever.',
        ),
        AreaHabilidade(
          id: 3,
          nome: 'Música (Car)',
          descricao:
              'O talento e a habilidade de dançar, cantar e tocar instrumentos musicais. Um artista musical é capaz de tocar pelo menos um tipo de instrumento musical (cordas, teclados, sopro ou percussão) com maestria mas, uma vez que seja necessário, é possível aprender qualquer outro tipo com facilidade e em poucas semanas de treino; a habilidade Arte (Música) também pode ser utilizada para reconhecer o som de instrumentos específicos que o personagem já tenha escutado (por exemplo, o teclado de templo de Blator) e para afiná-los, contanto que haja tempo e concentração para tal. A habilidade Arte (Música) também pode ser utilizada para criar um arranjo musical para um texto ou poesia, tornando-o agradável aos ouvintes. O mestre pode determinar que NPCs conheçam apenas um instrumento específico.',
        ),
      ],
    ),
    Habilidade(
      id: EnumHabilidades.carpintaria.index,
      tipoHabilidade: EnumCodigoHabilidade.profissional,
      nome: "Carpintaria",
      custo: 1,
      idAtributoBase: 0,
      descricao:
          '''Esta Habilidade permite ao personagem reparar ou manufaturar objetos de madeira, desde uma forma grosseira (tábuas ou troncos) até sua finalização, empregar técnicas de conservação através da aplicação de produtos naturais ou reconhecer a origem de uma degradação de um objeto de madeira ou vegetal lenhoso. Para executar sua profissão, o personagem precisará de ferramentas adequadas. Por isso, o seu estojo de carpintaria será sempre uma de suas posses mais valorizadas. Um bom carpinteiro pode ganhar boas somas de dinheiro com seu trabalho.

Os graus de dificuldade variam de acordo com a dificuldade do trabalho proposto e da disponibilidade de instalações e ferramentas adequadas (por exemplo: um carpinteiro sem um torno só poderá fabricar setas e lanças grosseiras).

Para se fazer um objeto, procede-se da seguinte maneira: o Mestre determina o tempo necessário e o jogador diz qual o seu Nível na Habilidade. O Mestre rola secretamente e verifica qual o resultado. Se o jogador obtiver sucesso, o objeto está pronto. Se ele fracassar normalmente, o item ficou pronto, mas não pode ser usado devido a um defeito de fabricação. Se o resultado for uma Falha Crítica, o item está pronto e pode ser utilizado, mas vai se quebrar na primeira vez em que for submetido à grande tensão. Por exemplo: uma lança se quebra na primeira vez que quem a estiver usando realizar um ataque 100% e uma embarcação irá se partir na primeira forte correnteza. Para se detectar um item nesse estado é necessário um Teste de Dificuldade Muito Difícil realizado por outro personagem que não seja seu criador.

Um reparo exige o empenho de 1/4 do custo do objeto em matéria prima. Uma falha na tentativa de reparar um objeto o mantêm danificado, mas uma nova tentativa é possível com o empenho de1/4 do custo do objeto em matéria prima. Se obtiver verde (Falha crítica), o objeto está destruído. O tempo de cada tentativa depende do objeto em questão, mas nunca pode ser inferior a uma hora. Além disso, o mestre deve ter em mente que alguns objetos não tem conserto.

É possível empregar essa habilidade para melhorar objetos de madeira. A tentativa é realizada como se fosse um reparo. Contudo, todos os testes são considerados difíceis(cor vermelha) ou, no caso de objetos delicados, como flechas, muito difíceis(cor azul). Se o personagem obtiver uma falha crítica, o objeto está danificado e precisará ser consertado conforme acima. Uma falha comum não traz maiores prejuízos. Um sucesso indica que ele ou (a) recebe mais resistência, ou (b) desloca-se com mais velocidade ou por uma distância maior em terra, água ou ar, conforme mais apropriado.

Como mostrado na tabela abaixo, um resultado absurdo no teste indica a construção de uma obra prima. Na prática, isso significa que o próprio material utilizado acabou se mostrando muito melhor do que o normal e o objeto é excepcional. Com isso, embarcações e outras estruturas grandes de madeira recebem mais resistência, enquanto flechas e outros objetos arremessáveis não quebram no primeiro disparo ou podem ser disparadas 1/3 mais longe devido a sua perfeita aerodinâmica.''',
      tarefasAperfeicoadas: [
        'construir armas',
        'recuperar flechas',
        'reconhecer madeiras',
        'construir estruturas',
        'construir embarcações',
        'reparar objetos e estruturas',
        'trabalhar com materiais improvisados',
        'expulsar e prevenir pragas de madeira',
      ],
    ),
    Habilidade(
      id: EnumHabilidades.codigos.index,
      tipoHabilidade: EnumCodigoHabilidade.subterfugio,
      nome: "Códigos",
      custo: 2,
      idAtributoBase: 5,
      isTesteSemNivel: false,
      isRestrita: true,
      descricao:
          '''Esta é a habilidade de decifrar e criar mensagens secretas. Uma mensagem como essa só poderá ser compreendida por alguém que não possua a Habilidade se ele souber a chave de codificação, que é o conjunto de regras utilizado para transcrever a mensagem original na mensagem codificada. Contudo, mesmo que um leigo possua tal chave, ele levará o dobro do tempo para terminar a transcrição do que quem souber a habilidade de Códigos.

Códigos é uma habilidade utilizada por sociedades secretas, exércitos e também por estudiosos e aventureiros que desejam descobrir os segredos mais bem guardados do passado e do presente. Geralmente as organizações possuem grupos inteiros dedicados a decifrar mensagens interceptadas de seus inimigos. Ao mesmo tempo, é comum que exploradores de antiguidades descubram textos antigos codificados e precisem compreendê-los para avançarem com suas descobertas.

O uso da Habilidade para decodificar uma escrita sempre exige muito tempo. Para cada página codificada, o mestre deverá exigir que o personagem a analise por, no mínimo, 2 horas e, então, solicitar um teste de habilidade. Porém, o tempo pode ser muito maior, a critério do mestre, dependendo da conservação do código. Um fracasso significa que o personagem não avançou na transcrição, talvez por não ter descoberto nada ou talvez por ter feito alguma suposição errada. O caminho inverso, ou seja, transcrever uma mensagem comum para uma mensagem codificada, será tão trabalhoso e exigirá tanto tempo quanto decifrá-la. Transcrever uma mensagem para uma linguagem em um código já conhecido exige apenas a metade do tempo. Um personagem pode conhecer tantas chaves de código quanto é seu total em códigos.

Também é possível aperfeiçoar uma chave de código ou torná-la mais simples. No entanto, a tarefa sempre será considerada um grau mais difícil do que a tarefa de construí-la do zero. Por exemplo, tornar uma chave simples em uma chave complexa é difícil, enquanto tornar uma difícil em simples é fácil. O personagem deve conhecer a chave de código que deseja aperfeiçoar ou simplificar. Aperfeiçoar e simplificar leva ¼ do tempo que se leva para construir.

Uma falha (verde) no teste de códigos tem significados peculiares. Para uma decifração o personagem interpretou errado a mensagem; para uma criação ele transmitiu a mensagem errada e terá de tentar decifrá-la como uma mensagem moderada se quiser consertar.''',
      tarefasAperfeicoadas: [
        'escrever em código',
        'criar chave de código',
        'aperfeiçoar chave de código',
        'reconhecer erro de decifração',
        'transferir mensagem codificada',
        'decifrar chave de código antigo',
        'deduzir autores de uma chave de código',
        'alterar chave de código sem alterar complexidade',
      ],
    ),
    Habilidade(
      id: EnumHabilidades.corrida.index,
      tipoHabilidade: EnumCodigoHabilidade.manobra,
      nome: "Corrida",
      custo: 1,
      idAtributoBase: 3,
      penalidades: [Penalidades.carga],
      descricao:
          '''Basicamente consiste no emprego de técnicas para correr mais rapidamente ou por mais tempo devido a intensivo treinamento. Uma Habilidade muito desenvolvida por animais corredores como cavalos, mas também extremamente eficaz quando usada por humanoides.

O funcionamento é bem simples: soma-se o bônus do resultado obtido na Velocidade Base final do personagem ou, alternativamente, sobre o número de rodadas que ele é capaz de manter a velocidade máxima em corrida de fundo.

Falha: desequilíbrio ou contusão causa perda de metade da Velocidade Base e impossibilidade de realização de uma corrida curta até que o personagem receba tratamento adequado (sucesso rotineiro em medicina ou 2 horas de descanso);
      ''',
      tarefasAperfeicoadas: [
        'correr em multidões',
        'correr em ladeiras',
        'correr em linha reta',
        'correr com obstáculos',
        'correr contra corrente',
        'correr em locais apertados',
        'correr em terreno acidentado',
        'correr com baixa visibilidade',
      ],
    ),
    Habilidade(
      id: EnumHabilidades.destravarFechaduras.index,
      tipoHabilidade: EnumCodigoHabilidade.subterfugio,
      nome: "Destravar Fechaduras",
      custo: 1,
      idAtributoBase: 0,
      isRestrita: true,
      isTesteSemNivel: false,
      descricao:
          '''O uso desta Habilidade permite a abertura e o fechamento de qualquer tipo de fechadura, tranca ou cadeado. O objeto com o qual se pretende usar a Habilidade deve estar ao alcance do personagem e estar em condições normais de ser destrancado. Ou seja, não se pode abrir fechaduras derretidas ou magicamente seladas).

Para se abrir uma porta é necessário primeiro abrir todas as suas fechaduras. Esta tarefa demora de 1 a 9 rodadas por tentativa dependendo do tipo de tranca ou cadeado (ver abaixo) e pode não ser silenciosa. Descobrir o tipo de tranca que se tem a frente é uma tarefa rotineira que leva 1 rodada. O mesmo teste também pode detectar armadilhas, dependendo de sua complexidade (ver seção de armadilhas). Se o objeto tiver uma armadilha acoplada a si que não tenha sido previamente desarmada, o uso da Habilidade vai dispará-la, potencialmente interrompendo a tentativa. O mestre deve exigir o teste de habilidade apenas depois de passado o tempo necessário para a execução da mesma.

Essa Habilidade é restrita. É necessário que alguém ensine as técnicas necessárias para aprendê-la. Por ser uma arte tipicamente de ladrões, não será fácil se achar alguém que admita que saiba e muito menos que esteja disposto a ensiná-la.

As trancas e cadeados se dividem em enfraquecidas, comuns, boas, seguras e complexas. A diferença, além da dificuldade para abrir, está no tempo que se leva a tentativa. Tentar destrancar trancas ou fechaduras levará 1 rodada para as enfraquecidas, 3 rodadas para as comuns, 5 rodadas para as boas, 7 rodadas para as seguras e 9 rodadas para as complexas. O mestre pode definir que até 5 trancas mais simples no mesmo dispositivo podem ser equivalentes a uma mais complexa e exigir apenas um teste de resolução.

Um personagem pode tentar realizar a tarefa mais rápido. O mestre deve aumentar em um grau a dificuldade para cada rodada que o personagem esteja tentando economizar, até um mínimo de 1 rodada.

Para voltar a travar uma tranca ou fechadura o personagem leva a metade do tempo e realiza um teste com um grau a menos de dificuldade. Em caso de fracasso, a fechadura será considerada um grau mais simples para fins de destravamentos futuros. Uma falha crítica no teste, seja para travar ou destravar, indica que o mecanismo foi emperrado e uma nova tentativa levará 1 hora.

Os graus sugeridos abaixo supõem que o personagem possua equipamentos adequados a disposição, como gazuas para arrombar cadeados ou escutas para cofres. Aumente em um grau a dificuldade se essa prerrogativa não for verdadeira.''',
      tarefasAperfeicoadas: [
        'trancar cofres',
        'trancar cadeados',
        'destrancar cofres',
        'trancar fechaduras',
        'destrancar cadeados',
        'trabalhar sob tensão',
        'destrancar fechaduras',
        'trabalhar em posição desconfortável',
        'trabalhar com ferramentas improvisadas',
      ],
    ),
    Habilidade(
      id: EnumHabilidades.empatia.index,
      tipoHabilidade: EnumCodigoHabilidade.influencia,
      nome: "Empatia",
      custo: 2,
      idAtributoBase: 2,
      descricao:
          '''Empatia é a técnica de reconhecer os sentimentos de outras pessoas, se colocar no lugar delas ou supor suas intenções. Ela também pode ser utilizada para sugerir sentimentos em outras pessoas e comandar a própria linguagem corporal. Um teste de empatia ainda pode ser solicitado para tentar perceber se outra pessoa está mentindo. Contudo, o uso de empatia nunca pode forçar uma pessoa a dizer a verdade, nem mesmo faz com que o usuário a descubra. O resultado da Habilidade apenas indica se há mentira nas palavras do alvo.

Empatia é conhecida por artistas, sacerdotes e espiões. É uma habilidade natural em muitas pessoas, mas que pode ser desenvolvida com treinamento apropriado. Muitas vezes ela será testada de forma resistida contra persuasão ou extrair informação, ou para perceber alguma forma de enganação. A diferença entre o nível obtido pelo usuário de empatia e do personagem em resistência está relacionado a um resultado geral da seguinte forma:

Diferença = Nível obtido pelo usuário de empatia - nível obtido pela resistência.

0 níveis de diferença = Resultado rotineiro

1 nível de diferença = Resultado fácil

2 níveis de diferença = Resultado médio

3 níveis de diferença = Resultado difícil

4 níveis de diferença = Resultado muito difícil

5 ou mais níveis de diferença = resultado absurdo.

Esta habilidade também pode ser utilizada para sedução, que consiste em causar uma atração física e/ou emocional em outra pessoa de modo que esta tentará agradar o sedutor da maneira que puder, desde que não se prejudique. Após o uso bem-sucedido de Sedução, qualquer Habilidade ou teste que envolva o atributo Carisma, será realizado com um Nível a menos de Dificuldade contra a pessoa seduzida.
      ''',
      tarefasAperfeicoadas: [
        'entender sentimentos',
        'seduzir a curto prazo',
        'seduzir a longo prazo',
        'desconfiar de mentiras',
        'exibir próprios atributos',
        'dizer o que se quer ouvir',
        'familiarizar-se com estranhos',
        'transmitir as próprias emoções',
      ],
    ),
    Habilidade(
      id: EnumHabilidades.enganacao.index,
      tipoHabilidade: EnumCodigoHabilidade.subterfugio,
      nome: "Enganação",
      isTesteSemNivel: false,
      custo: 2,
      idAtributoBase: 6,
      descricao:
          '''Enganação é o conjunto de técnicas utilizadas para ludibriar a inteligência de outros indivíduos. Ela engloba as seguintes competências, mas não está limitada a elas: disfarce, mentir, fazer imitações e falsificar objetos ou textos. Para falsificar um objeto o personagem deve ser capaz de construí-lo com a habilidade de arte ou profissional adequada; para falsificar um texto o personagem deve ser capaz de ler no idioma daquela escritura ou sofrer dois níveis de penalidade.

Enganação assume o disfarce de uma habilidade artística na mão de muitos usuários, embora ela seja, na verdade, uma das mais eficientes técnicas de subterfúgio. Ela é conhecida por espiões, assassinos, artistas, falsificadores, traficantes, trambiqueiros em geral e pode ser usada como sustento em muitas situações. Ela guarda algumas semelhanças com a habilidade de persuasão quanto às artes dramáticas. No entanto, a enganação deve ser utilizada para fazer imitações convincentes, enquanto a persuasão deve ser empregada para representar um personagem fictício.

O mestre deverá julgar se a tentativa do personagem é possível de ser realizada antes de estabelecer a dificuldade do teste. Alguns disfarces podem ser impossíveis, outros dependerão da capacidade dos alvos de serem ludibriados. Por exemplo, um pequenino pode tentar disfarçar-se de um ogro em meio a outros ogros, mas qualquer criatura mais inteligente não seria enganado por tal tentativa. Da mesma forma, uma tentativa de imitação será muito mais difícil ou até impossível se realizada contra alguém que conhece o imitado, dependendo do grau de intimidade - Nem o mais burro dos ogros acreditaria em um pequenino passando-se por seu irmão.

O tempo de preparação antes do personagem executar o teste de enganação varia de acordo com o uso. Uma falsificação competente, mas simples como uma assinatura, necessita de pelo menos 30 minutos, enquanto uma complexa, como um selo real, pode levar meses. Um disfarce elaborado necessita de pelo menos 10 minutos de preparo. Além disso, para ser capaz de imitar a voz ou os trejeitos de uma pessoa ou animal o personagem deve observá-lo atentamente em ação por pelo menos 5 minutos.
      ''',
      tarefasAperfeicoadas: [
        'maquiagem',
        'máscaras',
        'falsificar documentos',
        'reconhecer falsificação',
        'mentira deslavada',
        'falsificar objetos',
        'arremedo',
        'imitar animais',
      ],
    ),
    Habilidade(
      id: EnumHabilidades.engenharia.index,
      tipoHabilidade: EnumCodigoHabilidade.profissional,
      nome: "Engenharia",
      isTesteSemNivel: false,
      isRestrita: true,
      custo: 2,
      idAtributoBase: 5,
      descricao:
          '''Com habilidade de Engenharia o personagem será capaz de empreender projetos de edifícios, muralhas e máquinas de guerra. A construção sempre exige muito dinheiro para ser executada e bastante tempo para ser concluída. Essa Habilidade é restrita para grande parte da população e não pode ser aprendida sem que o personagem tenha um tutor. Tais tutores podem ser encontrados em alguns colégios de magias, algumas confrarias, em qualquer academia militar ou nos círculos sociais do alto comércio.

O uso da Habilidade começa com a concepção do projeto. Nesta etapa o Mestre e o jogador discutem a razoabilidade da tentativa, o tempo e a dificuldade para sua completação são estabelecidas. O personagem então realiza um esboço do projeto e, contanto que possua o material e o tempo necessários, começa a orientar os serviços. O esboço deve ser feito em papel ou pergaminho e apenas seu criador será capaz de entender, a menos que o engenheiro também seja hábil em Arte (Desenho).

O teste de habilidade deverá ser realizado em segredo pelo mestre e na metade do tempo previsto para a conclusão da obra. Um fracasso indica que a obra atrasará e custará mais caro. Uma falha crítica significa um acidente catastrófico, com perda de metade do material, possivelmente algumas vidas e forçando o reinício da contagem de tempo em caso de nova tentativa.

Outro uso que pode ser dado à Habilidade de Engenharia é detectar falhas em projetos de terceiros, ou até mesmo encontrar uma falha em uma muralha. O grau de dificuldade fica a cargo do Mestre.''',
      tarefasAperfeicoadas: [
        'projetar muralhas',
        'projetar máquinas de guerra',
        'projetar pontes',
        'projetar edificações',
        'encontrar falhas em projetos',
        'encontrar pontos fracos de estruturas',
        'sabotar máquina de guerra',
        'projetar demolição',
      ],
    ),
    Habilidade(
      id: EnumHabilidades.escalarSuperficies.index,
      tipoHabilidade: EnumCodigoHabilidade.manobra,
      nome: "Escalar Superfícies",
      custo: 2,
      idAtributoBase: 4,
      penalidades: [Penalidades.carga],
      descricao:
          '''Esta Habilidade permite que superfícies verticais ou quase verticais sejam escaladas. Tais superfícies podem ser de origem natural (encostas) ou artificial (muros, paredes, etc.). O Grau de Dificuldade dessa Habilidade é modificado pela disponibilidade de pítons, ganchos, cordas etc. Objetos que facilitam a tentativa. Também é a habilidade empregada quando o personagem tenta mover-se horizontalmente apoiando-se apenas com os braços.

A velocidade de escalada é igual a metade da Velocidade Base do personagem e cada rolamento é válido até que ele atinja sua VB em deslocamento. Um jogador pode anunciar que seu personagem vai tentar subir mais rápido. Nesse caso, o mestre deve aumentar a dificuldade em um grau para cada meia VB de aumento.

Um personagem que sofra qualquer dano na EF enquanto escala deve ser bem-sucedido em uma nova rolagem de Escalar Superfícies ou cairá. Um personagem que chegue a 0 ou menos de EF enquanto escala cai automaticamente.''',
      tarefasAperfeicoadas: [
        'correr na vertical',
        'muralhas',
        'árvores',
        'escalar rápido',
        'escalar sem ferramentas',
        'ferir-se e continuar',
        'auxiliar escalada',
        'planejar escalada',
      ],
    ),
    Habilidade(
      id: EnumHabilidades.escapar.index,
      tipoHabilidade: EnumCodigoHabilidade.subterfugio,
      nome: "Escapar",
      custo: 1,
      idAtributoBase: 0,
      descricao:
          '''Esta Habilidade representa a técnica de se libertar de amarras e outras formas de restrição de movimentos (algemas, cavaletes de madeira, poços repletos de gosma pegajosa, o agarramento de um gigante ou dragão etc.) sem o uso de força bruta, a proeza de passar por ambientes apertados, tais como cavernas, buracos, caixas, janelas pequenas etc., bem como a habilidade para fazer nós diferentes e mais eficientes.

Cada tentativa para escapar de amarras leva pelo menos 15 segundos (uma rodada). Para livrar-se de uma amarra feita por um escapista talentoso, o personagem precisa ser bem-sucedido em um teste resistido contra ele. Um empate significa que o personagem continua amarrado, mas pode tentar de novo em uma rodada. Uma falha significa que ele acabou apertando o nó e a próxima tentativa receberá as penalidades normais, mas vai levar 1 minuto. Cada nova falha dobra o tempo necessário para a próxima tentativa.

Essa Habilidade também pode ser utilizada como uma reação imediata a uma tentativa de agarrão. Nesse caso a tentativa não exige tempo algum e um sucesso indicará que o personagem conseguiu evitar ficar preso. No entanto, um personagem que já esteja agarrado ou preso por qualquer que seja o método segue as regras normais da Habilidade ou, quando em combate, da técnica de imobilização empregada contra ele.''',
      tarefasAperfeicoadas: [
        'livrar-se de cordas',
        'livrar-se de animais',
        'livrar-se de mãos/braços',
        'livrar-se de tentáculos',
        'espremer-se',
        'escapar ou morrer',
        'tentar de novo',
        'escapar com tempo de sobra',
      ],
    ),
    Habilidade(
      id: EnumHabilidades.escrita.index,
      nome: "Escrita",
      custo: 2,
      idAtributoBase: 5,
      isRestrita: true,
      isTesteSemNivel: false,
      tipoHabilidade: EnumCodigoHabilidade.conhecimento,
      descricao:
          '''Escrita é a Habilidade de ler e escrever, mais disseminada em Tagmar do que em nossa idade média mas, mesmo assim, incomum. Essa Habilidade permite ao personagem ler documentos escritos na linguagem aprendida, reconhecer alfabetos, traduzir textos entre os idiomas conhecidos, reconhecer caligrafias etc.

A posse dessa Habilidade permite ao personagem ler cartas, avisos, notas, bilhetes etc., assim como escrevê-los. A dificuldade aumenta quando o personagem tenta ler um texto arcaico mais complexo, escrito num dialeto irmão da língua, porém relativamente diferente etc. Escrita não está relacionada com o talento para escrever (esta é uma arte), mas sim com o conhecimento geral sobre a linguagem escrita.

Os vários idiomas de Tagmar são considerados ramos de conhecimento da habilidade Escrita, sendo que o ramo principal deve ser escolhido no momento da compra do primeiro nível (normalmente o idioma nativo do personagem). O personagem pode escolher 1 ramo secundário adicional a cada 4 pontos que possuir no total em Escrita. Como sempre, os ramos de conhecimento adicionais são testados com um grau de dificuldade a mais. No entanto, o personagem só pode saber escrever em idiomas que saiba falar, ou seja, esta é uma habilidade vinculada a Línguas.''',
      tarefasAperfeicoadas: [
        'ler linguagens antigas',
        'ler escritas ruins',
        'reconhecer alfabeto',
        'traduzir e transcrever',
        'disfarçar a própria escrita',
        'reconhecer escrita',
        'criar alfabetos',
        'ensinar a escrever',
      ],
    ),
    Habilidade(
      id: EnumHabilidades.etiqueta.index,
      tipoHabilidade: EnumCodigoHabilidade.influencia,
      nome: "Etiqueta",
      custo: 1,
      idAtributoBase: 2,
      descricao:
          '''Etiqueta é uma habilidade aprendida através de educação formal que confere ao personagem um conhecimento sobre as regras de conduta em sociedade e também sobre seus símbolos e representantes de destaque. Assim, o personagem saberá como se portar em muitas situações que constrangeriam outras pessoas e consegue a admiração dos envolvidos.

O personagem que tiver essa Habilidade tem conhecimento de como deve se comportar em qualquer sociedade com a qual seja familiar. Bons resultados em etiqueta fazem com que as pessoas tenham uma boa impressão do personagem.

Mesmo quando não se conhecem as regras de conduta locais, quem possui essa Habilidade é capaz de aprendê-las muito rápido, apenas observando outras pessoas. Quanto maior o tempo de observação, mais fácil fica aprender essas "novas" regras. Essa Habilidade também pode ser usada para situar o personagem em culturas muito diferentes da sua, como um Elfo numa reunião de Anões, embora a Dificuldade aumente. Um personagem nobre pode usar essa Habilidade de maneira inversa, isto é, como tratar um plebeu sem ofendê-lo ou irritá-lo.

O uso da Habilidade necessita, acima de tudo de "interpretação" do jogador, pois também depende dele o grau de dificuldade que o Mestre de Jogo irá atribuí-lo. O Mestre poderá dessa forma aumentar a dificuldade caso o jogador faça uma má interpretação.

Etiqueta também deve ser empregada quando o personagem deseja reconhecer símbolos de heráldica de famílias ou recordar os nomes de suas personalidades mais importantes.

Essa Habilidade será provavelmente esquecida pelos jogadores, por isso, o Mestre deverá mostrar-lhes como é importante, isto é, exigir o seu uso em ocasiões onde a sua falta possa constranger os personagens ou colocá-los em situações difíceis.''',
      tarefasAperfeicoadas: [
        'realeza',
        'comerciantes',
        'clericato',
        'plebe',
        'histórias de clãs',
        'heráldica',
        'festas',
        'ambientar-se',
      ],
    ),
    Habilidade(
      id: EnumHabilidades.extrairInformacao.index,
      tipoHabilidade: EnumCodigoHabilidade.influencia,
      nome: "Extrair Informação",
      custo: 2,
      idAtributoBase: 6,
      isRestrita: true,
      descricao:
          '''Esta é a habilidade de fazer um indivíduo revelar informações que não devia, de forma voluntária ou não. Para tal, extrair informação aproveita-se de técnicas de intimidação, interrogatório, lábia e/ou tortura. O mecanismo de utilização da Habilidade é diferente dependendo da abordagem que o personagem empregue.

Para ser eficiente no emprego da Habilidade, o personagem deve concentrar-se inteiramente na tarefa a fim de ser capaz de perceber as reações do alvo e adequar suas ações como consequência. Na prática, isso significa que uma interrupção no uso da técnica pode significar a perda de muitos minutos de trabalho. Obviamente, a vítima deve ter a informação que o personagem deseja para que o uso da Habilidade tenha sucesso. Em todos os casos, um resultado de falha significa que o personagem obteve uma informação falsa e a assumiu como verdadeira, dando a tentativa de uso da habilidade como bem sucedida para aquele questionamento.

O modo mais rápido de utilizar Extrair Informação é com o uso de intimidação. Para tal, o personagem precisa possuir alguma vantagem sobre o intimidado, seja ela músculos poderosos, uma arma perigosa ou um refém em perigo e passar uma rodada intimidando o alvo com palavras ou gestos, deixando claro o que deseja saber. A dificuldade do teste é estabelecida pelo mestre e o resultado deve ser sempre apresentado em uma frase curta, como um nome, uma orientação ou um sim/não.

Para ser capaz de Extrair Informação através de interrogatório, o personagem precisa estar em uma situação de vantagem sobre o indivíduo alvo, seja mantendo-o em cativeiro, possuindo um refém ou outra forma qualquer, desde que o mantenha acordado e em condições de falar. Contudo, o interrogatório não implica em danos físicos, mas sim na obtenção de informações através da identificação de contradições.

Os níveis na habilidade assumem que o interrogador tem condições de interrogar duas ou mais pessoas isoladamente sobre o mesmo assunto, desde que elas possuam conhecimento sobre o tema de interesse. Caso apenas uma pessoa esteja sendo interrogada, o personagem recebe um nível de penalidade nos resultados. O interrogador recebe um nível de dificuldade de bônus se possuir pelo menos quatro interrogados, contando que todos tenham conhecimento sobre o assunto de interesse, a critério do mestre. Cada teste de interrogatório leva no mínimo 5 minutos por interrogado e deve ser realizado no final do interrogatório.

O emprego da tortura difere de um interrogatório na medida que o torturador empregará violência física e/ou emocional para forçar a vítima a revelar a informação desejada. Esta é uma forma mais rápida de obtenção de informações do que o interrogatório, mas que pode resultar em morte ou severos danos físicos e talvez psicológicos à vítima. Contudo, a tortura não tem como premissa provocar dano à vítima, mas sim dor e/ou sofrimento. A melhor tortura é aquela que mantém o alvo vivo.

Para ser capaz de realizar uma tortura, o torturador deverá estar em condições de vantagem sobre seu alvo, mantê-lo imobilizado e em condições de se comunicar. Cada tentativa de obter informação através de tortura exige trinta segundos de perguntas e ameaças, no fim do qual o personagem irá realizar um teste de habilidade. Os resultados sugeridos consideram que o torturador não tentou evitar o dano físico ao torturado. Caso contrário, o resultado sofre um nível de penalidade em seu efeito (um resultado laranja torna-se um amarelo, por exemplo).

Alvos que ainda possuem Energia Heroica não são obrigados a ceder nenhuma informação, exceto no caso de um resultado absurdo no teste de Extrair Informação. Os testes de tortura devem ser realizados como um ataque contra uma defesa igual à moral da vítima (Personagens empregam Resistência à Dor contra tortura e moral contra outras formas de Extrair Informação).

Um indivíduo que é frequentemente submetido à tortura pode desenvolver algum problema psicológico, especialmente o pânico irracional em relação ao seu torturador. Tais efeitos ficam a critério do mestre.

Obs: Uma falha em um teste de tortura provoca 5 pontos de dano na EF do torturado e faz com que ele desmaie por 10 minutos, obrigando a interrupção da sessão de tortura.''',
      tarefasAperfeicoadas: [
        'provocar sofrimento físico',
        'provocar sofrimento psicológico',
        'assustar',
        'confundir',
        'induzir contradições',
        'manter vivo',
        'interrogatório amigável',
        'impor dano psicológico permanente',
      ],
    ),
    Habilidade(
      id: EnumHabilidades.furtarObjetos.index,
      tipoHabilidade: EnumCodigoHabilidade.subterfugio,
      nome: "Furtar Objetos",
      penalidades: [Penalidades.elmo],
      custo: 1,
      idAtributoBase: 0,
      descricao:
          '''É a Habilidade de retirar e/ou colocar objetos no corpo e/ou roupas de uma pessoa sem ser percebido (mas nem sempre), bem como retirar objetos de locais acessíveis sorrateiramente (em cima de um móvel, balcão etc.). Todas as tentativas desta habilidade levam uma rodada.

Personagens que estejam atentos ao personagem furtador têm direito a realizar um teste de Usar os Sentidos resistido contra Furtar Objetos. Um empate significa que o personagem tentando ocultar ou remover o objeto percebeu que não seria bem-sucedido e pode não realizar a tentativa. Um fracasso significa ser pego.

Personagens que não estejam atentos ao personagem que está utilizando a Habilidade não têm direito a teste para perceber o furto.
      ''',
      tarefasAperfeicoadas: [
        'pegar objetos de cintos',
        'plantar objetos em trajes',
        'furtar na multidão',
        'furtar objetos vigiados',
        'trocar objetos de lugar',
        'roubar e correr',
        'furtar de vítimas distraídas',
        'colocar objetos em locais visíveis discretamente',
      ],
    ),
    Habilidade(
      id: EnumHabilidades.jogatina.index,
      tipoHabilidade: EnumCodigoHabilidade.subterfugio,
      nome: "Jogatina",
      custo: 1,
      idAtributoBase: 6,
      penalidades: [Penalidades.elmo],
      descricao:
          '''É a técnica para jogar cartas, jogos de tabuleiro, apostar com segurança e trapacear. Jogos puramente de azar, como aqueles de dados e roleta em geral, não recebem nenhuma forma de ajuda a não ser que o personagem queira trapacear, o que também está incluído nesta Habilidade. Uma última competência de jogatina é a capacidade de estimar probabilidades. Um personagem hábil em jogo terá chances de deduzir se um acontecimento tem mais ou menos chances de ocorrer, contanto que conheça as variáveis.

Para determinar o resultado real de um jogo de azar o jogador deverá anunciar o que deseja fazer. O mestre então anuncia as possibilidades: (a) o jogador deve adivinhar o resultado de um lançamento no d20; (b) o mestre anuncia um número de 1 a 10 e o resultado do lançamento do d20 deverá ser menor do que esta dificuldade; (c) o jogador escolhe par ou ímpar e aguarda o resultado do lançamento do d20.

O jogador poderá usar meios escusos para sair vitorioso num jogo. Inclui-se entre estes métodos utilizar (ou fazer) cartas marcadas e dados viciados. É importante notar que dados viciados têm de ser preparados previamente (teste médio, realizado em segredo pelo mestre) e marcar cartas durante o jogo não só é difícil como também é arriscado. Um jogador que tenha sucesso na tentativa de trapaça ganha o jogo. Se ele obtiver uma falha catastrófica, ele se denunciou de alguma forma (o dado viciado quebrou mostrando os pesos dentro dele, as cartas na manga caíram na frente de todo mundo, etc.). Ninguém reage bem ao ver um trapaceiro em uma mesa de jogo e muitos partem para a violência física, especialmente se qualquer bem estiver em aposta. Os estabelecimentos em que o personagem trapaceiro for identificado irão, muito provavelmente, proibir que ele retorne.

Em um jogo de azar, uma falha comum na tentativa de trapaça não traz consequências e o jogo se desenvolve normalmente. Em um jogo competitivo, como de cartas ou de tabuleiro, uma fracasso comum na tentativa de trapacear significa um atraso para o trapaceiro, representado na forma das penalidades aplicadas em novas tentativas. No entanto, um personagem que esteja atento ao trapaceiro pode testar Jogatina de forma resistida para tentar identificar a trapaça. Ele detectará a fraude se seu resultado for superior ao teste de jogatina do trapaceiro.

Opcionalmente, o mestre poderá estabelecer que em jogos competitivos o personagem deverá realizar o teste resistido contra a habilidade do oponente (ou de outro jogador). Neste caso os resultados deverão ser interpretados da seguinte maneira: um empate significa que o jogo prossegue sem avanço para nenhum dos lados. As apostas, no entanto, devem ser aumentadas. Cada tentativa em um jogo deste tipo leva no mínimo 20 minutos e cada tentativa de desempate subsequente leva pelo menos mais 10 minutos.
      ''',
      tarefasAperfeicoadas: [
        'bom em jogo(s) de carta',
        'bom em jogo(s) de tabuleiro',
        'trapacear',
        'deduzir probabilidades',
        'procurar trapaceiro',
        'preparar conjunto de jogo para trapaça',
        'blefar em jogos',
        'aumentar apostas',
      ],
    ),
    Habilidade(
      id: EnumHabilidades.lidarComAnimais.index,
      tipoHabilidade: EnumCodigoHabilidade.geral,
      nome: "Lidar com Animais",
      custo: 1,
      idAtributoBase: 2,
      descricao:
          '''Esta Habilidade compreende todo tipo de interação com animais. Lidar com Animais permite ao personagem conhecer características dos animais, saber como se portar em determinadas situações e treiná-los para a realização de atividades de complexidade variável. Empregando um esforço muito maior, lidar com animais também pode ser empregada para interagir com outras criaturas irracionais.

Uma criatura é capaz de aprender sem grandes dificuldades até 3 truques após ser treinada por um conhecedor desta habilidade. No entanto um único truque leva 4 semanas para ser ensinado e é necessário um sucesso fácil modificado de acordo com o tipo de criatura (vide descrição dos graus de dificuldade, abaixo). O teste de habilidade é realizado apenas no final das 4 semanas. Se o sucesso for obtido, a criatura aprendeu um truque. Se a tentativa fracassou, a criatura não aprendeu o truque e o treinador pode tentar de novo, respeitando o tempo de treinamento. Se o resultado da tentativa for uma Falha Crítica (verde), a criatura não só não aprendeu o truque, como jamais o aprenderá deste treinador. Qualquer truque além do terceiro segue o mesmo procedimento, mas os testes de habilidade são um grau mais difíceis para cada novo truque. No entanto, a dificuldade máxima para aprender do 4º ao 6º truque é muito difícil e qualquer truque posterior só pode ser ensinado com um resultado absurdo no teste de habilidade.

O grau de dificuldade desta habilidade também depende de dois fatores: se o animal foi criado desde pequeno em cativeiro ou se ele foi capturado depois de adulto, e se ele é herbívoro, onívoro ou carnívoro. Testes que envolvam animais místicos são realizados com um grau a mais de Dificuldade.
      ''',
      tarefasAperfeicoadas: [
        'lidar com grupos',
        'treinar animais',
        'animais gigantes',
        'criaturas místicas',
        'acalmar os ânimos',
        'tratar animais',
        'lidar com animais de fazenda',
        'distrair criatura',
      ],
    ),
    Habilidade(
      id: EnumHabilidades.lideranca.index,
      tipoHabilidade: EnumCodigoHabilidade.influencia,
      nome: "Liderança",
      custo: 1,
      idAtributoBase: 2,
      descricao:
          '''Para aqueles que são colocados em posição de comando, não há Habilidade mais essencial que a Liderança. É através dela que você impõe respeito e confiança no coração dos seus subordinados. Ela também envolve a eloquência e a capacidade de falar e atrair a atenção. Um líder tem a eloquência para reunir multidões em seus discursos, ou apenas para distrair um trio de guardas enquanto seus companheiros invadem o local às suas costas, conforme a conveniência.

A Liderança é muitas vezes reconhecida como um dom. Contudo, ela é uma habilidade treinável e aprendida especialmente por soldados, bardos e sacerdotes, mas todos os grandes líderes, inclusive reis e os políticos, possuem algum grau desse talento. A simples experiência é uma grande fonte de aprendizado de liderança.

O uso da eloquência envolve uma conversa com início, meio e fim que deve durar entre um minuto e uma hora. Em menos tempo, não é possível cativar a plateia e em mais tempo o público começa a se distrair e perder o interesse. Em termos de jogo, isso significa que em qualquer momento fora dessa faixa de tempo o personagem não receberá os benefícios da Habilidade.

Um sucesso no teste de liderança exercendo a eloquência indica que a(s) pessoa(s) estão cativadas pelo discurso ou conversa do personagem e terão aumento de dois graus de dificuldade em qualquer teste de percepção realizados para detectar ameaças não imediatas. Um discurso interrompido no meio (por alguém que queria boicotar o personagem, por exemplo) exige um teste resistido de liderança. Aquele que obtiver sucesso cativa o público. Um empate significa que nenhum dos dois cativou a plateia e que ela começa a se dispersar, mas ainda não foi perdida. Um novo empate em um teste realizado um minuto depois (ou fracasso do Personagem, se o objetivo do intruso era apenas atrapalhar) dispersa a plateia e o período de 1 minuto deverá ser novamente respeitado, ou seja, o uso da habilidade começa do início.

A liderança também é a habilidade chave para motivar e comandar tropas e exércitos. Durante a violência dos combates, os seres muitas vezes se intimidam, se acovardam ou a auto preservação fala mais alto. As regras para moral e combate em massa estão descritos na seção Mecânica de Jogo.

Um líder que possua muitos comandantes e subcomandantes deve ser bem-sucedido em um teste de Liderança para transmitir uma mensagem específica corretamente através de sua hierarquia vertical. A dificuldade será rotineira para os que estão imediatamente abaixo do líder na escala de comando, mas a dificuldade aumenta em um grau para cada hierarquia abaixo desta. Por exemplo, um general controla quatro escalas verticais de comando abaixo dele. Para transmitir uma mensagem corretamente para todas as quatro camadas ele deverá ser bem-sucedido em um teste difícil de Liderança (rotineiro para a imediatamente abaixo, fácil para a segunda, médio para a terceira e difícil para a quarta). Uma mensagem transmitida dessa forma leva 1 hora para atingir completamente cada hierarquia abaixo.

Observe, no entanto, que nem sempre uma mensagem precisa chegar ao mais baixo escalão de uma hierarquia de comandados para surtir efeito. Por exemplo, um general pode planejar uma estratégia para flanquear seus inimigos e apenas os comandantes das tropas precisarão ficar sabendo dos detalhes, como sinais que receberão para entrar em batalha ou onde deverão permanecer até que partam para a luta. A partir daí será a habilidade de comando desse general que deverá entrar em cena.
      ''',
      tarefasAperfeicoadas: [
        'comandar soldados',
        'comandar conscritos',
        'inspirar confiança',
        'inspirar medo',
        'discursar para multidões',
        'conquistar pequenos grupos',
        'manter moral alta',
        'transmitir mensagens entre comandados',
      ],
    ),
    Habilidade(
      id: EnumHabilidades.linguas.index,
      nome: "Línguas",
      tipoHabilidade: EnumCodigoHabilidade.conhecimento,
      custo: 2,
      idAtributoBase: 5,
      isTesteSemNivel: false,
      descricao:
          '''Este é o conhecimento dos vários idiomas de Tagmar e o quanto o personagem é hábil em compreendê-los ou reconhecê-los.

Os testes de língua deverão ser invocados sempre que o personagem tentar discernir um diálogo na multidão, compreender alguém com a dicção muito prejudicada falando ou, mesmo ouvindo, tentar compreender um cochicho, conversa em baixo volume ou muito distante. Ressalta-se que ela não serve para perceber que alguém está falando, mas sim compreender o que se está falando. A habilidade também deve ser testada quando o personagem tenta disfarçar um sotaque ou falar em um dialeto próximo de um idioma que conhece. Finalmente, Línguas serve para o personagem tentar ler os lábios de alguém que esteja falando um idioma conhecido. Contudo, a distância máxima para realizar uma tentativa como essa é de 10 metros e o personagem não consegue entender palavras em outros idiomas, nomes próprios únicos ditos durante a conversa ou reconhecer sotaques.

Os vários idiomas de Tagmar são considerados ramos de conhecimento da habilidade Língua, sendo que o idioma nativo do personagem é o ramo principal. O personagem pode escolher 1 ramo secundário adicional a cada 4 pontos que possuir no total em Línguas, contanto que tenha gasto pelo menos 1 ponto na habilidade. Como sempre, os ramos de conhecimento adicionais são testados com um grau de dificuldade a mais.

Por exemplo, um elfo de intelecto 4 que não gastou pontos na habilidade testará Língua (élfico) na coluna 4, mas não tem ramos de conhecimento adicionais porque não gastou nenhum ponto em Língua. por outro lado, se este mesmo elfo gastar pontos para adquirir o nível 1 na habilidade ele terá um total de Línguas (élfico) igual a 5. Por isso, ele terá um ramo de conhecimento secundário, que ele opta por ser o Verrogari. Todos os testes que ele fizer para compreender o Verrogari são realizados com um grau de dificuldade a mais. Quando este elfo adquire o 4º nível em Línguas (élfico), ele escolhe um segundo ramo de conhecimento, pois agora seu total é (4 + Intelecto = 8). Ele opta por Lanta e o jogador anota em sua ficha de personagem.

Os personagens podem se comunicar livremente em seus idiomas tanto do ramo de conhecimento principal quanto do secundário. No entanto, um nativo sempre perceberá o sotaque e reconhecerá que aquele não é o idioma nativo do personagem. No entanto, é necessário um teste para reconhecer a qual idioma pertence aquele sotaque, quando não for óbvio, a critério do mestre (provavelmente um elfo terá sotaque de élfico, por exemplo). Para evitar esse fato, o jogador pode optar por comprar várias vezes a habilidade Línguas. Quando ele faz isso, o idioma para de ser considerado secundário e o personagem perde o sotaque.

Por exemplo, o mesmo elfo viu-se em uma situação de preconceito em Verrogar por carregar um forte sotaque élfico, uma vez que possui o Verrogari como idioma secundário. O jogador optou por comprar o nível 1 na habilidade Línguas novamente, desta vez em Verrogari. Agora o elfo não tem mais sotaque neste idioma, mas a coluna de resolução do Verrogari passa a ser o da nova habilidade (1 + intelecto = 5). O malês sempre carrega o sotaque de sua língua mãe, independente de ser idioma principal ou secundário. É possível disfarçar um sotaque com um sucesso médio em Línguas, mas um fracasso torna a situação constrangedora e uma falha crítica gera um mal entendido, provavelmente pelo mal uso de alguma expressão típica.

Apenas o idioma de nível mais alto concede ramos de conhecimento secundários.''',
      tarefasAperfeicoadas: [
        'falar bonito',
        'compreender conversa',
        'disfarçar sotaque',
        'ler lábios',
        'reconhecer idiomas',
        'reconhecer sotaques',
        'falar um dialeto',
        'traduzir simultaneamente',
      ],
    ),
    Habilidade(
      id: EnumHabilidades.malabarismo.index,
      tipoHabilidade: EnumCodigoHabilidade.manobra,
      nome: "Malabarismo",
      custo: 2,
      idAtributoBase: 0,
      penalidades: [Penalidades.carga, Penalidades.elmo],
      descricao:
          '''Esta é a habilidade da grande destreza com as mãos e os pés, bem como truques de prestidigitação e outras mágicas circenses menores; Malabarismo deve ser empregado em situações que exijam um grande esforço sobre a coordenação motora do personagem, como pegar algo caindo ou que foi arremessado. Geralmente, mas nem sempre, o uso de malabarismo está vinculado ao manuseio de algum objeto, em contraste com acrobacias, que está geralmente vinculada a destreza corporal sem o uso de objetos;

Essa habilidade também pode ser utilizada para fazer movimentos elaborados com suas armas. Os movimentos são puramente visuais e não provocam efeito prático em combate, mas são muito bonitos de se assistir e podem ser empregados para ganhar o respeito de uma plateia, bem como para simular combates de forma artística. O mestre deve exigir um teste de malabarismo sempre que o personagem tenta um movimento incomum e exuberante com sua arma. Para isso, ele deve empregar a menor coluna de resolução entre seu total na arma manuseada e em malabarismo.

Malabarismo pode ser empregada em combate em duas situações: (a) para chamar a atenção para algum movimento de sua arma com um movimento desnecessário, mas bonito ou (b) para pegar um objeto arremessado contra ele.

A primeira situação exigirá um teste de malabarismo a qualquer momento durante o combate durante o turno do malabarista, mas antes de qualquer ataque, se houver a intenção de realizar um. Uma falha indica que o personagem perdeu o domínio de sua arma na tentativa e se feriu, causando 50% de dano em si mesmo e abrindo a oportunidade para o oponente desferir-lhe um ataque oportuno. Adicionalmente o malabarista perde a oportunidade de atacar nesta rodada e atrai algumas risadas pelo desastre. Qualquer sucesso melhor chama a atenção de uma eventual plateia, possivelmente agregando fama ao personagem. Alguns gladiadores fazem uso desta técnica exatamente desta forma.

Um personagem também é capaz de pegar qualquer objeto arremessado contra si que não o tenha atingidodiretamente na EF. Para isso, é preciso obter um sucesso em malabarismo com dificuldade média para objetos aerodinâmicos ou muito difícil para objetos pesados ou com poucos locais para segurá-los, como armas médias. Uma falha crítica significa que o malabarista não conseguiu segurar o objeto e, em vez disso, foi atingido por ele nas mãos. O objeto provoca o dano diretamente na EF em vez de provocar na EH e causará 25% caso o ataque contra ele tenha obtido um resultado rotineiro. Adicionalmente o malabarista receberá -2 colunas de penalidade em qualquer ação que dependa de suas mãos, inclusive ataques, até que receba tratamento simples (sucesso rotineiro de medicina para aplicar sutura e ataduras).
      ''',
      tarefasAperfeicoadas: [
        'mágico de circo',
        'malabarista de circo',
        'pegar objetos arremessados contra você',
        'impressionar com armas leves',
        'impressionar com armas médias',
        'impressionar com armas pesadas',
        'malabarismo com cordas, correntes e chicotes',
        'manipular objetos com os pés',
      ],
    ),
    Habilidade(
      id: EnumHabilidades.manusearArmadilhas.index,
      tipoHabilidade: EnumCodigoHabilidade.geral,
      nome: "Manusear Armadilhas",
      custo: 2,
      idAtributoBase: 6,
      isTesteSemNivel: false,
      penalidades: [Penalidades.elmo],
      descricao:
          '''Esta Habilidade permite localizar, criar e desarmar todo tipo de mecanismos secretos, como armadilhas e passagens secretas. Para se desarmar uma armadilha ou passagem secreta é necessário que a mesma tenha sido localizada anteriormente. Um personagem pode desarmar uma armadilha apenas se ele mesmo a localizou. A Habilidade também permite ao personagem, construir uma armadilha ou rearmar uma desativada.

A pessoa que está tentando averiguar se existe uma armadilha em certo local, procede da seguinte maneira: ele diz o seu nível nesta Habilidade ao Mestre, que faz o rolamento da Habilidade de maneira que o jogador não possa ver o resultado. Se houver mais de uma armadilha no mesmo local, a Habilidade deve ser tentada uma vez para cada armadilha. Serão detectadas apenas as armadilhas nas quais o rolamento obtiver sucesso. Além disso, independentemente da tentativa resultar em sucesso ou fracasso, o usuário sempre pensará que teve sucesso na mesma (lembre-se: o Mestre rolará de maneira que o jogador não possa ver o resultado e agirá de acordo com este).

Essa Habilidade também permite que se torne inofensiva e/ou inoperante uma dada armadilha. A dita armadilha tem que ter sido encontrada primeiro. Note que, como no parágrafo anterior, o jogador não deve saber se a tentativa fracassou ou não. Para isso, o Mestre não deve se esquecer de fazer o rolamento de maneira que os jogadores não possam ver o resultado.

Como alternativa ao desarme, o personagem pode tentar modificar uma armadilha, acrescentando ou removendo mecanismos e etapas, como substituir setas comuns por venenosas, por exemplo.

Os Graus de Dificuldade abaixo listados são para condições ideais, devendo ser alterados para levar em conta fatores como pressa, pressão externa (perseguição, combate etc.), acesso aos mecanismos da armadilha e outros.
      ''',
      tarefasAperfeicoadas: [
        'detectar armadilhas',
        'desarmar armadilhas',
        'trabalhar sob pressão',
        'construir armadilhas simples',
        'construir armadilhas complexas',
        'inventar armadilhas',
        'sabotar armadilha',
        'modificar armadilha',
      ],
    ),
    Habilidade(
        id: EnumHabilidades.medicina.index,
        tipoHabilidade: EnumCodigoHabilidade.profissional,
        nome: "Medicina",
        custo: 1,
        idAtributoBase: 5,
        isTesteSemNivel: false,
        isRestrita: true,
        descricao:
            '''As técnicas de cura da antiguidade são muito pouco efetivas comparadas às de hoje, mas ainda assim podem representar a diferença entre a vida e a morte. A Habilidade Medicina inclui: reconhecer enfermidades, diagnosticar lesões, tratar ferimentos, curar doenças, fazer uso de ervas medicinais e salvar indivíduos à beira da morte.

No entanto, devido a sua grande amplitude de saberes, a medicina divide-se em três ramos de conhecimento que se interseccionam até certo ponto. Com isso, todo médico conhece o ramo Primeiros Socorros. No entanto, ele deve optar entre Cura ou Herbalismo como ramo primário. Tarefas referentes ao outro ramo serão testadas como seu ramo secundário e testadas com um nível de dificuldade a mais.

O jogador deve ter em mente que a escolha do ramo deverá orientar a forma que o personagem conhece a medicina. Por exemplo, um herbalista pode tentar curar 1 ponto de EF de um paciente após 8 horas de tratamento (tarefa rotineira de Cura) através de um sucesso Fácil em herbalismo. No entanto, o herbalista fará o mesmo serviço valendo-se de técnicas naturais, empregando plantas e remédios em vez de ataduras e procedimentos médicos comuns.

As condições em que a recuperação se processa influenciam muito no resultado. É mais fácil tratar um paciente numa cama com um teto sobre sua cabeça do que nos ermos sob clima adverso. Utensílios, ervas e remédios também afetam o Nível de Dificuldade da recuperação. Durante uma estabilização (Energia Física entre -11 e -15), caso ocorra uma Falha (resultado de cor Verde), o paciente morre imediatamente.

A abrangência de cada ramo de conhecimento são:
      ''',
        tarefasAperfeicoadas: [
          'diagnosticar',
          'estabilizar',
          'tratar doenças',
          'encontrar plantas medicinais',
          'preparar remédios',
          'tratar sob condições não ideais',
          'realizar procedimentos cirúrgicos',
        ],
        areas: [
          AreaHabilidade(
            id: 4,
            nome: 'Primeiros Socorros',
            descricao:
                'Os conhecimentos básicos que todo médico competente possui. Compreende a prática necessária para identificar e tratar dores, sintomas e ferimentos de batalhas evidentes. Os primeiros socorros compreendem um ramo da medicina de aplicação rápida e imediata, que inclui o uso de ataduras, ferramentas e fios de corte e de imobilização. Comparado com outros usos da medicina, os primeiros socorros pouco dependem de equipamentos específicos, uma vez que a maioria deles pode ser improvisado, a critério do mestre, e ter a tentativa de uso realizada com um nível a mais de dificuldade. Estabilizar pacientes à beira da morte também é uma tarefa básica de medicina realizada com primeiros socorros.',
          ),
          AreaHabilidade(
            id: 5,
            nome: 'Cura',
            descricao:
                'O ramo de conhecimento da medicina de Tagmar trata do cuidado e tratamento intensivo de doentes e feridos. Com ela os personagens podem indicar o comportamento correto que um paciente deve assumir a fim de ter sua condição melhorada. A recuperação da EF no tempo deve ser tentada com um teste de Medicina (Cura). A realização de pequenas cirurgias também é uma tentativa de cura. Os tratamentos e cirurgias ocasionalmente levam à perda de alguns pontos de EF em troca da recuperação de uma enfermidade, pois muitas vezes elas valem-se de sangrias.',
          ),
          AreaHabilidade(
            id: 6,
            nome: 'Herbalismo',
            descricao:
                'O conhecimento e a prática de preparar e empregar o extrato de ervas medicinais a fim de produzir remédios e anestésicos e utilizá-los corretamente. Os remédios podem ser utilizados para acelerar a regeneração natural de uma doença, vício ou ferimento. Diferente dos outros dois ramos desta habilidade, Medicina (Herbalismo) não possui um extenso conjunto de tradições e técnicas escritas. Ao contrário, a maior parte do que sabe-se sobre herbalismo vem de tradição oral, transmitida principalmente em sociedades tribais, o que acaba por tornar muito difícil um personagem urbano ter conhecimento sobre ela.',
          ),
        ]),
    Habilidade(
      id: EnumHabilidades.misticismo.index,
      tipoHabilidade: EnumCodigoHabilidade.conhecimento,
      nome: "Misticismo",
      custo: 1,
      idAtributoBase: 5,
      isRestrita: true,
      descricao:
          '''Misticismo é o conhecimento dos caminhos e componentes do uso de magia, das fórmulas mágicas e dos produtos alquímicos. Nem toda pessoa que sabe Misticismo pode usar magia, mas todo Mago precisa sabê-lo. Testes que envolvam magias discretas recebem dois graus a mais de Dificuldade.

Como um conhecimento, o Misticismo baseia-se em constatações físicas. Ele é um estudo técnico e teórico com vieses práticos utilizado por acadêmicos para estudar e reproduzir efeitos sobrenaturais. No entanto, ele não utiliza a aura do evocador, ou seja, o conhecedor de Misticismo não precisa ter qualquer dom sobrenatural para possuir o conhecimento. Mesmo assim, esta é uma habilidade conhecida por muito poucos.

O uso da alquimia é uma derivação do Misticismo. Para empregá-lo com sucesso para este fim, o personagem precisa dispor de equipamento adequado, o que inclui um laboratório equipado pelo menos com frascos limpos, fonte de fogo, água e qualquer outro sólido ou líquido exótico que o mestre julgue necessário.''',
      tarefasAperfeicoadas: [
        'preparar misturas alquímicas',
        'destilar misturas',
        'reconhecer vestígios de magias',
        'magias arcanas não infernais',
        'magias divinas',
        'magias arcanas infernais',
        'identificar criatura mágica',
        'reconhecer item arcano',
      ],
    ),
    Habilidade(
      id: EnumHabilidades.montarAnimais.index,
      tipoHabilidade: EnumCodigoHabilidade.manobra,
      nome: "Montar Animais",
      custo: 1,
      idAtributoBase: 3,
      penalidades: [Penalidades.carga],
      descricao:
          '''Esta Habilidade permite ao personagem montar todo tipo de animais como: cavalos, camelos, elefantes, grifos, pégasus e outros, sendo sua base de dificuldade os equinos (cavalos, pôneis, mulas, burros etc.) em geral. Assim, o Grau de Dificuldade cresce quando o animal não é um equino (controlar um cavalo dócil numa estrada é uma tarefa Rotineira, fazer o mesmo com um elefante é uma tarefa Fácil, mas conduzir um grifo em voo calmo exige um teste Médio). A habilidade também é empregada quando o personagem deseja conduzir criaturas através de rédeas, como uma carroça. Neste caso, os testes são ajustados para um grau de dificuldade a menos.

Outro fator a ser considerado é o tamanho da montaria contra o tamanho do montador. Isto é, um Pequenino pode montar um minúsculo pônei com desembaraço, mas terá muitas dificuldades em repetir o feito com um grande cavalo de guerra dos Humanos. Fica a cargo do bom senso dos jogadores, e da decisão final do Mestre, que alterações deverão ser feitas nos graus de dificuldade devido a fatores físicos dos personagens.

Personagens que falhem em testes nesta Habilidade não poderão conduzir ou controlar o animal de maneira alguma, na verdade, o personagem é apenas carregado pelo animal, estando mesmo um pouco à mercê deste.

O Grau de Dificuldade pode ser alterado por ser um animal xucro ou treinado. Deste modo, montar um cavalo de guerra é uma tarefa um grau mais fácil do que montar um cavalo normal e, por fim, em situações de corridas o total do personagem em Montar Animais deve ser somado ao total em Corrida da montaria.
      ''',
      tarefasAperfeicoadas: [
        'montar animal indomado',
        'montar criatura exótica',
        'saltar com a montaria',
        'controlar montaria assustada',
        'correr com a montaria',
        'desmontar',
        'conduzir montado e sem rédeas',
        'conduzir várias criaturas, com rédeas',
      ],
    ),
    Habilidade(
      id: EnumHabilidades.natacao.index,
      tipoHabilidade: EnumCodigoHabilidade.manobra,
      nome: "Natação",
      custo: 1,
      idAtributoBase: 3,
      isTesteSemNivel: false,
      penalidades: [Penalidades.armadura, Penalidades.carga],
      descricao:
          '''Esta Habilidade permite ao personagem sobreviver e deslocar-se em águas profundas. Quem não possui essa Habilidade simplesmente não sabe nadar. Águas profundas são aquelas em que um personagem não possa apoiar-se no fundo mantendo sua cabeça fora d'água. Essa Habilidade é, por motivos óbvios, altamente influenciada pelo tipo de armadura utilizada, não podendo executá-la aquele que estiver vestindo uma cota de malha parcial ou armadura mais pesada. O grau de dificuldade aumentará de acordo com o peso dos objetos que estejam sendo carregados pelo personagem (um saco, uma mochila ou mesmo outra pessoa).

Um fracasso no teste de habilidade significa que o personagem está cansando e engoliu água. Ele perde 2 pontos de EF por afogamento e avança no máximo a metade de sua VB, mas pode repetir o teste na próxima rodada. A EF perdida desta forma acumula para o cálculo das penalidades aplicadas por afogamento, conforme explicado no capítulo Mecânica de Jogo. Finalmente, um novo teste de habilidade deve ser realizado quando o personagem percorre uma distância de até 10 x VB nadando, ou até 5 x VB mergulhando.

A velocidade máxima que um personagem consegue se deslocar nadando ou mergulhando sem o apoio de magia é igual a sua Velocidade Base.
      ''',
      tarefasAperfeicoadas: [
        'nadar contra a correnteza',
        'nadar a favor da correnteza',
        'mergulhar',
        'não desistir (tentar novamente)',
        'nadar em ondas',
        'desviar de pedras',
        'nadar por muito tempo',
        'nadar para a superfície',
      ],
    ),
    Habilidade(
      id: EnumHabilidades.nautica.index,
      tipoHabilidade: EnumCodigoHabilidade.profissional,
      nome: "Náutica",
      custo: 1,
      idAtributoBase: 5,
      isTesteSemNivel: false,
      isRestrita: true,
      descricao:
          '''Náutica confere a capacidade de utilizar e comandar uma embarcação e sua tripulação. Essa Habilidade é restrita para grande parte da população e não pode ser aprendida sem que o personagem tenha um tutor. O conhecimento de náutica pode ser ensinado por piratas, corsários e alguns militares que sobrevivem e patrulham no Lago Denégrio e nos mares de Tagmar;

Um sucesso no teste de Habilidade significará que o personagem conseguiu executar (em caso de embarcações pequenas) ou comandar (em caso de embarcação de grande porte) uma ação bem sucedida.

Existe uma exigência para que a tentativa seja executada com sucesso: a tripulação designada para agir tem de ser suficiente para o tipo de embarcação. No entanto, se o número de marujos for menor que o número da tripulação ideal, haverá um acréscimo de um Grau de Dificuldade na Habilidade (Rotineiro torna-se Fácil, Fácil torna-se Médio, etc.).
      ''',
      tarefasAperfeicoadas: [
        'operar pequena embarcação',
        'comandar tripulação',
        'manter rota',
        'mapear rota',
        'abordar embarcação',
        'posicionar para combate a distância',
        'cruzar tempestades',
        'navegar a velocidade máxima',
      ],
    ),
    Habilidade(
      id: EnumHabilidades.navegacao.index,
      tipoHabilidade: EnumCodigoHabilidade.geral,
      nome: "Navegação",
      custo: 1,
      idAtributoBase: 6,
      descricao:
          '''Navegação indica o senso de direção do personagem e sua capacidade de orientação espacial. Todos os testes supõem que o personagem está na superfície. Com exceção dos anões, qualquer indivíduo que tente realizar um teste de navegação em ambientes subterrâneos deverá realizá-lo com um grau de dificuldade a mais.

O possuidor dessa Habilidade pode utilizar a posição dos astros, para determinar direções cardeais ou sua posição aproximada. O erro é de aproximadamente 500 quilômetros, quando no mar. Ambas as opções são influenciadas pelas condições do tempo.

Navegação também pode ser usada para que o personagem se oriente em cidades desconhecidas, florestas, cavernas, labirintos e etc. Obviamente essa Habilidade não indica o caminho certo, mas impede que o usuário fique andando em círculos ou se perca completamente, não sabendo mais de que direção veio.

Essa Habilidade também inclui o uso e confecção de mapas para uso próprio. Isso, combinado com as duas outras utilidades, permite que seja usada para orientação durante uma viagem, seja ela por mar ou por terra. O uso de mapas alterará substancialmente o grau de dificuldade na execução da Habilidade. Note que existem magias que podem auxiliar nesta tarefa.
      ''',
      tarefasAperfeicoadas: [
        'orientar-se pelas estrelas',
        'mapear e ler mapas',
        'orientar-se no subterrâneo',
        'estabelece pontos de referência em terra',
        'estabelece pontos de referência no mar',
        'percorrer labirintos',
        'confiar nos instintos',
        'reconhecer erros de cartografia',
      ],
    ),
    Habilidade(
      id: EnumHabilidades.negociacao.index,
      tipoHabilidade: EnumCodigoHabilidade.influencia,
      nome: "Negociação",
      custo: 1,
      idAtributoBase: 2,
      descricao:
          '''Negociação é o tino do personagem para a realização de trocas comerciais e escambos favoráveis. Ela tem dois objetivos principais. O primeiro é a capacidade de determinar o valor comercial de um item, sua autenticidade e onde é possível comprá-lo ou vendê-lo. O segundo é o talento para negociar preços a fim de vender um item mais caro ou comprar mais barato do que seu preço de mercado. O Mestre deve determinar os benefícios que um resultado vantajoso de negociação gere em um escambo, podendo ser utilizado o preço dos itens apresentados na seção Pertences e Afins.

Determinar preço e autenticidade de um item é uma tarefa que exige tempo. Dependendo do objeto analisado, o personagem ainda pode precisar de equipamento especial como lupas, balanças ou outras ferramentas, a critério do mestre. A dificuldade da rolagem deve ser determinada pelo Mestre antes do lançamento do dado, mas não deve ser revelada ao jogador para que este não saiba se a avaliação foi condizente com o real preço comercial do item. O mesmo é válido para a determinação da autenticidade de um artefato qualquer.

Para barganhar em uma negociação de preços o personagem precisa rolar um teste resistido contra seu comprador ou vendedor. A diferença de resultados resultará em um acréscimo ou redução de preço em relação ao preço justo do item, estabelecido pelo Mestre, de acordo com a seguinte tabela de orientação:

Empate: vender ou comprar uma mercadoria pelo preço justo.

Diferença de 1 Nível de Dificuldade: vender com preço 10% maior; comprar um item por 90% do valor.

Diferença de 2 níveis de dificuldade: vender com preço 25% maior; comprar um item por 80% do valor.

Diferença de 3 níveis de dificuldade: vender um item com preço 50% maior; comprar um item por 70% do valor.

Diferença de 4 níveis de dificuldade: vender um item com preço 75% maior; comprar um item por 60% do valor.

Diferença de 5 níveis de dificuldade: vender um item com preço 100% maior (o dobro); comprar um item por 50% do valor (metade).

Diferença de 6 níveis de dificuldade: vender um item com preço 200% maior (o triplo); comprar um item por 30% do valor.

Os outros usos de negociação envolvem uma dificuldade estabelecida pelo Mestre inspirada nas seguintes orientações:
      ''',
      tarefasAperfeicoadas: [
        'mercadorias baratas',
        'pinturas e esculturas',
        'armas e armaduras',
        'pedras preciosas',
        'joias',
        'somas vultosas',
        'extorquir',
        'escambo',
      ],
    ),
    Habilidade(
      id: EnumHabilidades.operacaoDeCerco.index,
      tipoHabilidade: EnumCodigoHabilidade.conhecimento,
      nome: "Operação de Cerco",
      custo: 1,
      idAtributoBase: 6,
      isTesteSemNivel: false,
      isRestrita: true,
      descricao:
          '''A Operação de Cerco, também conhecida pelo nome de Poliorcética, é uma habilidade ampla que envolve o conjunto de técnicas, conhecimentos e habilidades militares empregadas para realizar ou defender-se(antipoliorcética) de um cerco. O conhecimento da habilidade também deve ser exigido para coordenar adequadamente um pequeno grupo de operadores de dispositivos de guerra (balestras, catapultas, torres de cerco, aríetes e similares),quando estes equipamentos necessitarem de várias pessoas para o correto funcionamento, bem como para prever quais são os pontos estratégicos mais importantes de serem mantidos quando se executa um cerco ou defende-se contra ele.

Essa é uma habilidade com um viés prático, a de realizar operações militares ao locomover armas e coordenar suas operações, mas também teórico, como o de determinar pontos estratégicos mais valiosos para uma ofensiva ou defensiva de cerco. Utiliza-se a habilidade liderança para que um personagem consiga, ele mesmo, pôr em prática a teoria através da orientação a seus comandados. Um teste dessa Habilidade tem uma validade de 1 hora. O jogador não pode pedir outra tentativa antes que esse tempo tenha se passado.

O mestre deve exigir apenas um teste para cada grande grupo de máquinas iguais que se deseja preparar e disparar pelo período de validade do teste. Um sucesso indica que o personagem não cometerá erros durante as operações; um fracasso indica que ele se atrapalha e todos os envolvidos recebem uma penalidade em 1 nível em seus testes que envolvam as máquinas operadas (uma balestra está com a mira descalibrada, uma torre de cerco está muito longe do muro etc.); uma falha crítica indica que as máquinas enguiçaram e não podem operar ou se mover até serem consertadas por um carpinteiro (leva 1 hora). Esta habilidade não deve ser empregada como ataque, para apontar e atingir um alvo, mas sim para operar corretamente.

Quando movimenta tropas e estabelece pontos estratégicos para guardar posições, um bom operador de cerco leva em consideração o potencial ofensivo de seu oponente, o que inclui o alcance das magias de evocadores, no momento de montar e mover seus pelotões, estruturas e armas. Um fracasso em um teste com esta finalidade indica que o oponente terá livre movimentação naquele período e não terá dificuldade em receber mantimentos ou atacar com os recursos que possui. Uma falha crítica não só faz a operação não surtir efeito, como também abre uma brecha nas próprias defesas, concedendo um bônus de 1 nível em qualquer movimentação inimiga sobre as tropas aliadas (geralmente tais operações envolvem esta habilidade ou Liderança, ou Náutica).

Quando dois operadores de cerco competem - um defendendo uma posição e o outro a cercando, por exemplo - o mestre deverá exigir um teste resistido. Um empate indicará a manutenção da situação, com favorecimento à defesa sempre que for necessário definir. O vencedor terá alguma vantagem definida pelo mestre.''',
      tarefasAperfeicoadas: [
        'operar máquinas de guerra',
        'coordenar máquinas de guerra',
        'desestabilizar moral',
        'atravessar cerco',
        'atravessar defesas',
        'interceptar suprimentos',
        'planejar cerco',
        'sobreviver cercado',
      ],
    ),
    Habilidade(
      id: EnumHabilidades.persuasao.index,
      tipoHabilidade: EnumCodigoHabilidade.influencia,
      nome: "Persuasão",
      custo: 2,
      idAtributoBase: 2,
      descricao:
          '''Esta é a habilidade de falar e argumentar convincentemente através da eloquência ou da intimidação. Ela é usada principalmente com a finalidade de implantar um ponto de vista em outras pessoas, mas também pode ser utilizada para interpretação dramática, como a interpretação convincente de um personagem, na declamação de poesias e para narrar histórias de forma atraente.

A dificuldade no teste de habilidade é um critério do mestre e dependerá do argumento criado pelo jogador que interpreta o personagem persuasor. No entanto, algumas condições da persuasão irão impor modificadores de dificuldade na rolagem. As que mais frequentemente impõem penalidade são: raça do persuadido diferente do persuasor; ambiente hostil ou com preconceito contra o persuasor; persuasor com dicção limitada. Impõem bônus com mais frequência as condições: persuasor é um morador local; persuasor tem boa fama para com o persuadido, ou influência sobre ele; persuadido tem sentimentos positivos pelo persuasor; persuadido em condições desfavoráveis;

O mestre ainda deve considerar o uso de magias, pois várias tem o potencial de tornar um ambiente hostil em amistoso e vice-versa, alterando a dificuldade do teste;

Os ramos de conhecimento podem ser:
      ''',
      tarefasAperfeicoadas: [
        'arremedo',
        'atuação séria',
        'atuação cômica',
        'convencer',
        'ameaçar',
        'humilhar',
        'prolongar discussões',
        'provocar',
      ],
    ),
    Habilidade(
      id: EnumHabilidades.religiao.index,
      tipoHabilidade: EnumCodigoHabilidade.conhecimento,
      nome: "Religião",
      custo: 1,
      idAtributoBase: 5,
      descricao:
          '''Consiste no conhecimento dos dogmas e tradições das religiões conhecidas bem como de aspectos referentes aos deuses, enviados e demônios. Um personagem com a habilidade religião consegue reconhecer símbolos sagrados e profanos, identificar criaturas oriundas dos planos infernal e dos deuses, bem como relacionar itens mágicos de origem divina ou profana aos seus criadores;

Esta habilidade possui um ramo de conhecimento que deve ser escolhido no momento em que se adquire o primeiro nível em Religião. Os personagem realizará qualquer teste fora de seu ramo de conhecimento com um grau de dificuldade a mais. No entanto, é comum entre todos os ramos de religião o conhecimento sobre criaturas como enviados, demônios e as outras criaturas dos planos divinos e infernais. Um personagem com Religião saberá dizer se uma criatura pode ser esconjurada ou invocada (mesmo que ele mesmo não possa realizar), se é normalmente amistosa ou hostil, bem como conhecer superficialmente suas capacidades.

Os ramos de conhecimento podem ser:

Deuses: Os dogmas e tradições ditados pelos deuses do panteão de Tagmar. Estão incluídos conhecimentos de símbolos e personalidades importantes da história das congregações e ordens sagradas, as simpatias, rituais e as lendas que as cercam;

Demônios: Os dogmas e tradições ditados pelo demônios e seguidos pelos demonistas. Estão incluídos o conhecimentos de símbolos e personalidades importantes da seita, seus ídolos e rituais. Este conhecimento é raro e visto com maus olhos por toda a sociedade. Além de ser estudado em Runa e Saravossa, Religião (Demônios) é estudado dentro das ordens, algumas escolas de magia e em alguns poucos e secretos círculos;

Itens Sagrados: Conhecimento da origem de itens mágicos. Um personagem com este conhecimento será capaz de reconhecer um item mágico de origem divina ou profana após analisar suas marcas e símbolos, desde que haja alguma. Além disso, ele será capaz de deduzir uma ou todas as capacidades de tal item, a critério do mestre.

O ramo de conhecimento de um sacerdote deve ser Religião (Deuses). No entanto, eles são aperfeiçoados em Religião como um todo e realizam qualquer teste nesta habilidade com um grau de dificuldade a menos. Um sacerdote que pertence a uma ordem sacerdotal realiza qualquer teste relacionado a sua própria ordem com dois graus de dificuldade a menos, em vez de um.

Um teste de religião pode ser invocado pelo jogador apenas uma vez para cada milagre, vestígio, item ou criatura encontrada. Além disso, uma falha crítica no teste dá informações erradas que vão passar a fazer parte do conhecimento do personagem, o que poderá gerar um constrangimento ou até mesmo ferimentos no futuro.''',
      areas: [
        AreaHabilidade(
          id: 7,
          nome: 'Deuses',
          descricao:
              'Os dogmas e tradições ditados pelos deuses do panteão de Tagmar. Estão incluídos conhecimentos de símbolos e personalidades importantes da história das congregações e ordens sagradas, as simpatias, rituais e as lendas que as cercam',
        ),
        AreaHabilidade(
          id: 8,
          nome: 'Demônios',
          descricao:
              'Os dogmas e tradições ditados pelo demônios e seguidos pelos demonistas. Estão incluídos o conhecimentos de símbolos e personalidades importantes da seita, seus ídolos e rituais. Este conhecimento é raro e visto com maus olhos por toda a sociedade. Além de ser estudado em Runa e Saravossa, Religião (Demônios) é estudado dentro das ordens, algumas escolas de magia e em alguns poucos e secretos círculos',
        ),
        AreaHabilidade(
          id: 9,
          nome: 'Itens Sagrados',
          descricao:
              'Conhecimento da origem de itens mágicos. Um personagem com este conhecimento será capaz de reconhecer um item mágico de origem divina ou profana após analisar suas marcas e símbolos, desde que haja alguma. Além disso, ele será capaz de deduzir uma ou todas as capacidades de tal item, a critério do mestre',
        ),
      ],
      tarefasAperfeicoadas: [
        'cerimônias e rituais sagrados',
        'cerimônias e rituais profanos',
        'reconhecer itens sagrados',
        'reconhecer itens profanos',
        'símbolos',
        'doutrinas',
        'ordens sagradas',
        'cultos profanos',
      ],
    ),
    Habilidade(
      id: EnumHabilidades.sabedoria.index,
      tipoHabilidade: EnumCodigoHabilidade.conhecimento,
      nome: "Sabedoria",
      custo: 2,
      idAtributoBase: 2,
      descricao:
          '''Este é a habilidades dos anciões e dos conselheiros. A sabedoria envolve o conhecimento de história, lendas, natureza, geografia, monstros que supostamente habitam uma região, pessoas e símbolos comuns. Os sábios conhecem o passado e o ambiente ao seu redor, sendo capazes de formar o melhor juízo das situações que se apresentam.

A sabedoria é um conhecimento que se adquire com leitura, observação ou experiência e tem uma ênfase muito maior no entorno com o qual o sábio passou mais tempo do que nos locais distantes ou de pequena importância. Contudo, também é importante ressaltar que apenas aqueles que são capazes de compreender a própria natureza são capazes de compreender a natureza ao seu redor.

Como é uma ampla habilidade, a sabedoria deve ser adquirida em áreas de atuação, de acordo com as culturas a qual o personagem pode ter contato, pois o sábio de um lugar pode ser o tolo de outro.

Embora as áreas de conhecimento sejam apresentadas como um limite político para fins de simplificação, o mestre deve ter o cuidado de lembrar-se que a sabedoria tem limites culturais. Isso quer dizer que se um sábio navega pelo Rio Sevides no interior de Eredra, é a sabedoria das Águas que deve ser empregada para determinar o tipo de criaturas do rio que eles poderão se deparar.

Para determinar a dificuldade de um teste, o mestre deve considerar a área de atuação da habilidade e então estabelecer um resultado alvo com base na familiaridade do assunto ao personagem. Por exemplo, será mais difícil para um elfo de Âmien saber algo de Lar, mesmo que possua a sabedoria élfica.

São áreas de atuação possíveis:
      ''',
      areas: const <AreaHabilidade>[
        AreaHabilidade(
          id: 10,
          nome: 'Molda',
          descricao:
              'todo o noroeste dos reinos, que incluem os reinos de Calco, Conti, Plana, Azanti, Filanti e Acordo',
        ),
        AreaHabilidade(
          id: 11,
          nome: 'Runa',
          descricao:
              'todo o nordeste dos reinos, que incluem os reinos de Portis, As cidades Estado, Luna e Marana',
        ),
        AreaHabilidade(
          id: 12,
          nome: 'Águas',
          descricao:
              'A sabedoria dos marinheiros e dos homens do mar. Ela inclui a sabedoria sobre os povos de Porto Livre, dos navegantes do Lago Denégrio, dos códigos dos piratas e lendas sobre tesouros e ilhas nunca cartografadas incluindo, possivelmente, as Ilhas Independentes',
        ),
        AreaHabilidade(
          id: 13,
          nome: 'Verrogari',
          descricao:
              'todo o sudeste dos reinos, que incluem os reinos de Verrogar, Dantsem, Eredra e alguns conhecimentos sobre os bárbaros do sul da muralha, suas terras e lendas',
        ),
        AreaHabilidade(
          id: 14,
          nome: 'Leva',
          descricao:
              'todo o sudoeste dos reinos, que incluem os reinos da Levânia, Abadom e Ludgrim, bem como lendas e referências sobre a origem dos bankdis e demonistas, seus símbolos, heróis e lendas',
        ),
        AreaHabilidade(
          id: 15,
          nome: 'Élfica',
          descricao:
              'conhecimento preciso de natureza e sobre todas as terras e culturas élficas, incluindo Âmien e Lar, bem como sobre as comunidades élficas espalhadas pelo continente e também sobre histórias e lendas sobre Dartel. Os elfos sombrios não são abrangidos pela sabedoria élfica',
        ),
        AreaHabilidade(
          id: 16,
          nome: 'Anã',
          descricao:
              'conhecimento sobre o mundo debaixo de Tagmar. A sabedoria anã envolve o conhecimento de flora e fauna subterrânea, dos clãs dos anões, suas lendas, histórias e inimigos mais tradicionais',
        ),
      ],
      tarefasAperfeicoadas: [
        'histórias',
        'geografia',
        'heróis',
        'clãs',
        'ameaças locais',
        'lendas',
        'comportamento social',
        'símbolos',
      ],
    ),
    Habilidade(
      id: EnumHabilidades.seguirTrilhas.index,
      tipoHabilidade: EnumCodigoHabilidade.geral,
      nome: "Seguir Trilhas",
      custo: 1,
      idAtributoBase: 6,
      penalidades: [Penalidades.elmo],
      descricao:
          '''Esta é uma habilidade técnica e serve para encontrar e seguir trilhas feitas por qualquer tipo de criatura, bem como para ocultar as suas próprias. A critério do mestre, dependendo do ambiente, ela pode fornecer outros tipos de informação sobre quem, o que e como foi criada a pista, se pertencem a humanoides ou quadrúpedes, quantidade aproximada, tamanho estimado dos indivíduos, velocidade com que se deslocam etc.

O mestre deve estar atento para os vários usos desta habilidade. Ela não permite só o rastreamento de trilhas visuais, mas sim as vinculadas a todos os sentidos, especialmente auditivas e olfativas. Portanto, um personagem pode seguir trilhas para rastrear um odor. Contudo, ele estará limitado por suas próprias capacidades físicas, a critério do mestre. Por exemplo, um humano não tem um olfato apurado o suficiente para seguir uma trilha de odor que se inicia há um quilômetro, mas um cão talvez seja. Logo, o mestre deverá ajustar a dificuldade ou até torná-la impossível de acordo com a situação.

Personagens empregando esta habilidade se deslocam na metade da velocidade normal. Um teste deve ser realizado a cada dia de rastreamento ou a cada vez que o ambiente sofrer mudanças drásticas. São exemplos destas mudanças o começo de uma chuva torrencial, a mudança de terreno para um local mais difícil de detectar os rastros (areia - pedra, estrada - cidade etc) ou o cruzamento com um rio ou outro local que mascare as pistas.

Quando um Personagem tenta seguir a trilha de outro que tenta ocultá-la, o mestre deve ordenar um teste resistido e aplicar os modificadores temporais, além dos que julgar necessário devido ao clima e ao terreno. Um empate significa a manutenção da situação anterior: se o perseguidor estava na trilha, ele continua; se houve alguma mudança no terreno, ele perde. Detalhes ficam a critério do mestre.

A base do teste de seguir trilhas leva em consideração o tempo que a trilha foi aberta e não considera efeitos meteorológicos, terreno, e o número de seres que abriram a trilha. Para incluir estas variáveis, considera-se:

Clima: Instável, com chuvas ou ventos moderados, impõem um grau de dificuldade a mais na rolagem ou concedem 5 colunas adicionais no teste resistido de quem oculta a trilha; extremos, como fortes tempestades ou calor escaldante, impõem dois graus de dificuldade a mais na rolagem ou concedem 10 colunas adicionais ao testes resistido de quem oculta a trilha.

Terreno: Terreno fofo e facilmente marcado concede um grau de bônus na rolagem, ou concede 5 colunas adicionais no teste resistido de quem persegue; areia, solo ou grama é o padrão e não concedem modificadores; terreno pedregoso impõe um grau a mais de dificuldade ou concedem 5 colunas adicionais no teste resistido de quem oculta a trilha; Terreno coberto pela água impõe dois graus de dificuldade a mais na rolagem ou concede 10 colunas adicionais no teste resistido de quem oculta a trilha;

Número de perseguidos: Até 5 humanos ou equivalentes não concedem benefícios; até 30 humanos ou equivalentes concedem um grau de bônus à rolagem para rastrear ou 5 colunas adicionais no teste resistido para quem persegue; até 100 humanos ou equivalentes concedem dois graus de bônus à rolagem para rastrear ou 10 colunas adicionais no teste resistido de quem persegue; mais de 100 humanos ou equivalentes concedem três graus de bônus à rolagem para rastrear ou 15 colunas adicionais no teste resistido de quem persegue;

O termo “humanos ou equivalentes” significam humanoides de tamanho humano. Para criaturas maiores, como cavalos ou gigantes, o mestre pode considerar modificadores diferentes. Os modificadores acima deve ser aplicados sobre as seguintes orientações quanto ao tempo que a trilha foi aberta:
      ''',
      tarefasAperfeicoadas: [
        'ocultar trilha',
        'rastrear odor',
        'perseguir montarias',
        'perseguir humanoides',
        'encontrar rastros',
        'conhecimento em terrenos e climas ruins',
        'perseguir na cidade',
      ],
    ),
    Habilidade(
      id: EnumHabilidades.sensitividade.index,
      tipoHabilidade: EnumCodigoHabilidade.geral,
      nome: "Sensitividade",
      custo: 2,
      idAtributoBase: 1,
      penalidades: [Penalidades.armadura],
      descricao:
          '''As magias, os espíritos e as conexões com os vários planos de existência paralelos à Tagmar deixam uma marca no mundo que todos são capazes de experimentar em maior ou menor grau através de sinais como sensações estranhas, emoções aflorantes ou intuições repentinas. A habilidade para interpretar tais sinais de forma prática e rápida chama-se Sensitividade e ela relaciona-se intimamente com a força e flexibilidade da aura de cada indivíduo.

A Sensitividade é frequentemente interpretada como um dom, mas ela pode ser aperfeiçoada ao máximo através do estudo e da meditação. Xamãs, sacerdotes e especialmente magos são os que mais frequentemente desenvolvem esta habilidade, pois ela está muito relacionada com suas atividades cotidianas.

É importante destacar que as respostas obtidas por um teste de Sensitividade são sempre imprecisos ou vagos, pois suas respostas vem sempre na forma de sensações tais como calafrios, receios, transpiração forte, alívio, tendências a mudanças de humor, esperança, descrédito, vazio, alegria, tristeza, enfim, uma série de respostas físicas ou emocionais que o personagem sente devido à influência que o ambiente exerce sobre sua aura. Ocasionalmente, mas não sempre, a habilidade Misticismo pode ser utilizada para detalhar as impressões obtidas com Sensitividade. Outro ponto importante é que a Habilidade nunca pode ser utilizada para resistir ou quebrar nenhuma forma de encanto, pois ela representa apenas a capacidade de percebê-los e, em menor grau, de interpretá-los.

O mestre pode exigir um teste de Sensitividade para o personagem, por exemplo, pressentir uma aura impregnada em um ambiente; pressentir que uma magia está sendo realizada ou que um encantamento está ativo nas proximidades; pressentir a presença de objetos ou criaturas próximas incorpóreas ou sob efeito de magia; ou ainda para detectar a existência de portais mágicos intra ou interdimensionais. Como sempre, a Habilidade não aponta com precisão para o que é mágico, mas sim fornece uma intuição de que existe magia no mesmo ambiente em que ele se encontra. Dependendo do poder da magia ou da intensidade da aura local, o personagem pode nem saber se a sua intuição sensitiva refere-se a uma criatura, item mágico ou a um portal, por exemplo.
      ''',
      tarefasAperfeicoadas: [
        'pressentir karma arcano não infernal',
        'pressentir karma divino',
        'pressentir karma infernal',
        'sentir auras alteradas',
        'sentir auras em objetos',
        'pressentir o perigo',
        'pressentir maldade',
      ],
    ),
    Habilidade(
      id: EnumHabilidades.sobrevivencia.index,
      tipoHabilidade: EnumCodigoHabilidade.geral,
      nome: "Sobrevivência",
      custo: 1,
      idAtributoBase: 6,
      descricao:
          '''Sobrevivência é uma Habilidade que reúne conhecimentos teóricos e de campo, que permitem que seu conhecedor e alguns protegidos sobrevivam em todos os tipos de ambientes ermos e condições inóspitas. Ela permite que o personagem encontre comida, água, abrigo e permite também que ele evite fenômenos naturais perigosos nos mais diversos tipos de terrenos. Ela também permite reconhecer as maiores ameaças dos terrenos em questão, como plantas venenosas, animais hostis e monstros perigosos. Finalmente, esta confere ao personagem a capacidade de preparar alimentos e bebidas.

Esta habilidade dá ao personagem a capacidade de reconhecer quando um monstro ou animal é uma ameaça ou não, mas ela não informa detalhes sobre a criatura. Por exemplo, um rastreador de pântanos saberá que um animal está sentindo-se ameaçado e é perigoso, mas não saberá o que é ou o que aquela criatura pode fazer. Além disso, como é baseado em experiência e impressões, ela só fornece tais informações quando o personagem encontra a criatura, não antes.

Esta habilidade possui vários ramos de conhecimento devido a grande variedade que se encontra em diferentes lugares. Estes ramos se dividem em: floresta/selva, cerrado/savana, praia/montanha, geleira/tunda, mangue/pântano e deserto. Todos os testes realizados para um terreno diferente do ramo de conhecimento do personagem recebem um nível de penalidade no resultado.

Testes de sobrevivência podem ser empregados para procurar e colher ervas dos mais variados tipos. Contudo, a habilidade implica apenas no conhecimento das ervas in natura e de suas características relativas à sobrevivência, como uso com finalidade condimentar e nutricional. Por exemplo, ela dá ao personagem o conhecimento necessário para não se alimentar de uma planta venenosa, mas ele não é capaz de utilizá-la como veneno (exceto, é claro, na forma in natura, que é pouco útil e muito fácil de ser percebida).
      ''',
      tarefasAperfeicoadas: [
        'encontrar abrigo',
        'encontrar ervas comestíveis',
        'caçar',
        'pescar',
        'preparar bebidas',
        'preparar alimentos',
        'reconhecer o perigo',
        'alimentar dezenas de pessoas',
      ],
    ),
    Habilidade(
      id: EnumHabilidades.trabalhoEmMetal.index,
      tipoHabilidade: EnumCodigoHabilidade.profissional,
      nome: "Trabalho em Metal",
      isRestrita: true,
      custo: 2,
      idAtributoBase: 3,
      descricao:
          '''Esta Habilidade permite que um personagem faça ou repare objetos de metal. Note que apenas possuir a Habilidade não permite que o jogador faça os objetos que deseja, pois ele também precisa ter acesso aos instrumentos necessários. Essa Habilidade fornece apenas o conhecimento de como fazer, e não o material e os instrumentos necessários.

Os graus de dificuldade variam de acordo com a dificuldade do trabalho proposto e da disponibilidade de instalações e ferramentas adequadas (por exemplo: um ferreiro sem um torno só poderá fabricar setas e lâminas cegas).

Para se fazer um objeto, procede-se da seguinte maneira: o Mestre determina o tempo necessário e o jogador diz qual o seu nível na Habilidade. O Mestre rola secretamente e verifica qual o resultado. Se o jogador obtiver sucesso, o objeto está pronto. Se ele fracassar normalmente, o item ficou pronto, mas não pode ser usado devido a um defeito de fabricação. Se o resultado for uma Falha Crítica, o item está pronto e pode ser utilizado, mas vai se quebrar na primeira vez que for submetido à grande tensão. Por exemplo: uma espada se quebra na primeira vez que quem a estiver usando realizar um ataque 100%. Para se detectar um item nesse estado é necessário um teste de Dificuldade Muito Difícil por alguém que não seja seu criador. O custo que deve ser empenhado em cada tentativa de fabricação é igual a metade do custo de compra do objeto.

Para se reparar um objeto realiza-se um teste e, se bem-sucedido , o objeto está consertado. Com um fracasso comum o objeto continua danificado, podendo o jogador tentar de novo ao custo de 1/4 do custo de compra. Se obtiver Verde (Falha crítica), o objeto está destruído e sem conserto.

É possível empregar esta habilidade para melhorar armas metálicas. A tentativa é realizada como se fosse um reparo. Contudo, todos os testes são considerados difíceis ou, no caso de armas ou armaduras exóticas para o personagem, muito difíceis. Se o personagem obtiver uma falha crítica o objeto está danificado e precisará ser consertado conforme acima (armaduras tem absorção levada a 0). Uma falha comum não traz maiores prejuízos. Um sucesso indica que a arma foi colocada em ótimas condições e concederá um bônus de +1 em qualquer um dos modificadores LMP, a critério do artesão, mas apenas até sofrer alguma avaria, a critério do mestre. Por exemplo, o mestre pode julgar que uma espada sofre avaria e perde o bônus após um combate contra um golem de argila. Uma armadura receberá um bônus de +1 em sua defesa até passar por qualquer manutenção. Testes de reparo e refino devem ser feitos separadamente.

Como mostrado na tabela abaixo, um resultado absurdo no teste indica a construção de uma obra prima. Na prática, isso significa que o próprio material utilizado acabou se mostrando muito melhor do que o normal e a arma ou armadura é excepcional. Com isso, uma arma tem 2 pontos a mais distribuídos em seus modificadores LMP, a critério do artesão, mas que não podem ser aplicados em apenas um tipo de modificador. Uma armadura excepcional tem +1 ponto de defesa permanentemente. Uma arma ou armadura desta ainda pode ser refinada, mas uma falha crítica na tentativa lhe retira a qualidade excepcional e seus benefícios.

OBS: para cunhar moedas sem os moldes originais, é necessário usar em conjunto a Habilidade Falsificação.
      ''',
      tarefasAperfeicoadas: [
        'trabalhar com ferramentas improvisadas',
        'consertar armas',
        'consertar armaduras',
        'construir armas',
        'construir armaduras',
        'melhorar armas e armaduras',
        'cunhar moedas',
        'ferreiro',
      ],
    ),
    Habilidade(
      id: EnumHabilidades.trabalhosManuais.index,
      tipoHabilidade: EnumCodigoHabilidade.profissional,
      nome: "Trabalhos Manuais",
      custo: 2,
      idAtributoBase: 0,
      descricao:
          '''Esta Habilidade serve para fazer, reparar e melhorar objetos que não são de metal ou madeira (couro, corda, tecido etc.). Os graus de dificuldade variam de acordo com a dificuldade do trabalho proposto e da disponibilidade de instalações e ferramentas adequadas.

Para se fazer um objeto, procede-se da seguinte maneira: o Mestre determina o tempo necessário e o jogador diz qual o seu nível na Habilidade. O Mestre rola secretamente e verifica qual o resultado. Se o jogador obtiver sucesso, o objeto está pronto. Se ele fracassar normalmente, o item ficou pronto, mas não pode ser usado devido a um defeito de fabricação. Se o resultado for uma Falha Crítica, o item está pronto e pode ser utilizado, mas vai se quebrar na primeira vez que for submetido à grande tensão. Por exemplo: uma corda se parte na primeira vez que sustentar o peso de um Humano adulto. Para se detectar um item nesse estado é necessário um sucesso no teste de Dificuldade Muito Difícil realizado por outro personagem.

Para se reparar um objeto como uma armadura de couro, um resultado positivo significa sucesso na tentativa. Com um fracasso comum o objeto continua danificado, podendo o jogador tentar de novo, às custas de 1/4 do custo do material novo. Se obtiver Verde (Falha crítica), o objeto está destruído.

É possível empregar esta habilidade para melhorar objetos e armaduras. A tentativa é realizada como se fosse um reparo. Contudo, todos os testes são considerados difíceis ou, no caso de objetos exóticos ou muito delicados, muito difíceis. Se o personagem obtiver uma falha crítica o objeto está danificado e precisará ser consertado conforme acima (armaduras tem absorção levada a 0). Uma falha comum não traz maiores prejuízos. Um sucesso indica que uma armadura foi colocada em ótimas condições e concederá um bônus de +1 em sua defesa até passar por qualquer manutenção. Outros objetos podem conceder benefícios limitados para situações específica a critério do mestre. Por exemplo, uma sela de cavalaria pode conceder um bônus de +1 aos testes de montar animais utilizados em tentativas de se manter sobre a montaria durante 1 semana, quando os remendos já estarão gastos e não trarão novos benefícios. Testes de reparo e refino devem ser feitos separadamente.

Como mostrado na tabela abaixo, um resultado absurdo no teste indica a construção de uma obra prima. Na prática, isso significa que o próprio material utilizado acabou se mostrando muito melhor do que o normal e a armadura ou objeto é excepcional. Com isso, uma armadura excepcional tem +1 ponto de defesa permanentemente e um objeto pode conceder um bônus de +1 coluna de resolução em alguma situação específica permanentemente. Um objeto ou armadura excepcional ainda podem ser refinados, mas uma falha crítica na tentativa lhes retiram a qualidade excepcional e seus benefícios.
      ''',
      tarefasAperfeicoadas: [
        'alfaiate',
        'tecelão',
        'sapateiro/curtidor',
        'chapeleiro',
        'cabeleireiro',
        'trabalhar com ferramentas improvisadas',
        'construir armaduras',
        'reparar armaduras',
        'melhorar armaduras',
      ],
    ),
    Habilidade(
      id: EnumHabilidades.usarOsSentidos.index,
      tipoHabilidade: EnumCodigoHabilidade.geral,
      nome: "Usar os Sentidos",
      custo: 2,
      idAtributoBase: 6,
      descricao:
          '''Usar os Sentidos é a habilidade de empregar os sentidos a fim de obter sensações físicas de interesse do meio com bastante precisão. Um personagem deve estar em alerta para poder fazer uso de Usar os Sentidos. Por exemplo, um personagem em guarda, procurando ou não por algo, pode se aproveitar de Usar os Sentidos, mas um personagem dormindo não. A captação de sensações de forma totalmente passiva (como enquanto se dorme ou se está concentrado em uma tarefa exigente) é realizada com um teste de atributo de percepção, mas nunca é capaz de fornecer informações muito detalhadas e precisas. Usar os Sentidos habilidade envolve o conhecimento e a apuração da visão, audição, olfato, tato e paladar. A precisão de um sentido é diferente de pessoa para pessoa, ficando a cargo de mestre e do histórico do personagem o caso de algum talento extra ou inaptidão para determinado sentido. As regras da habilidade consideram uma pessoa mediana.

Usar os Sentidos é, normalmente, um talento natural. Contudo, ele pode ser treinado para melhorar a precisão de suas constatações. Personagens bem treinados em Usar os Sentidos são atentos e perspicazes para detectar alterações em seu meio.

As vezes o odor pode deixar uma trilha. Para humanos e outros seres com capacidade olfativa equivalente, antes de ser capaz de seguir essa trilha de odor será preciso ser bem-sucedido em um teste de Usar os Sentidos. Animais com o olfato mais apurado não precisam realizar este teste antes de utilizarem a habilidade de seguir trilhas.

Finalmente, um teste de Usar os Sentidos pode discernir odores em um alimento ou bebida. No entanto, esta habilidade não serve para identificar venenos. pois ela não implica em saber que tal odor trata-se de um veneno. A melhor conclusão que um personagem bem-sucedido em um teste de Usar os Sentidos poderá ter é de que há algo de diferente na bebida ou alimento. Consulte a habilidade venefício para mais detalhes sobre a detecção de venenos.
      ''',
      tarefasAperfeicoadas: [
        'perceber armadilhas; encontrar passagens secretas',
        'discriminar odores e sabores',
        'ouvir através do sólido',
        'manter guarda',
        'reconhecer ao tato',
        'reconhecer sons',
        'enxergar na penumbra',
      ],
    ),
    Habilidade(
      id: EnumHabilidades.veneficio.index,
      tipoHabilidade: EnumCodigoHabilidade.conhecimento,
      nome: "Venefício",
      custo: 1,
      idAtributoBase: 5,
      isTesteSemNivel: false,
      isRestrita: true,
      descricao:
          '''Quem possuir esta Habilidade terá um conhecimento sobre venenos e drogas, incluindo o seu preparo e utilização. O personagem poderá tentar reconhecer um veneno ou os seus sintomas, prepará-los e saber manipulá-los de forma correta. Esta habilidade também engloba o preparo dos antídotos.

Para preparar um veneno, um personagem deve primeiro especificar que tipo de veneno ou droga deseja. Existem inúmeros em Tagmar e todos eles podem ser enquadrados em cinco tipos (ver capítulo “Mecânica de Jogo” na parte de Regras deste manual). A dificuldade e o custo do veneno ou droga variam de acordo com o tipo, mas as demais características, como efeitos, força de ataque e tipo de aplicação mudam de um para outro.

Um teste bem-sucedido de Venefício permite ao personagem extrair ou produzir veneno ou droga adequadamente de uma planta, animal ou sintetizá-lo quando dispuser dos equipamentos necessários. Um fracasso consome o material, mas o produto é inútil. Em uma falha catastrófica o personagem acaba por se envenenar com o veneno ou droga que tentava produzir.

Venenos feitos para serem usados em armas sempre são bastante visíveis. Caso alguém examine uma arma à procura de veneno, ele sempre será encontrado. Em situações onde não se puder examinar a arma (em combate, por exemplo), uma examinação média se faz necessária.

Venenos de ingestão normalmente têm sabor facilmente identificável e por isso são misturados em bebidas de sabor forte (vinho tinto ou aguardente, por exemplo) ou em comidas condimentadas. Quem comer ou beber o alimento envenenado terá direito a fazer um teste do atributo Percepção para sentir que há alguma coisa errada com a comida ou bebida. O teste é Fácil para alimentos sem sabor (como água), Médio para alimentos normais (como carnes e vinhos) e Difícil para alimentos com gosto forte (como aguardente e comidas bem condimentadas). Um personagem que procure por alterações no alimento ou bebida envenenada segue as regras da habilidade Venefício. Contudo, identificar que há algo de estranho com o gosto de um alimento ou bebida não significa perceber que ela está envenenada. A critério do mestre um personagem que não tenha razão para desconfiar do envenenamento não tem direito a teste.

Outro uso da Habilidade é saber aplicar corretamente o veneno. Saber misturá-lo à comida de forma a deixar o sabor o menos perceptível possível, ou a uma arma de forma a que ele não se desprenda da mesma. Ambos os casos, requerem um teste Fácil de Venefício. Em caso de sucesso o veneno foi corretamente usado. Uma falha faz com que o veneno seja percebido ou se desprenda da arma após duas rodadas de combate. O veneno ainda tem seu efeito normal se ingerido ou injetado. Uma Falha Crítica quer dizer que não só o veneno é imediatamente percebido (uma camada amarelada em cima do vinho) ou que sai imediatamente da arma, tornando-o inútil.

As drogas são uma variação dos venenos que podem ser produzidas com esta habilidade. Diferente dos venenos, uma droga sempre concede um benefício inicialmente. No entanto, todas elas são viciantes e provocam crises de abstinência se o personagem não voltar a utilizá-la no final de seu ciclo. O número de vezes que um personagem pode fazer uso de uma droga sem se viciar está em sua descrição. Elas se enquadram nos mesmos cinco tipos de venenos, aumentando suas dependências e efeitos colaterais da abstinência com o aumento do tipo, mas também os benefícios iniciais.

Enfim, a última utilidade desta Habilidade é a capacidade de preparar antídotos, se tiver identificado o veneno ou droga e possuir os ingredientes necessários. Caso o personagem obtenha sucesso no preparo do antídoto a neutralização do veneno é bem sucedida. Caso seja necessário algum método especial de aplicação (como a vaporização), será preciso um sucesso fácil em Medicina para a neutralização. A Dificuldade para preparar o antídoto em geral é a mesma daquela para preparar o veneno. A cura da dependência é mais demorada e deve seguir as regras apresentadas em Mecânica de Jogo.

Venefício é sempre visto com muito preconceito nas sociedades de Tagmar. Ser reconhecido como um envenenador é uma sentença de morte em quase todo o mundo conhecido e, por isso, ninguém que conheça esta Habilidade irá confessar isso voluntariamente. Para complicar ainda mais as coisas, preparar venenos utilizáveis é muito complicado, o que não permite que um personagem aprenda essa Habilidade (compre nível 1 nela) sem ter quem o ensine. Estes dois fatos juntos fazem com que aprender a usar venenos seja bastante difícil. Convencer o envenenador a ensinar o seu ofício é ainda mais difícil e sempre irá envolver o pagamento de somas absurdas (pelo menos 15 ou 20 moedas de ouro).
      ''',
      tarefasAperfeicoadas: [
        'aplicar veneno em arma',
        'fabricar veneno',
        'preparar antídotos',
        'fabricar droga',
        'disfarçar veneno em bebida',
        'disfarçar veneno em alimento',
        'fabricar drogas',
        'coletar ingredientes',
      ],
    ),
  ];
}
