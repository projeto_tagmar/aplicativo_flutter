import 'dart:math';

import 'package:flutter/cupertino.dart';

import 'package:tagmar/models/ficha/ficha.dart';

class Fichas with ChangeNotifier {
  List<Ficha> fichas = [];

  List<Ficha> get all {
    return [...fichas];
  }

  Ficha findById(String id) {
    return fichas.firstWhere((ficha) => ficha.id == id);
  }

  void removeById(String id) {
    fichas.removeWhere((ficha) => ficha.id == id);
    notifyListeners();
  }

  bool get isEmpty {
    return fichas.isEmpty;
  }

  void mockOne() {
    fichas.add(
      Ficha(
        id: DateTime.now().toString(),
        nome: 'nome ${fichas.length}',
        criacao: DateTime.now(),
        narrador: "fulaninho",
        nivel: Random().nextInt(30),
        experiencia: Random().nextInt(200),
      ),
    );
    notifyListeners();
  }

  void mockReset() {
    fichas.clear();
    notifyListeners();
  }
}
