import 'package:flutter/cupertino.dart';
import 'package:tagmar/enums/enum_atributo_secundario.dart';
import 'package:tagmar/models/atributo/atributo.dart';

class AtributosSecundarios with ChangeNotifier {
  static List<Atributo> atributos = [
    Atributo(
      id: EnumAtributoSecundario.resistenciaFisica.index,
      nome: "Resistência Física",
      descricao:
          'O perigo para os personagens vem de muitas formas: combates mortais, armadilhas perigosas, venenos, doenças etc. Quando o personagem sofre a ação dos venenos ou doenças, ele pode tentar a Resistência Física. A Resistência Física é a medida da capacidade de resistir a este tipo de ataque.',
    ),
    Atributo(
      id: EnumAtributoSecundario.resistenciaMagia.index,
      nome: "Resistência à Magia",
      descricao:
          'Quando algo com Aura (objeto ou ser) é atacado por magia pura (ataque direto), ele pode tentar resistir aos efeitos deste ataque. Mesmo os efeitos benéficos podem ser resistidos, já que esta resistência é um ato voluntário. Isto significa também que, caso um indivíduo o deseje, ele pode falhar automaticamente na resistência.',
    ),
    Atributo(
      id: EnumAtributoSecundario.velocidadeBase.index,
      nome: "Velocidade Base",
      descricao:
          'A velocidade base representa a distância (em metros) que o personagem pode andar em uma rodada. Essa velocidade pode variar.',
    ),
    Atributo(
      id: EnumAtributoSecundario.karma.index,
      nome: "Karma",
      descricao:
          '''As classes místicas, cada uma com o seu método, conseguem entrar em sintonia com o mana (a substância da magia). Com isso, cada místico consegue o seu poder, sua força mágica, que é chamada "Karma". A quantidade de Karma que um personagem consegue acumular depende de dois fatores: o seu Estágio e a sua Aura.

Indivíduos que possuem o atributo Aura mais desenvolvido têm maior potencial de controle sobre o mana, ganhando mais Karma que pessoas de mesmo Estágio que tenham uma Aura mais fraca. Tanto ou mais importante do que a sua Aura é o seu Estágio, pois este indica a sua experiência com o controle de forças sobrenaturais.
      ''',
    ),
  ];
}
