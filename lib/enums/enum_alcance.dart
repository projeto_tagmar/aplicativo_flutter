import 'package:tagmar/util/abstract_enum_help.dart';

enum EnumAlcance {
  variavel,
  pessoal,
  toque,
  metros,
  quilometros,
}

extension _AlcanceExtension on EnumAlcance {
  String get selectedName {
    final names = [
      "Variável",
      "Pessoal",
      "Toque",
      "metro",
      "quilômetro",
    ];
    return names[index];
  }

  String get name {
    return "Alcance";
  }
}

class Alcance extends AbstractEnumHelp<EnumAlcance> {
  final bool isRaio;
  final bool isQuadrado;
  final String? outraDescricao;

  const Alcance({
    int? valor,
    this.isRaio = false,
    this.outraDescricao,
    this.isQuadrado = false,
    required EnumAlcance tipo,
  }) : super(valor: valor, tipo: tipo);

  @override
  String get name {
    return tipo.name;
  }

  @override
  String get selectedName {
    return tipo.selectedName;
  }

  @override
  String toString() {
    final plural = valor != null && valor! > 1 ? "s" : "";
    if (outraDescricao != null) {
      return outraDescricao!;
    } else if (isQuadrado) {
      return "${super.toString()} quadrado$plural";
    } else if (isRaio) {
      return "${super.toString()} de raio";
    } else {
      return super.toString();
    }
  }
}
