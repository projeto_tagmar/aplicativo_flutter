import 'package:tagmar/util/abstract_enum_help.dart';

enum EnumEvocacao {
  variavel,
  instantanea,
  rodadas,
  minutos,
  horas,
  ritual,
}

extension _EvocacaoExtension on EnumEvocacao {
  String get selectedName {
    final names = [
      "Variável",
      "Instantânea",
      "rodada",
      "minuto",
      "hora",
      "Ritual",
    ];
    return names[index];
  }

  String get name {
    return "Evocação";
  }
}

class Evocacao extends AbstractEnumHelp<EnumEvocacao> {
  final String? outraDescricao;

  const Evocacao({
    int? valor,
    this.outraDescricao,
    required EnumEvocacao tipo,
  }) : super(valor: valor, tipo: tipo);

  @override
  String get name {
    return tipo.name;
  }

  @override
  String get selectedName {
    if (outraDescricao != null) {
      return outraDescricao!;
    } else {
      return tipo.selectedName;
    }
  }
}
