enum EnumCodigoHabilidade {
  profissional,
  subterfugio,
  manobra,
  influencia,
  conhecimento,
  geral,
}

EnumCodigoHabilidade habilidadeFromInt(int val) {
  return EnumCodigoHabilidade.values[val];
}
