import 'package:tagmar/util/abstract_enum_help.dart';

enum EnumQuadroDeRolagem {
  verde,
  branco,
  amarelo,
  laranja,
  vermelho,
  azul,
  roxo,
  cinza,
}

extension _QuadroDeRolagemExtension on EnumQuadroDeRolagem {
  String get selectedName {
    final names = [
      "Verde",
      "Branco",
      "Amarelo",
      "Laranja",
      "Vermelho",
      "Azul",
      "Roxo",
      "Cinza",
    ];
    return names[index];
  }

  String get selectedColor {
    final colors = [
      '#91cf4f',
      '#ffffff',
      '#ffff00',
      '#ff9700',
      '#ff0000',
      '#00a1e6',
      '#251963',
      '#979797'
    ];

    return colors[index];
  }

  String get name {
    return "Quadro de Rolagens";
  }
}

class QuadroDeRolagem extends AbstractEnumHelp<EnumQuadroDeRolagem> {
  final String descricao;
  const QuadroDeRolagem({
    int? valor,
    required this.descricao,
    required EnumQuadroDeRolagem tipo,
  }) : super(valor: valor, tipo: tipo);

  @override
  String get name {
    return tipo.name;
  }

  String get color {
    return tipo.selectedColor;
  }

  @override
  String get selectedName {
    return tipo.selectedName;
  }
}
