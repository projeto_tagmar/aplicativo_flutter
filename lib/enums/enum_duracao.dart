import 'package:tagmar/util/abstract_enum_help.dart';

enum EnumDuracao {
  uso,
  ativacao,
  variavel,
  instantanea,
  especial, // ver "visão de batalhas"
  ritual,
  segundo,
  rodadas,
  combate,
  cena,
  minutos,
  horas,
  dia,
  semana,
  mes,
  ano,
  anoEDia,
  permanente,
}

extension _DuracaoExtension on EnumDuracao {
  String get selectedName {
    final names = [
      "uso",
      "ativação",
      "Variável",
      "Instantânea",
      "Especial",
      "Ritual",
      "segundo",
      "rodada",
      "combate",
      "cena",
      "minuto",
      "hora",
      "dia",
      "semana",
      "mês",
      "ano",
      "1 ano e 1 dia",
      "Permanente"
    ];
    return names[index];
  }

  String get name {
    return "Duração";
  }
}

class Duracao extends AbstractEnumHelp<EnumDuracao> {
  final String? outraDescricao;

  const Duracao({
    int? valor,
    this.outraDescricao,
    required EnumDuracao tipo,
  }) : super(valor: valor, tipo: tipo);

  @override
  String get name {
    return tipo.name;
  }

  @override
  String get selectedName {
    return tipo.selectedName;
  }

  @override
  String toString() {
    if (outraDescricao != null) {
      return outraDescricao!;
    } else {
      return super.toString();
    }
  }
}
