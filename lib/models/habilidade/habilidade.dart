import 'package:tagmar/util/serializer.dart';
import 'package:tagmar/enums/enum_codigo_habilidade.dart';
import 'package:tagmar/models/xtra/abstract_named_item.dart';
import 'package:tagmar/models/habilidade/area_habilidade.dart';
import 'package:tagmar/models/habilidade/enum_penalidades.dart';

class Habilidade extends AbstractNamedItem {
  final int custo;
  final bool isRestrita;
  final int idAtributoBase;
  final bool isTesteSemNivel;
  final List<AreaHabilidade> areas;
  final List<Penalidades> penalidades;
  final List<String> tarefasAperfeicoadas;
  final EnumCodigoHabilidade tipoHabilidade;

  const Habilidade({
    required int id,
    required this.custo,
    required String nome,
    this.areas = const [],
    this.isRestrita = false,
    required String descricao,
    this.penalidades = const [],
    this.isTesteSemNivel = true,
    required this.tipoHabilidade,
    required this.idAtributoBase,
    required this.tarefasAperfeicoadas,
  }) : super(
          id: id,
          nome: nome,
          descricao: descricao,
        );
}

class HabilidadeSerializer extends Serializer<Habilidade> {
  @override
  Habilidade fromJson(Map<String, dynamic> json) {
    // TODO: implement fromJson
    throw UnimplementedError();
  }

  @override
  Map<String, dynamic> toJson(Habilidade object) {
    // TODO: implement toJson
    throw UnimplementedError();
  }
}
