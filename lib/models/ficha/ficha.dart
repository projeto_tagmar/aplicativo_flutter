import 'package:flutter/cupertino.dart';

class Ficha with ChangeNotifier {
  String id;
  String nome;
  DateTime criacao;
  String narrador;
  int nivel;
  int experiencia;
  Ficha({
    required this.id,
    required this.nome,
    required this.nivel,
    required this.criacao,
    required this.narrador,
    required this.experiencia,
  });
}
