import 'package:tagmar/enums/enum_alcance.dart';
import 'package:tagmar/enums/enum_duracao.dart';
import 'package:tagmar/enums/enum_evocacao.dart';
import 'package:tagmar/models/dinheiro/dinheiro.dart';
import 'package:tagmar/models/xtra/abstract_describeable_item.dart';

class Nivel extends AbstractDescribeableItem {
  final int nivel;
  final Dinheiro? custo;
  final Alcance? alcance;
  final Duracao? duracao;
  final Nivel? secundario;
  final Evocacao? evocacao;

  const Nivel({
    this.custo,
    this.alcance,
    this.duracao,
    this.evocacao,
    required int id,
    this.secundario,
    required this.nivel,
    required String descricao,
  }) : super(
          id: id,
          descricao: descricao,
        );
}
