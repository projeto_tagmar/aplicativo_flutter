import 'package:tagmar/models/magia/magia_custo.dart';
import 'package:tagmar/models/xtra/abstract_named_item.dart';

class GrupoMagico extends AbstractNamedItem {
  final List<MagiaCusto> idMagias;
  final int idProfissao;

  const GrupoMagico({
    required int id,
    required String nome,
    required this.idMagias,
    required this.idProfissao,
    required String descricao,
  }) : super(id: id, nome: nome, descricao: descricao);
}
