import 'package:tagmar/enums/enum_alcance.dart';
import 'package:tagmar/enums/enum_duracao.dart';
import 'package:tagmar/models/magia/nivel.dart';
import 'package:tagmar/enums/enum_evocacao.dart';
import 'package:tagmar/models/dinheiro/dinheiro.dart';
import 'package:tagmar/models/xtra/abstract_named_item.dart';

class Magia extends AbstractNamedItem {
  final List<String>? extra;
  final Alcance alcance;
  final Duracao duracao;
  final Dinheiro? custo;
  final Evocacao evocacao;
  final List<Nivel> niveis;

  const Magia({
    this.custo,
    this.extra,
    required int id,
    required String nome,
    required this.niveis,
    required this.alcance,
    required this.duracao,
    required this.evocacao,
    required String descricao,
  }) : super(
          id: id,
          nome: nome,
          descricao: descricao,
        );
}
