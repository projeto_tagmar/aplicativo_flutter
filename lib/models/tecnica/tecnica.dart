import 'package:tagmar/enums/enum_atributo.dart';
import 'package:tagmar/enums/enum_tecnicas_combate.dart';
import 'package:tagmar/enums/enum_quadro_de_rolagem.dart';
import 'package:tagmar/enums/enum_tecnica_categoria.dart';
import 'package:tagmar/models/xtra/abstract_indexable_item.dart';

class TecnicaDeCombate extends AbstractIndexableItem {
  final String nome;
  final String mecanica;
  final String aprimoramento;
  final EnumAtributo atributo;
  final QuadroDeRolagem? rolagem;
  final EnumTecnicaCategoria categoria;
  final List<EnumTecnicasDeCombate>? requisitos;

  const TecnicaDeCombate({
    this.rolagem,
    this.requisitos,
    required int id,
    required this.nome,
    required this.atributo,
    required this.mecanica,
    required this.categoria,
    required this.aprimoramento,
  }) : super(
          id: id,
        );
}
