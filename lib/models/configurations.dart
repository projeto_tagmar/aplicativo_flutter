import 'package:flutter/cupertino.dart';

import 'package:shared_preferences/shared_preferences.dart';

class Configuracoes with ChangeNotifier {
  bool _isDark = true;
  bool _isAnimate = true;
  bool _isWifiOnly = true;
  bool _isCheckUpdate = true;

  bool _loaded = false;

  static String dark = "dark";
  static String wifi = "wifi";
  static String animate = "animate";
  static String autoUpdate = "auto update";

  /// Carrega todas as configurações
  void loadAll() async {
    if (!_loaded) {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      _isDark = prefs.getBool(Configuracoes.dark) ?? true;
      _isWifiOnly = prefs.getBool(Configuracoes.wifi) ?? true;
      _isAnimate = prefs.getBool(Configuracoes.animate) ?? true;
      _isCheckUpdate = prefs.getBool(Configuracoes.autoUpdate) ?? true;
      _loaded = true;
      notifyListeners();
    }
  }

  /// Salva a configuração que foi modificada
  void updateLocaly(String field, bool value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setBool(field, value);
  }

  bool get isDark {
    return _isDark;
  }

  bool get isAnimate {
    return _isAnimate;
  }

  bool get isWifiOnly {
    return _isWifiOnly;
  }

  bool get isCheckUpdate {
    return _isCheckUpdate;
  }

  set isDark(bool dark) {
    _isDark = dark;
    updateLocaly(Configuracoes.dark, dark);
    notifyListeners();
  }

  set isAnimate(bool animate) {
    _isAnimate = animate;
    updateLocaly(Configuracoes.animate, animate);
    notifyListeners();
  }

  set isWifiOnly(bool wifiOnly) {
    _isWifiOnly = wifiOnly;
    updateLocaly(Configuracoes.wifi, wifiOnly);
    notifyListeners();
  }

  set isCheckUpdate(bool checkUpdate) {
    _isCheckUpdate = checkUpdate;
    updateLocaly(Configuracoes.autoUpdate, checkUpdate);
    notifyListeners();
  }
}
