abstract class AbstractIndexableItem {
  final int id;

  const AbstractIndexableItem({required this.id});
}
