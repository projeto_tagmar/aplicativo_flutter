import 'package:tagmar/models/xtra/abstract_describeable_item.dart';

abstract class AbstractNamedItem extends AbstractDescribeableItem {
  final String nome;

  const AbstractNamedItem({
    required int id,
    required this.nome,
    required String descricao,
  }) : super(
          id: id,
          descricao: descricao,
        );
}
