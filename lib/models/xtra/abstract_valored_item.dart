import 'package:tagmar/models/xtra/abstract_named_item.dart';

class AbstractValoredItem extends AbstractNamedItem {
  int nivel;

  AbstractValoredItem({
    this.nivel = 0,
    required int id,
    required String nome,
    required String descricao,
  }) : super(id: id, nome: nome, descricao: descricao);
}
