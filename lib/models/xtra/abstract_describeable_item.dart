import 'package:tagmar/models/xtra/abstract_indexable_item.dart';

abstract class AbstractDescribeableItem extends AbstractIndexableItem {
  final String descricao;

  const AbstractDescribeableItem({required int id, required this.descricao})
      : super(id: id);
}
