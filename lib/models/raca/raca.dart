import 'package:tagmar/util/serializer.dart';
import 'package:tagmar/models/xtra/abstract_named_item.dart';

class Raca extends AbstractNamedItem {
  final int aura;
  final int peso;
  final int forca;
  final int fisico;
  final int carisma;
  final int efBasica;
  final int percepcao;
  final int agilidade;
  final double altura;
  final int velocidade;
  final int inteligencia;

  const Raca({
    required int id,
    required String descricao,
    required String nome,
    this.aura = 0,
    this.peso = 0,
    this.forca = 0,
    this.fisico = 0,
    this.altura = 0,
    this.carisma = 0,
    this.efBasica = 0,
    this.percepcao = 0,
    this.agilidade = 0,
    this.velocidade = 0,
    this.inteligencia = 0,
  }) : super(
          id: id,
          nome: nome,
          descricao: descricao,
        );
}

class RacaSerializer extends Serializer<Raca> {
  static const String _auraString = 'aura';
  static const String _pesoString = 'peso';
  static const String _forcaString = 'forca';
  static const String _fisicoString = 'fisico';
  static const String _carismaString = 'carisma';
  static const String _agilidadeString = 'agilidade';
  static const String _percepcaoString = 'percepcao';
  static const String _inteligenciaString = 'inteligencia';

  static const String _idString = 'id';
  static const String _alturaString = 'altura';
  static const String _nomeRacaString = 'nome';
  static const String _efBasicaString = 'ef_basica';
  static const String _descricaoString = 'descricao';
  static const String _velocidadeString = 'velocidade';

  @override
  Raca fromJson(Map<String, dynamic> json) {
    return Raca(
      aura: json[_auraString]?.toInt(),
      forca: json[_forcaString]?.toInt(),
      fisico: json[_fisicoString]?.toInt(),
      carisma: json[_carismaString]?.toInt(),
      agilidade: json[_agilidadeString]?.toInt(),
      percepcao: json[_percepcaoString]?.toInt(),
      inteligencia: json[_inteligenciaString]?.toInt(),
      nome: json[_nomeRacaString],
      id: json[_idString]?.toInt(),
      peso: json[_pesoString]?.toInt(),
      descricao: json[_descricaoString],
      altura: json[_alturaString]?.toDouble(),
      efBasica: json[_efBasicaString]?.toInt(),
      velocidade: json[_velocidadeString]?.toInt(),
    );
  }

  @override
  Map<String, dynamic> toJson(Raca object) {
    final data = <String, dynamic>{};
    data[_auraString] = object.aura;
    data[_forcaString] = object.forca;
    data[_fisicoString] = object.fisico;
    data[_carismaString] = object.carisma;
    data[_agilidadeString] = object.agilidade;
    data[_inteligenciaString] = object.inteligencia;

    data[_idString] = object.id;
    data[_pesoString] = object.peso;
    data[_nomeRacaString] = object.nome;
    data[_alturaString] = object.altura;
    data[_efBasicaString] = object.efBasica;
    data[_descricaoString] = object.descricao;
    data[_percepcaoString] = object.percepcao;
    data[_velocidadeString] = object.velocidade;
    return data;
  }
}
