class DrawerItemModel {
  final String text;
  final String route;

  DrawerItemModel({
    required this.text,
    required this.route,
  });
}
