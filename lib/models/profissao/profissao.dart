import 'package:tagmar/util/serializer.dart';
import 'package:tagmar/enums/enum_codigo_habilidade.dart';
import 'package:tagmar/models/xtra/abstract_named_item.dart';

class Profissao extends AbstractNamedItem {
  final int ehBasica;
  final List<int> racasNaoPermitidas;
  final int habilidadeIdAperfeicoada;
  final int pontosAquisicaoHabilidade;
  final EnumCodigoHabilidade? cdTipoHabilidadePenalizada;

  const Profissao({
    required int id,
    required String nome,
    required this.ehBasica,
    required String descricao,
    this.cdTipoHabilidadePenalizada,
    required this.racasNaoPermitidas,
    required this.habilidadeIdAperfeicoada,
    required this.pontosAquisicaoHabilidade,
  }) : super(
          id: id,
          nome: nome,
          descricao: descricao,
        );
}

class ProfissaoSerializer extends Serializer<Profissao> {
  static const String _idString = "id";
  static const String _nomeString = "nome";
  static const String _ehBasicaString = "eh_basica";
  static const String _descricaoString = "descricao";
  static const String _pontosAquisicaoString = "pontos_aquisicao";
  static const String _racasNaoPermitidasString = "racas_nao_permitidas";
  static const String _habilidadeIdString = "id_habilidade_aperfeicoada";
  static const String _cdTipoString = "codigo_tipo_habilidade_penalizada";

  static const String _separatorString = ",";

  @override
  Profissao fromJson(Map<String, dynamic> json) {
    List<int> racasPermitidas = json[_racasNaoPermitidasString]
        .toString()
        .split(_separatorString)
        .map((String e) => int.parse(e))
        .toList();
    return Profissao(
      id: json[_idString],
      nome: json[_nomeString],
      descricao: json[_descricaoString],
      racasNaoPermitidas: racasPermitidas,
      ehBasica: json[_ehBasicaString]?.toInt(),
      habilidadeIdAperfeicoada: json[_habilidadeIdString]?.toInt(),
      pontosAquisicaoHabilidade: json[_pontosAquisicaoString]?.toInt(),
      cdTipoHabilidadePenalizada:
          habilidadeFromInt(json[_cdTipoString]?.toInt()),
    );
  }

  @override
  Map<String, dynamic> toJson(Profissao object) {
    final data = <String, dynamic>{};
    data[_idString] = object.id;
    data[_nomeString] = object.nome;
    data[_ehBasicaString] = object.ehBasica;
    data[_descricaoString] = object.descricao;
    data[_habilidadeIdString] = object.habilidadeIdAperfeicoada;
    data[_pontosAquisicaoString] = object.pontosAquisicaoHabilidade;
    data[_cdTipoString] = object.cdTipoHabilidadePenalizada.toString();
    data[_racasNaoPermitidasString] =
        object.racasNaoPermitidas.join(_separatorString);
    return data;
  }
}
