class Dinheiro {
  final int ouro;
  final int prata;
  final int cobre;
  final String? descricao;

  const Dinheiro(
      {this.ouro = 0, this.prata = 0, this.cobre = 0, this.descricao});

  @override
  String toString() {
    if (descricao != null) {
      return descricao!;
    }
    String output = "";
    if (ouro > 0) {
      output += "$ouro moeda${ouro > 1 ? 's' : ''} de ouro";
      if (prata > 0 && cobre > 0) {
        output += ", ";
      } else if (prata > 0 || cobre > 0) {
        output += " e ";
      }
    }
    if (prata > 0) {
      output += "$prata moeda${prata > 1 ? 's' : ''} de prata";
      if (cobre > 0) {
        output += " e ";
      }
    }
    if (cobre > 0) {
      output += "$cobre moeda${cobre > 1 ? 's' : ''} de cobre";
    }

    if (ouro == 0 && prata == 0 && cobre == 0) {
      output = "Ver descrição";
    }
    return output;
  }
}
