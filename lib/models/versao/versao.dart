class Versao {
  final String version;
  final List<String> changes;

  const Versao({required this.version, required this.changes});
}
