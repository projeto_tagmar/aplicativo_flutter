import 'package:tagmar/models/xtra/abstract_named_item.dart';

class Atributo extends AbstractNamedItem {
  const Atributo({
    required int id,
    required String nome,
    required String descricao,
  }) : super(
          id: id,
          nome: nome,
          descricao: descricao,
        );
}
