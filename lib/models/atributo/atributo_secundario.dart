import 'package:tagmar/models/atributo/atributo.dart';

class AtributoSecundario extends Atributo {
  const AtributoSecundario({
    required int id,
    required String nome,
    required String descricao,
  }) : super(
          id: id,
          nome: nome,
          descricao: descricao,
        );
}
